-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 14, 2017 at 07:06 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eccs`
--

-- --------------------------------------------------------

--
-- Table structure for table `classroom`
--

CREATE TABLE `classroom` (
  `classroomID` int(11) NOT NULL,
  `caretakerID` int(11) DEFAULT NULL,
  `levelID` int(11) NOT NULL DEFAULT '1',
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `classroom`
--

INSERT INTO `classroom` (`classroomID`, `caretakerID`, `levelID`, `name`) VALUES
(7, 153, 1, 'Classroom 7'),
(8, 7, 3, 'Classroom 8'),
(13, 12, 2, 'Classroom 13'),
(19, 18, 3, 'Classroom 19');

-- --------------------------------------------------------

--
-- Table structure for table `conversation`
--

CREATE TABLE `conversation` (
  `conversationID` int(11) NOT NULL,
  `userID1` int(11) DEFAULT NULL,
  `userID2` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `conversation`
--

INSERT INTO `conversation` (`conversationID`, `userID1`, `userID2`) VALUES
(1, 152, 153),
(2, 11, 153);

-- --------------------------------------------------------

--
-- Table structure for table `conversation_content`
--

CREATE TABLE `conversation_content` (
  `conversation_contentID` int(11) NOT NULL,
  `conversationID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `datetime` datetime NOT NULL,
  `message` varchar(1000) NOT NULL DEFAULT '',
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `game`
--

CREATE TABLE `game` (
  `gameID` int(11) NOT NULL,
  `name` varchar(60) NOT NULL,
  `description` varchar(100) NOT NULL,
  `levelID` int(11) NOT NULL,
  `uploaderID` int(11) NOT NULL,
  `tutorialVideoID` int(11) DEFAULT NULL,
  `tutorialText` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `game`
--

INSERT INTO `game` (`gameID`, `name`, `description`, `levelID`, `uploaderID`, `tutorialVideoID`, `tutorialText`) VALUES
(5, 'Snake', 'Snake thing', 2, 151, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `gamehistory`
--

CREATE TABLE `gamehistory` (
  `gamehistoryID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `gameID` int(11) NOT NULL,
  `playedOn` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `message` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gamehistory`
--

INSERT INTO `gamehistory` (`gamehistoryID`, `userID`, `gameID`, `playedOn`, `message`) VALUES
(1, 151, 5, '2017-03-03 22:59:27', 'I got a score of 0 on Snake!'),
(5, 151, 5, '2017-03-03 23:19:55', 'I got a score of 0 on Snake!'),
(6, 2, 5, '2017-03-05 09:54:24', 'I got a score of 0 on Snake!'),
(7, 151, 5, '2017-03-05 13:14:45', 'I got a score of 0 on Snake!'),
(8, 2, 5, '2017-03-05 13:31:28', 'I got a score of 1 on Snake!'),
(9, 2, 5, '2017-03-05 13:33:17', 'I got a score of 0 on Snake!'),
(10, 2, 5, '2017-03-05 13:33:30', 'I got a score of 3 on Snake!'),
(11, 2, 5, '2017-03-05 13:33:39', 'I got a score of 0 on Snake!'),
(12, 2, 5, '2017-03-05 13:34:41', 'I got a score of 0 on Snake!'),
(13, 2, 5, '2017-03-05 16:24:34', 'I got a score of 1 on Snake!'),
(14, 2, 5, '2017-03-05 16:31:54', 'I got a score of 1 on Snake!');

-- --------------------------------------------------------

--
-- Table structure for table `image`
--

CREATE TABLE `image` (
  `imageID` int(11) NOT NULL,
  `uploadDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `filename` varchar(100) NOT NULL,
  `description` varchar(300) NOT NULL,
  `uploaderID` int(11) NOT NULL,
  `lastUpdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `image`
--

INSERT INTO `image` (`imageID`, `uploadDate`, `filename`, `description`, `uploaderID`, `lastUpdate`) VALUES
(1, '2017-01-19 00:00:00', 'default.jpg', 'Default Image', 151, '2017-01-19 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `level`
--

CREATE TABLE `level` (
  `levelID` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `nextLevelID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `level`
--

INSERT INTO `level` (`levelID`, `level`, `name`, `nextLevelID`) VALUES
(1, 1, 'Nursery', 2),
(2, 2, 'Preschool', 3),
(3, 3, 'Kindergarten', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `roleID` int(11) NOT NULL,
  `name` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`roleID`, `name`) VALUES
(1, 'Moderator'),
(2, 'Caretaker'),
(3, 'Parent'),
(4, 'Child');

-- --------------------------------------------------------

--
-- Table structure for table `spiceactivity`
--

CREATE TABLE `spiceactivity` (
  `spiceactivityID` int(11) NOT NULL,
  `spicedevelopmentID` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `spiceactivity`
--

INSERT INTO `spiceactivity` (`spiceactivityID`, `spicedevelopmentID`, `name`) VALUES
(1, 1, 'Hangman'),
(2, 1, 'Team Building'),
(3, 1, 'What is it?'),
(4, 1, 'Tell A Story'),
(5, 1, 'Bogus Adventure Stories'),
(6, 5, 'Lunch Time'),
(7, 2, 'Soccer'),
(8, 2, 'Basketball'),
(9, 2, 'Tag'),
(10, 2, 'Dodgeball'),
(11, 2, 'Tug of war'),
(12, 3, 'Chess'),
(13, 3, 'Calculus'),
(14, 3, 'Crossword'),
(15, 3, 'Tic Tac Toe'),
(16, 3, 'Reading'),
(17, 4, 'Drawing'),
(18, 4, 'Dancing'),
(19, 4, 'Painting'),
(20, 4, 'Clay Molding'),
(21, 4, 'Crafting'),
(22, 5, 'If You’re Happy and You Know It'),
(23, 5, 'Mad Face, Scary Face'),
(24, 5, 'How Does It Feel?'),
(25, 5, 'Naming Emotions');

-- --------------------------------------------------------

--
-- Table structure for table `spiceactivitydetail`
--

CREATE TABLE `spiceactivitydetail` (
  `spiceactivitydetail` int(11) NOT NULL,
  `childID` int(11) NOT NULL,
  `caretakerID` int(11) NOT NULL,
  `spiceactivityID` int(11) NOT NULL,
  `rating` int(11) NOT NULL,
  `addedOn` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `comments` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `spiceactivitydetail`
--

INSERT INTO `spiceactivitydetail` (`spiceactivitydetail`, `childID`, `caretakerID`, `spiceactivityID`, `rating`, `addedOn`, `comments`) VALUES
(2, 2, 6, 2, 10, '2017-01-31 08:32:14', 'this is a very long comment. this is a very long comment. this is a very long comment. this is a very long comment. this is a very long comment. this is a very long comment. '),
(3, 2, 6, 3, 4, '2017-01-01 08:32:14', 'This is short comment'),
(4, 2, 6, 4, 5, '2016-12-22 08:32:14', 'this is a very long comment. this is a very long comment. this is a very long comment. this is a very long comment. this is a very long comment. this is a very long comment. '),
(5, 2, 6, 5, 8, '2016-12-20 08:32:14', 'This is short comment'),
(6, 2, 6, 6, 0, '2017-01-22 08:32:14', 'this is a very long comment. this is a very long comment. this is a very long comment. this is a very long comment. this is a very long comment. this is a very long comment. '),
(7, 2, 6, 7, 10, '2017-01-22 08:32:14', 'This is short comment'),
(8, 2, 6, 8, 9, '2017-01-22 08:32:14', 'this is a very long comment. this is a very long comment. this is a very long comment. this is a very long comment. this is a very long comment. this is a very long comment. '),
(9, 2, 6, 9, 3, '2017-01-31 08:32:14', 'This is short comment'),
(10, 2, 6, 10, 4, '2017-01-22 08:32:14', 'this is a very long comment. this is a very long comment. this is a very long comment. this is a very long comment. this is a very long comment. this is a very long comment. '),
(11, 2, 6, 11, 0, '2017-01-22 08:32:14', 'This is short comment'),
(12, 2, 6, 12, 8, '2017-01-22 08:32:14', 'this is a very long comment. this is a very long comment. this is a very long comment. this is a very long comment. this is a very long comment. this is a very long comment. '),
(13, 2, 6, 13, 7, '2017-01-22 08:32:14', 'This is short comment'),
(14, 2, 6, 14, 9, '2017-01-22 08:32:14', 'this is a very long comment. this is a very long comment. this is a very long comment. this is a very long comment. this is a very long comment. this is a very long comment. '),
(15, 2, 6, 15, 6, '2017-01-22 08:32:14', 'This is short comment'),
(16, 2, 6, 16, 9, '2017-01-29 08:32:14', 'this is a very long comment. this is a very long comment. this is a very long comment. this is a very long comment. this is a very long comment. this is a very long comment. '),
(17, 2, 6, 17, 2, '2017-01-22 08:32:14', 'This is short comment'),
(18, 2, 6, 18, 7, '2017-01-22 08:32:14', 'this is a very long comment. this is a very long comment. this is a very long comment. this is a very long comment. this is a very long comment. this is a very long comment. '),
(19, 2, 6, 19, 6, '2017-01-22 08:32:14', 'This is short comment'),
(20, 2, 6, 20, 6, '2017-01-22 08:32:14', 'this is a very long comment. this is a very long comment. this is a very long comment. this is a very long comment. this is a very long comment. this is a very long comment. '),
(21, 2, 6, 21, 2, '2017-01-22 08:32:14', 'This is short comment'),
(22, 2, 6, 22, 5, '2017-01-22 08:32:14', 'this is a very long comment. this is a very long comment. this is a very long comment. this is a very long comment. this is a very long comment. this is a very long comment. '),
(23, 2, 6, 23, 1, '2017-01-22 08:32:14', 'This is short comment'),
(24, 2, 6, 24, 8, '2017-01-22 08:32:14', 'this is a very long comment. this is a very long comment. this is a very long comment. this is a very long comment. this is a very long comment. this is a very long comment. '),
(25, 2, 6, 25, 0, '2017-01-22 08:32:14', 'This is short comment'),
(26, 2, 151, 3, 4, '2017-02-03 14:46:59', 'This is editted'),
(27, 2, 151, 1, 2, '2017-02-03 16:14:56', 'No comment'),
(28, 2, 151, 7, 10, '2017-02-06 00:18:00', 'He was very fast');

-- --------------------------------------------------------

--
-- Table structure for table `spicedevelopment`
--

CREATE TABLE `spicedevelopment` (
  `spicedevelopmentID` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `spicedevelopment`
--

INSERT INTO `spicedevelopment` (`spicedevelopmentID`, `name`) VALUES
(4, 'Creative'),
(5, 'Emotional'),
(3, 'Intellectual'),
(2, 'Physical'),
(1, 'Social');

-- --------------------------------------------------------

--
-- Table structure for table `spiceformdetail`
--

CREATE TABLE `spiceformdetail` (
  `spiceformdetailID` int(11) NOT NULL,
  `childID` int(11) NOT NULL,
  `caretakerID` int(11) NOT NULL,
  `social_detail` varchar(1000) NOT NULL,
  `physical_detail` varchar(1000) NOT NULL,
  `intellectual_detail` varchar(1000) NOT NULL,
  `creative_detail` varchar(1000) NOT NULL,
  `emotional_detail` varchar(1000) NOT NULL,
  `addedOn` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `spiceformdetail`
--

INSERT INTO `spiceformdetail` (`spiceformdetailID`, `childID`, `caretakerID`, `social_detail`, `physical_detail`, `intellectual_detail`, `creative_detail`, `emotional_detail`, `addedOn`) VALUES
(2, 2, 151, 'a', 'b', 'c', 'dadf', 'adfafafafadfasdf', '2017-01-24 13:26:44'),
(3, 9, 151, 'Not so friendly.', 'Not so active.', '', '', '', '2017-01-24 13:29:44'),
(4, 2, 151, 'Not so friendly.', 'asfa', 'adf', 'asdfa', '', '2017-02-05 00:00:00'),
(5, 2, 6, 'She is very friendly', 'She is very strong', 'She is very smart', 'Very creative too', 'Very emotional', '2017-03-01 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `userID` int(11) NOT NULL,
  `userName` varchar(50) NOT NULL,
  `pass` varchar(50) NOT NULL,
  `lastModified` datetime NOT NULL,
  `lastLogin` datetime NOT NULL,
  `roleID` int(11) NOT NULL,
  `classroomID` int(11) DEFAULT NULL,
  `emailVerified` int(11) NOT NULL DEFAULT '0',
  `emailVerificationHash` varchar(100) NOT NULL,
  `adminVerified` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`userID`, `userName`, `pass`, `lastModified`, `lastLogin`, `roleID`, `classroomID`, `emailVerified`, `emailVerificationHash`, `adminVerified`) VALUES
(1, 'user1', '21232f297a57a5a743894a0e4a801fc3', '2016-11-12 00:00:00', '2016-06-12 00:00:00', 3, NULL, 1, '4b0250793549726d5c1ea3906726ebfe', 1),
(2, 'user2', '5f4dcc3b5aa765d61d8327deb882cf99', '2015-12-24 00:00:00', '2016-02-16 00:00:00', 4, NULL, 1, '4b0250793549726d5c1ea3906726ebfe', 1),
(3, 'user3', '21232f297a57a5a743894a0e4a801fc3', '2016-12-09 00:00:00', '2016-12-31 00:00:00', 3, NULL, 1, '4b0250793549726d5c1ea3906726ebfe', 1),
(4, 'user4', '5f4dcc3b5aa765d61d8327deb882cf99', '2016-08-10 00:00:00', '2016-08-07 00:00:00', 4, 13, 1, '4b0250793549726d5c1ea3906726ebfe', 1),
(5, 'user5', '5f4dcc3b5aa765d61d8327deb882cf99', '2016-03-21 00:00:00', '2016-08-11 00:00:00', 4, 7, 1, '4b0250793549726d5c1ea3906726ebfe', 1),
(6, 'user6', '21232f297a57a5a743894a0e4a801fc3', '2016-01-24 00:00:00', '2016-03-02 00:00:00', 2, NULL, 1, '4b0250793549726d5c1ea3906726ebfe', 1),
(7, 'user7', '5f4dcc3b5aa765d61d8327deb882cf99', '2016-11-01 00:00:00', '2016-05-17 00:00:00', 2, NULL, 0, '4b0250793549726d5c1ea3906726ebfe', 0),
(8, 'user8', '5f4dcc3b5aa765d61d8327deb882cf99', '2016-02-17 00:00:00', '2016-07-08 00:00:00', 3, NULL, 1, '4b0250793549726d5c1ea3906726ebfe', 0),
(9, 'user9', '5f4dcc3b5aa765d61d8327deb882cf99', '2016-06-16 00:00:00', '2016-12-22 00:00:00', 4, 7, 1, '4b0250793549726d5c1ea3906726ebfe', 1),
(10, 'user10', '5f4dcc3b5aa765d61d8327deb882cf99', '2016-04-06 00:00:00', '2016-12-07 00:00:00', 4, 19, 1, '4b0250793549726d5c1ea3906726ebfe', 1),
(11, 'user11', '5f4dcc3b5aa765d61d8327deb882cf99', '2016-03-02 00:00:00', '2016-05-26 00:00:00', 3, NULL, 0, '4b0250793549726d5c1ea3906726ebfe', 0),
(12, 'user12', '5f4dcc3b5aa765d61d8327deb882cf99', '2016-02-27 00:00:00', '2016-07-29 00:00:00', 2, NULL, 0, '4b0250793549726d5c1ea3906726ebfe', 0),
(13, 'user13', '5f4dcc3b5aa765d61d8327deb882cf99', '2017-01-01 00:00:00', '2015-12-30 00:00:00', 4, 19, 1, '4b0250793549726d5c1ea3906726ebfe', 1),
(14, 'user14', '5f4dcc3b5aa765d61d8327deb882cf99', '2016-05-07 00:00:00', '2016-11-15 00:00:00', 2, NULL, 1, '4b0250793549726d5c1ea3906726ebfe', 1),
(15, 'user15', '5f4dcc3b5aa765d61d8327deb882cf99', '2016-01-01 00:00:00', '2016-08-29 00:00:00', 2, NULL, 1, '4b0250793549726d5c1ea3906726ebfe', 0),
(16, 'user16', '5f4dcc3b5aa765d61d8327deb882cf99', '2015-12-22 00:00:00', '2016-05-26 00:00:00', 4, 13, 1, '4b0250793549726d5c1ea3906726ebfe', 1),
(17, 'user17', '5f4dcc3b5aa765d61d8327deb882cf99', '2016-12-31 00:00:00', '2016-11-18 00:00:00', 4, 8, 1, '4b0250793549726d5c1ea3906726ebfe', 1),
(18, 'user18', '5f4dcc3b5aa765d61d8327deb882cf99', '2016-11-03 00:00:00', '2016-07-20 00:00:00', 2, NULL, 1, '4b0250793549726d5c1ea3906726ebfe', 1),
(19, 'user19', '5f4dcc3b5aa765d61d8327deb882cf99', '2016-04-18 00:00:00', '2016-03-12 00:00:00', 4, 7, 1, '4b0250793549726d5c1ea3906726ebfe', 1),
(20, 'user20', '5f4dcc3b5aa765d61d8327deb882cf99', '2016-03-11 00:00:00', '2016-05-27 00:00:00', 3, NULL, 1, '4b0250793549726d5c1ea3906726ebfe', 0),
(151, 'admin', '21232f297a57a5a743894a0e4a801fc3', '2017-01-19 00:00:00', '2017-01-19 00:00:00', 1, NULL, 1, 'eba0dc302bcd9a273f8bbb72be3a687b', 1),
(152, 'parent', 'd0e45878043844ffc41aac437e86b602', '2017-02-28 00:00:00', '2017-02-28 00:00:00', 3, NULL, 1, '6a10bbd480e4c5573d8f3af73ae0454b', 1),
(153, 'caretaker', '3ba981de0dd9c6cb247a61ee2f5ad9b5', '2017-04-05 00:00:00', '2017-04-05 00:00:00', 2, NULL, 1, '389bc7bb1e1c2a5e7e147703232a88f6', 1);

-- --------------------------------------------------------

--
-- Table structure for table `userprofile`
--

CREATE TABLE `userprofile` (
  `userProfileID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `emailAddress` varchar(100) NOT NULL,
  `firstName` varchar(200) NOT NULL,
  `lastName` varchar(200) NOT NULL,
  `telephone` varchar(50) NOT NULL,
  `address` varchar(300) DEFAULT NULL,
  `gender` varchar(1) NOT NULL DEFAULT '1',
  `dateOfBirth` date DEFAULT NULL,
  `parentID` int(11) DEFAULT NULL,
  `registeredBy` int(11) DEFAULT NULL,
  `levelID` int(11) DEFAULT NULL,
  `profPicID` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userprofile`
--

INSERT INTO `userprofile` (`userProfileID`, `userID`, `emailAddress`, `firstName`, `lastName`, `telephone`, `address`, `gender`, `dateOfBirth`, `parentID`, `registeredBy`, `levelID`, `profPicID`) VALUES
(1, 1, 'LissetteCraw@gmail.com', 'Lissette', 'Craw', '2461405835', '24 Summer Pl, Llansamlet, Swansea SA7 9SB, UK', '2', '1983-02-28', 0, NULL, NULL, 1),
(2, 2, 'AdelaideLabat@gmail.com', 'Adelaide', 'Labat', '8447659296', 'B4190, Bewdley DY12 2QL, UK', '1', '2014-10-09', 152, NULL, 2, 1),
(3, 3, 'CarmellaSchock@gmail.com', 'Carmella', 'Schock', '9050103731', '37 The Pines, Horsham RH12 4UF, UK', '2', '1986-11-04', NULL, NULL, NULL, 1),
(4, 4, 'BebeOrdonez@gmail.com', 'Bebe', 'Ordonez', '5792980132', '24 Vicarage Way, Hurst Green, Etchingham TN19 7QQ, UK', '2', '2016-02-13', 152, NULL, 1, 1),
(5, 5, 'ByronFleetwood@gmail.com', 'Byron', 'Fleetwood', '4932100726', '29 Upper Belgrave Rd, Stoke-on-Trent ST3 4RA, UK', '1', '2013-05-15', 152, NULL, 1, 1),
(6, 6, 'StaceeKorte@gmail.com', 'Stacee', 'Korte', '2139373464', '1 Bridgemans Green, Latchingdon, Chelmsford CM3 6LJ, UK', '2', '1983-09-29', NULL, NULL, NULL, 1),
(7, 7, 'JuttaVanmatre@gmail.com', 'Jutta', 'Vanmatre', '6240332605', '4 Lower Westfields, Bromyard HR7 4EP, UK', '2', '1988-06-19', NULL, NULL, NULL, 1),
(8, 8, 'CliffRedington@gmail.com', 'Cliff', 'Redington', '4518553091', '9 Finistere Ave, Eastbourne BN23 6UJ, UK', '1', '1995-02-08', NULL, NULL, NULL, 1),
(9, 9, 'CheriLinehan@gmail.com', 'Cheri', 'Linehan', '3926163267', 'Turlowfields Ln, Ashbourne DE6 1PU, UK', '1', '2014-09-05', 11, NULL, 1, 1),
(10, 10, 'DarrylHoffmeister@gmail.com', 'Darryl', 'Hoffmeister', '7538306524', '5 Dane St, London WC1R, UK', '2', '2016-06-01', 11, NULL, 3, 1),
(11, 11, 'PercyHaggard@gmail.com', 'Percy', 'Haggard', '5055877586', '60 Ashford Rd, Tenterden TN30 6LR, UK', '2', '1980-09-15', NULL, NULL, NULL, 1),
(12, 12, 'StanConkling@gmail.com', 'Stan', 'Conkling', '9607359813', 'Tanner''s Hill, Brockham, Betchworth RH3 7JN, UK', '1', '1990-02-16', NULL, NULL, NULL, 1),
(13, 13, 'EnedinaChristy@gmail.com', 'Enedina', 'Christy', '7075470232', '14 Lime Terrace, Irthlingborough, Wellingborough NN9 5SJ, UK', '1', '2014-01-31', 20, NULL, 2, 1),
(14, 14, 'VeronikaLinger@gmail.com', 'Veronika', 'Linger', '3433127702', 'Golf Course Rd, Painswick, Stroud GL6, UK', '2', '1991-05-12', NULL, NULL, NULL, 1),
(15, 15, 'CassondraPetties@gmail.com', 'Cassondra', 'Petties', '9163999375', '43 Hill Ln, Haverfordwest SA61, UK', '2', '1993-05-11', NULL, NULL, NULL, 1),
(16, 16, 'ToniBrabant@gmail.com', 'Toni', 'Brabant', '1370346225', '5-6 A247, Send, Woking GU23, UK', '1', '2016-02-21', 20, NULL, 2, 1),
(17, 17, 'JacklynSergeant@gmail.com', 'Jacklyn', 'Sergeant', '8556192244', '96 Calder Rd, Mirfield WF14 8NP, UK', '1', '2014-06-23', 20, NULL, 1, 1),
(18, 18, 'TrentVandermark@gmail.com', 'Trent', 'Vandermark', '8351022267', '4 Cae Ganol, Drenewydd yn Notais, Porthcawl CF36 3RS, UK', '1', '1987-04-15', NULL, NULL, NULL, 1),
(19, 19, 'DarwinGloor@gmail.com', 'Darwin', 'Gloor', '2393616882', '6 Royal Buildings, Penarth CF64 3ED, UK', '1', '2016-01-18', 20, NULL, 1, 1),
(20, 20, 'TrumanBirge@gmail.com', 'Truman', 'Birge', '4062551060', '11 Doncaster Rd, Darfield, Barnsley S73 9JB, UK', '1', '1986-12-06', NULL, NULL, NULL, 1),
(151, 151, 'TrumanBirge@gmail.com', 'Truman', 'Birge', '4062551060', '11 Doncaster Rd, Darfield, Barnsley S73 9JB, UK', '1', '1986-12-06', NULL, NULL, NULL, 1),
(152, 152, 'parent@parent.com', 'parent', 'parent', '09228232333', 'Somewhere', '1', '1990-10-23', NULL, NULL, NULL, 1),
(153, 153, 'caretaker@caretaker.com', 'Caretaker', 'Caretaker', '09228732222', 'somewhere', '1', '1993-10-23', NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `video`
--

CREATE TABLE `video` (
  `videoID` int(11) NOT NULL,
  `title` varchar(100) NOT NULL DEFAULT 'A title',
  `filename` varchar(200) NOT NULL,
  `uploadDate` date NOT NULL DEFAULT '0000-00-00',
  `uploaderID` int(11) NOT NULL,
  `roleID` int(11) DEFAULT NULL,
  `levelID` int(11) DEFAULT NULL,
  `description` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `video`
--

INSERT INTO `video` (`videoID`, `title`, `filename`, `uploadDate`, `uploaderID`, `roleID`, `levelID`, `description`) VALUES
(6, 'A title', 'https://www.youtube.com/watch?v=W7qWa52k-nE', '2017-02-18', 151, 3, 1, '1'),
(7, 'A title', 'https://www.youtube.com/watch?v=W7qWa52k-nE', '2017-02-18', 151, 2, 1, '1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `classroom`
--
ALTER TABLE `classroom`
  ADD PRIMARY KEY (`classroomID`),
  ADD KEY `caretakerID` (`caretakerID`),
  ADD KEY `levelID` (`levelID`);

--
-- Indexes for table `conversation`
--
ALTER TABLE `conversation`
  ADD PRIMARY KEY (`conversationID`),
  ADD KEY `userID1` (`userID1`),
  ADD KEY `userID2` (`userID2`);

--
-- Indexes for table `conversation_content`
--
ALTER TABLE `conversation_content`
  ADD PRIMARY KEY (`conversation_contentID`),
  ADD KEY `conversationID` (`conversationID`),
  ADD KEY `userID` (`userID`),
  ADD KEY `datetime` (`datetime`);

--
-- Indexes for table `game`
--
ALTER TABLE `game`
  ADD PRIMARY KEY (`gameID`),
  ADD UNIQUE KEY `name` (`name`),
  ADD KEY `levelID` (`levelID`),
  ADD KEY `uploaderID` (`uploaderID`),
  ADD KEY `tutorialVideoID` (`tutorialVideoID`);

--
-- Indexes for table `gamehistory`
--
ALTER TABLE `gamehistory`
  ADD PRIMARY KEY (`gamehistoryID`),
  ADD KEY `userID` (`userID`),
  ADD KEY `gameID` (`gameID`),
  ADD KEY `playedOn` (`playedOn`);

--
-- Indexes for table `image`
--
ALTER TABLE `image`
  ADD PRIMARY KEY (`imageID`),
  ADD KEY `uploaderID` (`uploaderID`),
  ADD KEY `uploadDate` (`uploadDate`),
  ADD KEY `lastUpdate` (`lastUpdate`);

--
-- Indexes for table `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`levelID`),
  ADD UNIQUE KEY `level` (`level`),
  ADD KEY `nextLevelID` (`nextLevelID`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`roleID`);

--
-- Indexes for table `spiceactivity`
--
ALTER TABLE `spiceactivity`
  ADD PRIMARY KEY (`spiceactivityID`),
  ADD KEY `spicedevelopmentID` (`spicedevelopmentID`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `spiceactivitydetail`
--
ALTER TABLE `spiceactivitydetail`
  ADD PRIMARY KEY (`spiceactivitydetail`),
  ADD KEY `childID` (`childID`),
  ADD KEY `caretakerID` (`caretakerID`),
  ADD KEY `spiceactivityID` (`spiceactivityID`),
  ADD KEY `rating` (`rating`),
  ADD KEY `addedOn` (`addedOn`);

--
-- Indexes for table `spicedevelopment`
--
ALTER TABLE `spicedevelopment`
  ADD PRIMARY KEY (`spicedevelopmentID`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `spiceformdetail`
--
ALTER TABLE `spiceformdetail`
  ADD PRIMARY KEY (`spiceformdetailID`),
  ADD KEY `childID` (`childID`),
  ADD KEY `caretakerID` (`caretakerID`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`userID`),
  ADD UNIQUE KEY `userName` (`userName`),
  ADD KEY `roleID` (`roleID`),
  ADD KEY `emailVerified` (`emailVerified`),
  ADD KEY `adminVerified` (`adminVerified`),
  ADD KEY `classroomID` (`classroomID`);

--
-- Indexes for table `userprofile`
--
ALTER TABLE `userprofile`
  ADD PRIMARY KEY (`userProfileID`),
  ADD UNIQUE KEY `userID` (`userID`),
  ADD KEY `gender` (`gender`),
  ADD KEY `dateOfBirth` (`dateOfBirth`),
  ADD KEY `emailAddress` (`emailAddress`),
  ADD KEY `parentID` (`parentID`),
  ADD KEY `registeredBy` (`registeredBy`),
  ADD KEY `levelID` (`levelID`),
  ADD KEY `profPicID` (`profPicID`);

--
-- Indexes for table `video`
--
ALTER TABLE `video`
  ADD PRIMARY KEY (`videoID`),
  ADD KEY `uploaderID` (`uploaderID`),
  ADD KEY `uploadDate` (`uploadDate`),
  ADD KEY `roleID` (`roleID`),
  ADD KEY `levelID` (`levelID`),
  ADD KEY `title` (`title`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `classroom`
--
ALTER TABLE `classroom`
  MODIFY `classroomID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `conversation`
--
ALTER TABLE `conversation`
  MODIFY `conversationID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `conversation_content`
--
ALTER TABLE `conversation_content`
  MODIFY `conversation_contentID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `game`
--
ALTER TABLE `game`
  MODIFY `gameID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `gamehistory`
--
ALTER TABLE `gamehistory`
  MODIFY `gamehistoryID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `image`
--
ALTER TABLE `image`
  MODIFY `imageID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `level`
--
ALTER TABLE `level`
  MODIFY `levelID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `roleID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `spiceactivity`
--
ALTER TABLE `spiceactivity`
  MODIFY `spiceactivityID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `spiceactivitydetail`
--
ALTER TABLE `spiceactivitydetail`
  MODIFY `spiceactivitydetail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `spicedevelopment`
--
ALTER TABLE `spicedevelopment`
  MODIFY `spicedevelopmentID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `spiceformdetail`
--
ALTER TABLE `spiceformdetail`
  MODIFY `spiceformdetailID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `userID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=154;
--
-- AUTO_INCREMENT for table `userprofile`
--
ALTER TABLE `userprofile`
  MODIFY `userProfileID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=154;
--
-- AUTO_INCREMENT for table `video`
--
ALTER TABLE `video`
  MODIFY `videoID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `classroom`
--
ALTER TABLE `classroom`
  ADD CONSTRAINT `classroom_ibfk_2` FOREIGN KEY (`levelID`) REFERENCES `level` (`levelID`),
  ADD CONSTRAINT `classroom_ibfk_3` FOREIGN KEY (`caretakerID`) REFERENCES `user` (`userID`);

--
-- Constraints for table `game`
--
ALTER TABLE `game`
  ADD CONSTRAINT `game_ibfk_1` FOREIGN KEY (`levelID`) REFERENCES `level` (`levelID`);

--
-- Constraints for table `gamehistory`
--
ALTER TABLE `gamehistory`
  ADD CONSTRAINT `gamehistory_ibfk_2` FOREIGN KEY (`gameID`) REFERENCES `game` (`gameID`),
  ADD CONSTRAINT `gamehistory_ibfk_3` FOREIGN KEY (`userID`) REFERENCES `user` (`userID`);

--
-- Constraints for table `image`
--
ALTER TABLE `image`
  ADD CONSTRAINT `image_ibfk_1` FOREIGN KEY (`uploaderID`) REFERENCES `user` (`userID`);

--
-- Constraints for table `level`
--
ALTER TABLE `level`
  ADD CONSTRAINT `level_ibfk_1` FOREIGN KEY (`nextLevelID`) REFERENCES `level` (`levelID`);

--
-- Constraints for table `spiceactivity`
--
ALTER TABLE `spiceactivity`
  ADD CONSTRAINT `spiceactivity_ibfk_1` FOREIGN KEY (`spicedevelopmentID`) REFERENCES `spicedevelopment` (`spicedevelopmentID`);

--
-- Constraints for table `spiceactivitydetail`
--
ALTER TABLE `spiceactivitydetail`
  ADD CONSTRAINT `spiceactivitydetail_ibfk_1` FOREIGN KEY (`childID`) REFERENCES `user` (`userID`),
  ADD CONSTRAINT `spiceactivitydetail_ibfk_2` FOREIGN KEY (`caretakerID`) REFERENCES `user` (`userID`),
  ADD CONSTRAINT `spiceactivitydetail_ibfk_3` FOREIGN KEY (`spiceactivityID`) REFERENCES `spiceactivity` (`spiceactivityID`) ON DELETE CASCADE;

--
-- Constraints for table `spiceformdetail`
--
ALTER TABLE `spiceformdetail`
  ADD CONSTRAINT `spiceformdetail_ibfk_1` FOREIGN KEY (`childID`) REFERENCES `user` (`userID`),
  ADD CONSTRAINT `spiceformdetail_ibfk_2` FOREIGN KEY (`caretakerID`) REFERENCES `user` (`userID`);

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`roleID`) REFERENCES `role` (`roleID`),
  ADD CONSTRAINT `user_ibfk_2` FOREIGN KEY (`classroomID`) REFERENCES `classroom` (`classroomID`);

--
-- Constraints for table `userprofile`
--
ALTER TABLE `userprofile`
  ADD CONSTRAINT `userprofile_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `user` (`userID`) ON DELETE CASCADE;

--
-- Constraints for table `video`
--
ALTER TABLE `video`
  ADD CONSTRAINT `video_ibfk_2` FOREIGN KEY (`roleID`) REFERENCES `role` (`roleID`),
  ADD CONSTRAINT `video_ibfk_3` FOREIGN KEY (`uploaderID`) REFERENCES `user` (`userID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
