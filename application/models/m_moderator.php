<?php

	class m_user extends MY_Model
	{
		protected $_table_name = 'user';
		protected $_order_by = 'username';
		
		public function __construct()
		{
			parent::__construct();
		}
		
		public function login($user, $pass)
		{
			$user = $this->get_by( 

				array (
					'userName' => $user,
					'pass' => $pass,
				));
			

			if($user)
			{
				foreach ($user as $row)
				{
					$data = array (
						'userID'   		=> $row->userID,
						'userName'   		=> $row->userName,
						'pass' 		=> $row->pass ,
						'roleID' 		=> $row->roleID ,
						'classroomID' 		=> $row->classroomID ,
						'lastModified' 		=> $row->lastModified ,
						'lastLogin' 		=> $row->lastLogin ,
						'emailVerified' 		=> $row->emailVerified ,
						'adminVerified' 		=> $row->adminVerified ,
						'loggedin' 		=> TRUE
					);
					$this->session->set_userdata($data);
					return true;
				}
			}
			else{
				return false;
				
			}
		}
		public function checkUsername($user)
		{
			$user = $this->get_by( 

				array (
					'userName' => $user
				));
				if($user){
					return true;
				}
				else{
					return false;
				}
		}
		
		public function changePassUser($data,$id)
		{
			$query = $this->db->update('user', $data, array('userID' => $id));


				if($query){
					return true;
				}
				else{
					return false;
				}
		}
		public function addUser($data)
		{
			$user_data = array (
				'userName' =>  $data['username'],
				'lastModified' =>  date('Y-m-d'),
				'lastLogin' =>  date('Y-m-d'),
				'roleID' =>  $data['role'],
				'emailVerificationHash' =>  $data['hash'],
				'pass' =>  $data['pass']
			);
			
			$user_query = $this->db->insert('user', $user_data); 
			$insert_id = $this->db->insert_id();
			if ($this->db->affected_rows() > 0)
			{
			  
				$profile_data = array (
					'userID' =>  $insert_id,
					'emailAddress' =>  $data['email'],
					'firstName' =>  $data['name'],
					'lastName' =>  $data['surname'],
					'telephone' =>  $data['telephone'],
					'address' =>  $data['address'],
					'gender' =>  $data['gender'],
					'dateOfBirth' =>  $data['bday']
				);
			   $profile_query = $this->db->insert('userprofile', $profile_data); 
			  if ($this->db->affected_rows() > 0)
				{
					return $insert_id;
				}
			}
		}
		
		public function verifyEmail($data,$id)
		{
			$this->db->where('userID', $id);
			$this->db->update('user', $data); 
			if ($this->db->affected_rows() > 0)
				{
					return true;
				}
		}
		
	}