<?php

	class m_user extends MY_Model
	{
		protected $_table_name = 'user';
		protected $_order_by = 'username';
		
		public function __construct()
		{
			parent::__construct();
		}
		
		public function login($user, $pass)
		{
			$user = $this->get_by( 

				array (
					'userName' => $user,
					'pass' => $pass,
				));
			

			if($user)
			{
				foreach ($user as $row)
				{
					$data = array (
						'userID'   		=> $row->userID,
						'userName'   		=> $row->userName,
						'pass' 		=> $row->pass ,
						'roleID' 		=> $row->roleID ,
						'classroomID' 		=> $row->classroomID ,
						'lastModified' 		=> $row->lastModified ,
						'lastLogin' 		=> $row->lastLogin ,
						'emailVerified' 		=> $row->emailVerified ,
						'adminVerified' 		=> $row->adminVerified ,
						'loggedin' 		=> TRUE
					);
					$this->session->set_userdata($data);
					return true;
				}
			}
			else{
				return false;
				
			}
		}
		public function checkUsername($user)
		{
			$user = $this->get_by( 

				array (
					'userName' => $user
				));
				if($user){
					return true;
				}
				else{
					return false;
				}
		}
		
		public function changePassUser($data,$id)
		{
			$query = $this->db->update('user', $data, array('userID' => $id));


				if($query){
					return true;
				}
				else{
					return false;
				}
		}
		/******* Add ang Get Data ******/
		public function addUser($data)
		{
			$user_data = array (
				'userName' =>  $data['username'],
				'lastModified' =>  date('Y-m-d'),
				'lastLogin' =>  date('Y-m-d'),
				'roleID' =>  $data['role'],
				'emailVerificationHash' =>  $data['hash'],
				'pass' =>  $data['pass']
			);
			
			$user_query = $this->db->insert('user', $user_data); 
			$insert_id = $this->db->insert_id();
			if ($this->db->affected_rows() > 0)
			{
			  
				$profile_data = array (
					'userID' =>  $insert_id,
					'emailAddress' =>  $data['email'],
					'firstName' =>  $data['name'],
					'lastName' =>  $data['surname'],
					'telephone' =>  $data['telephone'],
					'address' =>  $data['address'],
					'gender' =>  $data['gender'],
					'dateOfBirth' =>  $data['bday']
				);
			   $profile_query = $this->db->insert('userprofile', $profile_data); 
			  if ($this->db->affected_rows() > 0)
				{
					return $insert_id;
				}
			}
		}
		
		public function verifyEmail($data,$id)
		{
			$this->db->where('userID', $id);
			$this->db->update('user', $data); 
			if ($this->db->affected_rows() > 0)
				{
					return true;
				}
		}
		public function adminVerified($id)
		{
			$query = $this->db->update('user', array('adminVerified' => 1), array('userID' => $id));


				if($query){
					return true;
				}
				else{
					return false;
				}
		}
		public function getAdminVerify()
		{
			$this->db->select('*');
			$this->db->from('user');
			$this->db->join('userprofile', 'user.userID = userprofile.userID'); 
			$this->db->where('adminVerified',"0");
			$query = $this->db->get();
			return $query->result();
		}
		
		public function getCaretakers()
		{
			$this->db->select('*');
			$this->db->from('user');
			$this->db->join('userprofile', 'user.userID = userprofile.userID'); 
			$this->db->where('roleID',"2");
			$query = $this->db->get();
			return $query->result();
		}
		
		public function getParents()
		{
			$this->db->select('*');
			$this->db->from('user');
			$this->db->join('userprofile', 'user.userID = userprofile.userID'); 
			$this->db->where('roleID',"3");
			$query = $this->db->get();
			
			return $query->result();
		}
		
		public function getNames($userID)
		{
			$this->db->select('firstName, lastName');
			$this->db->from('user');
			$this->db->where(array('user.userID' => $userID));
			$this->db->join('userprofile', 'user.userID = userprofile.userID'); 
			$query = $this->db->get();
			return $query->result();
		}
		
		public function getUserChild()
		{
			$this->db->select('*, userprofile.levelID as uLevelID, level.name as levelName');
			$this->db->from('user');
			$this->db->join('userprofile', 'userprofile.userID = user.userID','left'); 
			$this->db->join('classroom', 'classroom.classroomID = user.classroomID','left'); 
			$this->db->join('level', 'userprofile.levelID = level.levelID','left'); 
			
			$this->db->where('user.roleID',"4");
			$query = $this->db->get();
			return $query->result();
		}
		
		public function levelUpChild($userID,$level)
		{
			$nextLevel = 0;
			if($level == 1){
				$nextLevel = 2;
			}
			else if($level == 2){
				$nextLevel = 3;
			}
			else if($level == 3){
				$nextLevel = 3;
			}
			$query = $this->db->update('user', array('classroomID' => NULL), array('userID' => $userID));
			$query1 = $this->db->update('userprofile', array('levelID' =>$nextLevel), array('userID' => $userID));
			if($query && $query1){
				return true;
			}else{
				return false;
			}
		}
		
		public function getUserInfo($userID)
		{
			$this->db->select('*, classroom.levelID as clevel, userprofile.levelID as childLevel');
			$this->db->from('user');
			$this->db->where(array('user.userID' => $userID));
			$this->db->join('userprofile', 'user.userID = userprofile.userID','left outer'); 
			$this->db->join('classroom', 'classroom.classroomID = user.classroomID','left outer'); 
			$query = $this->db->get();
			return $query->result();

		}	

		
		public function getLikeCaretaker($keyword){  
			$this->db->select('*');
			$this->db->from('user');
			$this->db->where('roleID',"2");
			$this->db->like('firstName',$keyword,'after');  
			$this->db->join('userprofile', 'user.userID = userprofile.userID'); 
			$query = $this->db->get();      

			return $query->result();  
		} 
		public function getParentID($keyword){  
			$this->db->select('*');
			$this->db->from('user');
			$this->db->where('roleID',"3");
			$this->db->like('firstName',$keyword,'after');  
			$this->db->join('userprofile', 'user.userID = userprofile.userID'); 
			$query = $this->db->get();      

			return $query->result();  
		} 
		public function getAllClassroom($keyword){  
			$this->db->select('*');
			$this->db->from('classroom');
			$this->db->like('name',$keyword,'after'); 
			$query = $this->db->get();      

			return $query->result();  
		} 
		
		public function getClassroomWithCaretaker(){  
			$this->db->select('*, classroom.name as cname,classroom.levelID as clevel, level.name as levelName');
			$this->db->from('classroom');
			$this->db->join('userprofile', 'classroom.caretakerID = userprofile.userID', "left"); 
			$this->db->join('level', 'classroom.levelID = level.levelID', "left"); 
			$query = $this->db->get();      

			return $query->result();  
		} 
		public function getClassroomLevel($classID){  
			$this->db->select('levelID');
			$this->db->from('classroom');
			$this->db->where('classroomID',$classID);
			$query = $this->db->get();      

			return $query->result();  
		} 
		public function getClassroomInfo($classroomID){  
			$this->db->select('*, classroom.levelID as clevel');
			$this->db->from('classroom');
			$this->db->where('classroomID',$classroomID);
			//$this->db->join('userprofile', 'classroom.caretakerID = userprofile.userID'); 
			$query = $this->db->get();      
			$row = $query->first_row();
			
			if($row->caretakerID == NULL){
				return $query->result();
			}else{
				$this->db->select('*, classroom.levelID as clevel');
				$this->db->from('classroom');
				$this->db->where('classroomID',$classroomID);
				$this->db->join('userprofile', 'classroom.caretakerID = userprofile.userID'); 
				$query2 = $this->db->get();   
				return $query2->result();
			}
			  
		} 
		
		public function addChild($data)
		{
			$user_data = array (
				'lastModified' =>  date('Y-m-d'),
				'lastLogin' =>  date('Y-m-d'),
				'roleID' =>  $data['roleID'],
				'classroomID' =>  $data['classroomID']
			);
			
			$user_query = $this->db->insert('user', $user_data); 
			$insert_id = $this->db->insert_id();
			if ($this->db->affected_rows() > 0)
			{
			  
				$profile_data = array (
					'userID' =>  $insert_id,
					'firstName' =>  $data['name'],
					'lastName' =>  $data['surname'],
					'telephone' =>  $data['telephone'],
					'address' =>  $data['address'],
					'gender' =>  $data['gender'],
					'parentID' =>  $data['parentID'],
					'levelID' =>  $data['levelID'],
					'emailAddress' =>  " ",
					'dateOfBirth' =>  $data['bday']
				);
			   $profile_query = $this->db->insert('userprofile', $profile_data); 
			  if ($this->db->affected_rows() > 0)
				{
					return $insert_id;
				}
			}
		}
		public function addClassroom($data) {
			$query = $this->db->insert('classroom', $data); 
			$insert_id = $this->db->insert_id();
			  if ($this->db->affected_rows() > 0)
				{
					return $insert_id;
				}
		}
		
		public function allChildrenInClassroom($classroomID)
		{
			$this->db->select('*');
			$this->db->from('user');
			$this->db->where(array('user.classroomID' => $classroomID));
			$this->db->join('userprofile', 'user.userID = userprofile.userID'); 
			$query = $this->db->get();
			return $query->result();

		}
		
		public function getAssignedChildData($childID) {
			$this->db->select('*');
			$this->db->from('user');
			$this->db->where(array('user.userID' => $childID));
			$this->db->join('userprofile', 'user.userID = userprofile.userID'); 
			$query = $this->db->get();
			return $query->result();
			
		}
		
		public function getAssignedChildToCaretaker($caretakerID) {
			
			$allData = array();
			$this->db->select('classroomID');
			$this->db->from('classroom');
			$this->db->where(array('caretakerID' => $caretakerID));
			$query = $this->db->get();
			$row2 = $query->first_row();
			if($query->num_rows() == 0){
				return false;
			}
			else{
					$this->db->select('*');
					$this->db->from('user');
					$this->db->where(array('user.classroomID' => $row2->classroomID ));
					$this->db->join('userprofile', 'user.userID = userprofile.userID'); 
					$query2 = $this->db->get();
					foreach ($query2->result() as $row)
						{
							$this->db->select('firstName, lastName');
							$this->db->from('user');
							$this->db->where(array('user.userID' => $row->parentID ));
							$this->db->join('userprofile', 'user.userID = userprofile.userID'); 
							$query3 = $this->db->get();
							foreach ($query3->result() as $row3)
							{
								$data = array (
									'userID'   		=> $row->userID,
									'userName'   		=> $row->userName,
									'pass' 		=> $row->pass ,
									'roleID' 		=> $row->roleID ,
									'classroomID' 		=> $row->classroomID ,
									'lastModified' 		=> $row->lastModified ,
									'lastLogin' 		=> $row->lastLogin ,
									'emailVerified' 		=> $row->emailVerified ,
									'adminVerified' 		=> $row->adminVerified ,
									'emailAddress' 		=> $row->emailAddress ,
									'firstName' 		=> $row->firstName ,
									'lastName' 		=> $row->lastName ,
									'telephone' 		=> $row->telephone ,
									'address' 		=> $row->address ,
									'gender' 		=> $row->gender ,
									'dateOfBirth' 		=> $row->dateOfBirth ,
									'parentID' 		=> $row->parentID ,
									'registeredBy' 		=> $row->registeredBy ,
									'caretakerID' 		=> $caretakerID ,
									'parentName' 		=> ucfirst($row3->firstName)." ".ucfirst($row3->lastName)
								);
							
								array_push($allData,$data);
							}
						}
				return $allData;
			}

		}
		public function getMyChildren($parentID) {
			
			$allData = array();
			$this->db->select('*');
			$this->db->from('userprofile');
			$this->db->where(array('userprofile.parentID' => $parentID));
			$this->db->join('user', 'user.userID = userprofile.userID'); 
			$query = $this->db->get();
			
			foreach ($query->result() as $row)
				{
					$this->db->select('caretakerID');
					$this->db->from('classroom');
					$this->db->where(array('classroom.classroomID' => $row->classroomID ));
					$query2 = $this->db->get();
					foreach ($query2->result() as $row2)
					{
						$this->db->select('firstName, lastName');
						$this->db->from('user');
						$this->db->where(array('user.userID' => $row2->caretakerID ));
						$this->db->join('userprofile', 'user.userID = userprofile.userID'); 
						$query3 = $this->db->get();
						foreach ($query3->result() as $row3)
						{
							$data = array (
								'userID'   		=> $row->userID,
								'userName'   		=> $row->userName,
								'pass' 		=> $row->pass ,
								'roleID' 		=> $row->roleID ,
								'classroomID' 		=> $row->classroomID ,
								'lastModified' 		=> $row->lastModified ,
								'lastLogin' 		=> $row->lastLogin ,
								'emailVerified' 		=> $row->emailVerified ,
								'adminVerified' 		=> $row->adminVerified ,
								'emailAddress' 		=> $row->emailAddress ,
								'firstName' 		=> $row->firstName ,
								'lastName' 		=> $row->lastName ,
								'telephone' 		=> $row->telephone ,
								'address' 		=> $row->address ,
								'gender' 		=> $row->gender ,
								'dateOfBirth' 		=> $row->dateOfBirth ,
								'parentID' 		=> $row->parentID ,
								'registeredBy' 		=> $row->registeredBy ,
								'caretakerID' 		=> $row->caretakerID ,
								'caretakerName' 		=> ucfirst($row2->firstName)." ".ucfirst($row2->lastName) ,
								'parentName' 		=> ucfirst($row3->firstName)." ".ucfirst($row3->lastName) ,
								'roomname' 		=> $row->name 
							);
						
							array_push($allData,$data);
						}
					}
				}
			return $allData;			
		}
		
		/****** Delete Data ****/
		
		public function removeUser($userID) {
			$this->db->update('classroom', array('caretakerID' => NULL), array('caretakerID' => $userID));
			$this->db->delete('video', array('uploaderID' => $userID));
			$this->db->delete('gamehistory', array('userID' => $userID));
			$this->db->delete('image', array('uploaderID' => $userID));
			$this->db->delete('userprofile', array('userID' => $userID));
			$this->db->delete('spiceformdetail', array('caretakerID' => $userID));
			$this->db->delete('spiceactivitydetail', array('caretakerID' => $userID));
			$query = $this->db->delete('user', array('userID' => $userID));
			
			if($query){
				return true;
			}else{
				return false;
			}
			
		}
		public function removeChild($userID) {
			 $this->db->update('classroom', array('caretakerID' => NULL), array('caretakerID' => $userID));
			$this->db->delete('video', array('uploaderID' => $userID));
			$this->db->delete('gamehistory', array('userID' => $userID));
			$this->db->delete('image', array('uploaderID' => $userID));
			$this->db->delete('userprofile', array('userID' => $userID));
			$this->db->delete('spiceformdetail', array('childID' => $userID));
			$this->db->delete('spiceactivitydetail', array('childID' => $userID));
			$query = $this->db->delete('user', array('userID' => $userID));
			
			if($query){
				return true;
			}else{
				return false;
			}
			
		}
		public function removeClassroom($classID) {
			$query = $this->db->update('user', array('classroomID' => NULL), array('classroomID' => $classID));
			
			if($query){
				$this->db->delete('classroom', array('classroomID' => $classID));
				if ($this->db->affected_rows() > 0) {
					return TRUE;
				} else {
						return false;
				}
			}else{
				return false;
			}
			
		}
		
		public function removeMyChild($userID) {
			$query = $this->db->update('userprofile', array('parentID' => NULL), array('userID' => $userID));
			
			if($query){
				return TRUE;
			}else{
				return false;
			}
			
		}
		
		public function removeChildInClass($userID) {
			$query = $this->db->update('user', array('classroomID' => NULL), array('userID' => $userID));
			
			if($query){
				return TRUE;
			}else{
				return false;
			}
			
		}
		/********  Update Data*********/
		
		public function updateProfileAccount($profileData,$userID)
		{
			
			$query = $this->db->update('userprofile', $profileData, array('userID' => $userID));
			if($query){
				return true;
			}else{
				return false;
			}
		}
		
		public function updateChildProfileAccount($profileData,$userID,$classID)
		{
			$query = $this->db->update('user', $classID, array('userID' => $userID));
			$query1 = $this->db->update('userprofile', $profileData, array('userID' => $userID));
			if($query && $query1){
				return true;
			}else{
				return false;
			}
		}
		
		public function updateClassroom($classData,$classID)
		{
			$query1 = $this->db->update('user', array('classroomID' => NULL), array('classroomID' => $classID));
			$query2 = $this->db->update('classroom', $classData, array('classroomID' => $classID));
			if($query1 && $query2){
				return true;
			}else{
				return false;
			}
		}
		
		
		public function getMyCaretaker($id)
		{
			$this->db->select('*');
			$this->db->from('user');
			$this->db->where(array('user.userID' => $id));
			$this->db->join('userprofile', 'user.userID = userprofile.userID'); 
			$query = $this->db->get();
			return $query->result();

		}		
		
		public function getChildData($childID) {
			$selectQuery = 'image.filename
						 , user.userID
						 , userprofile.firstName
						 , userprofile.lastName
						 , userprofile.telephone
						 , userprofile.dateOfBirth
						 , userprofile.address
						 , userprofile.parentID as parentID';
			
			$this->db->select($selectQuery);
			$this->db->from('user');
			$this->db->join('userprofile', 'user.userID = userprofile.userID'); 
			$this->db->join('image', 'userprofile.profPicID = image.imageID');
			
			$this->db->where(array('user.userID' => $childID));
			
			$query = $this->db->get();
			$child['details'] = $query->result_array();
			
			if($child['details'][0]['parentID'] != null) {
				
				$this->db->select($selectQuery);
				$this->db->from('user');
				$this->db->join('userprofile', 'user.userID = userprofile.userID'); 
				$this->db->join('image', 'userprofile.profPicID = image.imageID');
				
				$this->db->where(array('user.userID' => $child['details'][0]['parentID']));
				$query = $this->db->get();
				
				$child['parent'] = $query->result_array();
			} else {
				$child['parent'] = null;
			}
			
			
			$this->db->select('spicedevelopment.name as spiceDevelopmentName
							 , spicedevelopment.spicedevelopmentID as spiceDevelopmentID
							 , spiceactivity.name as spiceActivityName
							 , spiceactivity.spiceactivityID as spiceActivityID');

			$this->db->from('spiceactivity'); 
			$this->db->join('spicedevelopment', 'spicedevelopment.spicedevelopmentID = spiceactivity.spicedevelopmentID');
			$query = $this->db->get();
			$child['spiceActivities'] = $query->result_array();
			
			//  get all activities done by child
			$this->db->select('spiceactivitydetail.rating
									 , spiceactivitydetail.comments
									 , spiceactivitydetail.addedOn
									 , spiceactivitydetail.spiceActivityID
									 , spiceactivitydetail.spiceactivitydetail as spiceActivityDetailID');
			$this->db->from('spiceactivitydetail'); 
			$this->db->where(array('spiceactivitydetail.childID' => $childID
												, 'spiceactivitydetail.addedOn >=' => date('Y-m-d',strtotime('last sunday -56 days'))));
			$query = $this->db->get();
			$child['spiceActivityRatings'] = $query->result_array();

			///////// gets spice form details
			
			$this->db->select('*');
			$this->db->from('spiceformdetail');
			
			$this->db->where(array('spiceformdetail.childID' => $childID,
								   'spiceformdetail.addedOn >=' => date('Y-m-d',strtotime('last sunday'))));
			$query = $this->db->get();
			$spiceFormDetails = $query->result_array();
			$child['spiceFormDetails'] = count($spiceFormDetails) == 0 ? null : $spiceFormDetails;
			
			// gets gametime timeline data by child
			$this->db->select('*');
			$this->db->from('gamehistory');
			$this->db->join('game', 'game.gameID = gamehistory.gameID');
			$this->db->where(array('userID' => $childID));
			$this->db->limit(10);
			$query = $this->db->get();
			$child['timeline'] = $query->result_array();
		
			// gets spiceform details
			$this->db->select('spiceactivitydetail.rating
							 , spiceactivitydetail.comments
							 , spiceactivitydetail.addedOn
							 , spiceactivitydetail.spiceActivityID
							 , spiceactivitydetail.spiceactivitydetail as spiceActivityDetailID
							 , spiceactivity.name as spiceActivityName
							 , spicedevelopment.name as spiceDevName');
			$this->db->from('spiceactivitydetail');
			$this->db->join('spiceactivity', 'spiceactivitydetail.spiceactivityID = spiceactivity.spiceactivityID');
			$this->db->join('spicedevelopment', 'spicedevelopment.spicedevelopmentID = spiceactivity.spicedevelopmentID');
			$this->db->where(array('spiceactivitydetail.childID' => $childID
								 , 'spiceactivitydetail.addedOn <' => date('Y-m-d',strtotime('last sunday -7 days'))));
			$this->db->limit(25);
			$query = $this->db->get();
			$spiceFormDetails = $query->result_array();
			$child['timelineSpice'] = count($spiceFormDetails) == 0 ? null : $spiceFormDetails;

			
			
			return $child;
		}

		public function getParentCaretakerData($userID, $roleID) {
			$selectQuery = 'image.filename
						 , user.userID
						 , user.roleID
						 , userprofile.firstName
						 , userprofile.lastName
						 , userprofile.telephone
						 , userprofile.dateOfBirth
						 , userprofile.address
						 , userprofile.gender
						 , userprofile.emailAddress
						 , userprofile.parentID as parentID';
			
			$this->db->select($selectQuery);
			$this->db->from('user');
			$this->db->join('userprofile', 'user.userID = userprofile.userID');
			$this->db->join('image', 'userprofile.profPicID = image.imageID');
			
			$this->db->where(array('user.userID' => $userID));
			
			$query = $this->db->get();
			$data['user'] = $query->result_array();
			
			if($roleID == 3) {
				$this->db->select($selectQuery);
				$this->db->from('user');
				$this->db->join('userprofile', 'user.userID = userprofile.userID'); 
				$this->db->join('image', 'userprofile.profPicID = image.imageID');
				$this->db->where(array('userprofile.parentID' => $userID));
				$query = $this->db->get();
				
				$data['children'] = $query->result_array();
				
			} else if($roleID == 2) {
				$selectQuery =  $selectQuery.", parentprofile.firstName as parentFirstName
											  , parentprofile.lastName as parentLastName";
				
				$this->db->select($selectQuery);
				$this->db->from('user');
				$this->db->join('userprofile', 'user.userID = userprofile.userID'); 
				$this->db->join('image', 'userprofile.profPicID = image.imageID');
				$this->db->join('classroom', 'user.classroomID = classroom.classroomID');
				$this->db->join('userprofile as parentprofile', 'userprofile.parentID = parentprofile.userID','left');
				$this->db->where(array('classroom.caretakerID' => $userID));
				$query = $this->db->get();
				$data['children'] = $query->result_array();
			}
			
			return $data;
		}
		
		public function addConversation($senderID, $receiverID) {
			
			$data = array(
				'userID1' => $receiverID,
				'userID2' => $senderID
			);
			
			$this->db->insert('conversation',$data);
			
			return ($this->db->affected_rows() != 1) ? false : true;
		}
		
		public function getAssignedChildren($searchString, $searcherUserID, $searcherRoleID)
		{
			// $assigneeID = isset($data['userID']);
			$this->db->select('user.userID, user.lastModified, userprofile.firstName, userprofile.lastName,image.filename as imageName');
			$this->db->from('user');
			$this->db->join('userprofile', 'user.userID = userprofile.userID');
			$this->db->join('image', 'userprofile.profPicID = image.imageID');
			
			// if user was a caretaker
			if($searcherRoleID == 2) {
				$this->db->join('classroom', 'user.classroomID = classroom.classRoomID');
				$this->db->where('classroom.caretakerID', $searcherUserID);
			} else if ($searcherRoleID == 3) {	// if user was a parent
				$this->db->where('userprofile.parentID', $searcherUserID);
			}
			$this->db->where('user.roleID', 4);
			$this->db->like('userprofile.lastName', $searchString, 'after');
			$this->db->limit(10);

			$query=$this->db->get();
			$result=$query->result_array();
			
			return count($result) ? $result : FALSE;
		}
		
		public function getParentsCaretakers($searchString, $searcherUserID, $searcherRoleID)
		{
			// $assigneeID = isset($data['userID']);
			$this->db->select('user.userID, user.roleID, user.lastModified, userprofile.firstName, userprofile.lastName,image.filename as imageName');
			$this->db->from('user');
			$this->db->join('userprofile', 'user.userID = userprofile.userID');
			$this->db->join('image', 'userprofile.profPicID = image.imageID');
			
			// $this->db->where('user.roleID', 2);
			$this->db->or_where_in('user.roleID', array(2,3));
			$this->db->like('userprofile.lastName', $searchString, 'after');
			$this->db->limit(10);

			$query=$this->db->get();
			$result=$query->result_array();
			
			return count($result) ? $result : FALSE;
		}
		
		public function insertOrUpdateSpiceForm($data)
		{
			// $assigneeID = isset($data['userID']);
			$this->db->select('*');
			$this->db->from('spiceformdetail');
			
			$this->db->where(array('childID' => $data['childid'],
												 'addedOn >=' => date('Y-m-d',strtotime('last sunday'))));
			
			$query=$this->db->get();
			$result=$query->result_array();
			
			$output = array(
				'mode' => '',
				'flag' => ''
			);
			// if a spice form exists for the child in the current week
			if(count($result) > 0) {
				$output['mode'] = 'update';
				
				$inputDetails = array(
					'caretakerID' => $data['caretakerID'],
					'social_detail' => $data['social'],
					'physical_detail' => $data['physical'],
					'intellectual_detail' => $data['intellectual'],
					'creative_detail' => $data['creative'],
					'emotional_detail' => $data['emotional'],
					// 'addedOn' => date('Y-m-d h:i:s', time())
					'addedOn' => date('Y-m-d', time())
				);
			
				$this->db->where(array('childID' => $data['childid'],
													 'addedOn >=' => date('Y-m-d',strtotime('last sunday'))));
													 
				$this->db->update('spiceformdetail', $inputDetails); 
				$output['flag'] = $this->db->affected_rows() > 0;
				
			} else {
				$output['mode'] = 'insert';
				
				$inputDetails = array(
					'childID' => $data['childid'],
					'caretakerID' => $data['caretakerID'],
					'social_detail' => $data['social'],
					'physical_detail' => $data['physical'],
					'intellectual_detail' => $data['intellectual'],
					'creative_detail' => $data['creative'],
					'emotional_detail' => $data['emotional']
				);
			   $profile_query = $this->db->insert('spiceformdetail', $inputDetails); 
			   
			   $output['flag'] = true;
			}
			
			return $output;
		}
		
		public function saveActivityDetail($data)
		{
			// $assigneeID = isset($data['userID']);
			$this->db->select('*');
			$this->db->from('spiceactivitydetail');
			
			$this->db->where(array('childID' => $data['spiceChildID'],
												 'spiceactivityID' => $data['spiceActivityID'],
												 'addedOn >=' => date('Y-m-d',strtotime('last sunday'))));
			
			$query=$this->db->get();
			$result=$query->result_array();
			
			$output = array(
				'mode' => '',
				'flag' => ''
			);
			// if activity detail exists for the child in the current week
			if(intval($data['rating']) == -1 ) {
				$output['mode'] = 'delete';
				
				$this->db->where(array('childID' => $data['spiceChildID'],
												 'spiceactivityID' => $data['spiceActivityID'],
												 'addedOn >=' => date('Y-m-d',strtotime('last sunday'))));
													 
				$this->db->delete('spiceactivitydetail'); 
				$output['flag'] = $this->db->affected_rows() > 0;
				
			} else {
				if(intval($data['rating']) < -1 ) $data['rating'] = 0;
				else if(intval($data['rating']) > 10 ) $data['rating'] = 10;
					
					
				if(count($result) > 0) {
					
					$output['mode'] = 'update';
					
					$inputDetails = array(
						'rating' => $data['rating'],
						'comments' => $data['comment']
					);
				
					$this->db->where(array('childID' => $data['spiceChildID'],
													 'spiceactivityID' => $data['spiceActivityID'],
													 'addedOn >=' => date('Y-m-d',strtotime('last sunday'))));
														 
					$this->db->update('spiceactivitydetail', $inputDetails); 
					$output['flag'] = $this->db->affected_rows() > 0;
					
				} else {
					$output['mode'] = 'insert';
					
					$inputDetails = array(
						'childID' => $data['spiceChildID'],
						'caretakerID' => $data['caretakerID'],
						'spiceactivityID' => $data['spiceActivityID'],
						'rating' => $data['rating'],
						'comments' => $data['comment']
					);
					
				   $profile_query = $this->db->insert('spiceactivitydetail', $inputDetails); 
				   
				   $output['flag'] = true;
				}	
			}
			
			return $output;
		}
		
		public function getNotifications($data) {
			
			
			$this->db->select('user.userID');
			$this->db->from('user');
			$this->db->join('classroom', 'user.classRoomID = classroom.classroomID'); 
			$this->db->where(array('user.roleID' => 4));
			if($data['currentUserRoleID'] != '1' ) {
				$this->db->where(array('classroom.caretakerID' => $data['caretakerID']));
			}
			
			
			$query=$this->db->get();
			$output['totalAssignedChildren'] = $query->num_rows();

			$this->db->select('user.userID');
			$this->db->from('user');
			$this->db->join('classroom', 'user.classRoomID = classroom.classroomID'); 
			$this->db->join('spiceformdetail', 'user.userID = spiceformdetail.childID'); 
			$this->db->where(array('user.roleID' => 4));
			if($data['currentUserRoleID'] != '1' ) {
				$this->db->where(array('classroom.caretakerID' => $data['caretakerID']));
			}
			$this->db->where(array('spiceformdetail.addedOn >=' => date('Y-m-d',strtotime('last sunday'))));
			$this->db->where(array('spiceformdetail.social_detail <>' => '',
												'spiceformdetail.physical_detail <>' => '',
											   'spiceformdetail.intellectual_detail <>' => '',
											   'spiceformdetail.creative_detail <>' => '',
											   'spiceformdetail.emotional_detail <>' => ''));
			
			
			
			$query=$this->db->get();
			$output['numCompletedThisWeek'] = $query->num_rows();
			
			
			
			if($data['currentUserRoleID'] == '1' ) {
			
				$this->db->select('user.userID');
				$this->db->from('user');
				$this->db->or_where('user.roleID', 3);
				$this->db->or_where('user.roleID', 2);
				// $this->db->where(array('emailVerified' => 1, 'adminVerified' => 0));
				$this->db->where(array('adminVerified' => 0));
				
				$query=$this->db->get();
				$output['newUsers'] = $query->num_rows();
			}
			
			return $output;
			// if($data['currentUserRoleID'] != 1 ) {
				// $this->db->select('count(*) as totalChildren');
			// }			
		}
		
		public function getActivities() {
			$this->db->select('spiceactivity.name as spiceActivityName,
							   spiceactivity.spiceactivityID,
							   spicedevelopment.name as spiceDevelopmentName');
			$this->db->from('spiceactivity');
			$this->db->join('spicedevelopment', 'spiceactivity.spicedevelopmentID = spicedevelopment.spicedevelopmentID'); 
			$query=$this->db->get();
			$result=$query->result_array();
			return $result;
		}
		
		public function addActivity($data) {
			$this->db->insert('spiceactivity', $data);
			$output['flag'] = $this->db->affected_rows() > 0;

			if($output['flag']) {
				$output['id'] = $this->db->insert_id();
			}
			return $output;
		}
		
		public function deleteActivity($data) {
			$this->db->where('spiceactivityID', $data['activityID']);
			$this->db->delete('spiceactivity');
			return $this->db->affected_rows() > 0;
		}
		
		public function addVideo($data)
		{
			$this->db->insert('video', $data);
			return $this->db->affected_rows() > 0;
		}
		
		public function getVideoById($videoID)
		{
			$this->db->select('filename');
			$this->db->where('videoID', $videoID);
			$this->db->from('video');
			$query=$this->db->get();
			$result=$query->result_array();
			
			return $result;
		}
		
		public function getGuidanceVideos($data) {
			$this->db->select('*');
			$this->db->from('video');
			if($data['roleID'] != 1) {
				$this->db->where('roleID', $data['roleID']);
			}

			$query=$this->db->get();
			$result=$query->result_array();
			return $result;
		}
		
		public function deleteGuidanceVideo($data) {
			$this->db->where('videoID', $data['videoID']);
			$this->db->delete('video');
			return $this->db->affected_rows() > 0;
		}
		
		public function addGame($data) {
			$this->db->insert('game', $data);
			return $this->db->affected_rows() > 0;
		}
		
		public function getGameListings($data) {
			$this->db->select('game.gameID,
							   game.name,
							   game.description
							   ');
			$this->db->from('game');
			
			if($this->session->userdata('childid') != null) {
				$this->db->join('userprofile', 'userprofile.levelID = game.levelID'); 
				$this->db->where('userprofile.userID', $this->session->userdata('childid'));
			}
			$query=$this->db->get();
			$result=$query->result_array();
			return $result;
		}
		
		public function getGameData($gameID, $childID) {
			
			
			$this->db->select('game.gameID,
							   game.name,
							   game.description,
							   game.levelID
							   ');
			$this->db->from('game');
			
			if($childID != null) {
				$this->db->join('userprofile', 'game.levelID = userprofile.levelID'); 
				$this->db->where('userprofile.userID', $childID);
			}
			$this->db->where('game.gameID', $gameID);
			
			// if($data['roleID'] == 4) {
				// $this->db->join('userprofile', 'userprofile.levelID = game.levelID'); 
			// }
			$query=$this->db->get();
			$result=$query->result_array();
			return $result;
		}
		
		public function deleteGame($data) {
			$this->db->where('gameID', $data['gameID']);
			$this->db->delete('game');
			return $this->db->affected_rows() > 0;
		}
		
		public function addGameHistoryData($data) {
			
			if($data['childID'] != null ) {
				// select level id
				$input = array(
					'userID' => $data['childID'],
					'gameID' => $data['gameID'],
					'message' => $data['message']
				);
				$this->db->insert('gamehistory', $input);
			} else {
				$input = array(
					'userID' => $data['userID'],
					'gameID' => $data['gameID'],
					'message' => $data['message']
				);
				$this->db->insert('gamehistory', $input);
			}
			
			return $this->db->affected_rows() > 0;
		}
		
		public function getConversations($userID) {
			$this->db->select('*, conversationID, userID2 as userID');
			$this->db->from('conversation');
			$this->db->join('userprofile', 'userprofile.userID = conversation.userID2'); 
			$this->db->join('image', 'image.imageID = userprofile.profPicID'); 
			// $this->db->join('conversation_content', 'image.imageID = userprofile.profPicID'); 
			$this->db->where('userID1', $userID);

			$query1 = $this->db->get()->result();
			
			$this->db->select('*, conversationID, userID1 as userID');
			$this->db->from('conversation');
			$this->db->join('userprofile', 'userprofile.userID = conversation.userID1'); 
			$this->db->join('image', 'image.imageID = userprofile.profPicID'); 
			$this->db->where('userID2', $userID);
			$query2 = $this->db->get()->result();

			$query = array_merge($query1, $query2);
			
			return $query;
		}
	}
	