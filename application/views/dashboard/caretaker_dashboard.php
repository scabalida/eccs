<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<meta name="keywords" content="Teaching Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
	Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
	<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
    <title>Teaching an Education School Category Flat Bootstrap Responsive Website Template | Home </title>
    <!-- Bootstrap Core CSS -->
	<link href="<?php echo base_url();?>css/style.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>css/bootstrap.css" rel="stylesheet">
	<link href='//fonts.googleapis.com/css?family=Capriola' rel='stylesheet' type='text/css'>
	<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <!-- MetisMenu CSS -->
    <link href="<?php echo base_url();?>css/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url();?>css/admin.css" rel="stylesheet">
	<link href="<?php echo base_url();?>css/animate.min.css" rel="stylesheet">
    <!-- Morris Charts CSS -->
    <link href="<?php echo base_url();?>css/morris.css" rel="stylesheet">
	 <!-- DataTables CSS -->
    <link href="<?php echo base_url();?>css/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="<?php echo base_url();?>css/dataTables.responsive.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="<?php echo base_url();?>css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>
<?php
// echo '<pre>';
	// print_r($this->session->all_userdata());
	// print_r($this->session->userdata('userID'));
// echo '</pre>';
?>
<!-- Spice activities modal -->
	<div class="modal fade" id="modal_spiceActivities" tabindex="1" role="dialog" aria-labelledby="modal_spiceActivities">
	
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title"><i class="glyphicon glyphicon-lock"></i> Activities </h4>
		  </div>
		  <div class="modal-body">
				<div id="spice-activities-container">
					<div id="social_activities">
						<h3>Social Activities</h3>
						<div class="table-responsive">
							<table class="table table-bordered table-hover table-striped">
								<thead>
									<tr>
										<th>Activity</th>
										<th>Rating</th>
										<th>Comments</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody id="table-activities-social">
								</tbody>
							</table>
						</div>
					</div>
					<div id="physical_activities">
						<h3>Physical Activities</h3>
						<div class="table-responsive">
							<table class="table table-bordered table-hover table-striped">
								<thead>
									<tr>
										<th>Activity</th>
										<th>Rating</th>
										<th>Comments</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody id="table-activities-physical">
								</tbody>
							</table>
						</div>
					</div>
					<div id="intellectual_activities">
						<h3>Intellectual Activities</h3>
						<div class="table-responsive">
							<table class="table table-bordered table-hover table-striped">
								<thead>
									<tr>
										<th>Activity</th>
										<th>Rating</th>
										<th>Comments</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody id="table-activities-intellectual">
								</tbody>
							</table>
						</div>							
					</div>
					<div id="creative_activities">
						<h3>Creative Activities</h3>
						<div class="table-responsive">
							<table class="table table-bordered table-hover table-striped">
								<thead>
									<tr>
										<th>Activity</th>
										<th>Rating</th>
										<th>Comments</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody id="table-activities-creative">
								</tbody>
							</table>
						</div>							
					</div>
					<div id="emotional_activities">
						<h3>Emotional Activities</h3>
						<div class="table-responsive">
							<table class="table table-bordered table-hover table-striped">
								<thead>
									<tr>
										<th>Activity</th>
										<th>Rating</th>
										<th>Comments</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody id="table-activities-emotional">
								</tbody>
							</table>
						</div>							
					</div>
				</div>
			</br>
		  </div>
		</div>
		
	  </div>
	</div>
<!-- Spice details modal -->
	<div class="modal fade" id="modal_spiceForm" tabindex="1" role="dialog" aria-labelledby="modal_spiceForm">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title"><i class="glyphicon glyphicon-lock"></i> Spice Form </h4>
		  </div>
			<form id="spiceForm" data-toggle="validator" role="form">
			  <div class="modal-body">
						<div class="form-group has-feedback" >
							<label>Name:</label>
							<input type="text" id="spiceform_input_idchild" name="childname" class="form-control" disabled="" >
							<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
							<div class="help-block with-errors"></div>
						</div>
						
						<div class="form-group has-feedback" >
							<label>Social Development Details:</label>
							<input type="text" id="spiceform_input_socialdev" name="social" class="form-control">
							<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
							<div class="help-block with-errors"></div>
						</div>
						<div class="form-group has-feedback">
							<label>Physical Development Details:</label>
							<input type="text" id="spiceform_input_physicaldev" name="physical" class="form-control">
							<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
							<div class="help-block with-errors"></div>
						</div>
						<div class="form-group has-feedback">
							<label>Intellectual Development Details:</label>
							<input type="text" id="spiceform_input_intellectualdev" name="intellectual" class="form-control">
							<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
							<div class="help-block with-errors"></div>
						</div>
						<div class="form-group has-feedback">
							<label>Creative Development Details:</label>
							<input type="text" id="spiceform_input_creativedev" name="creative" class="form-control">
							<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
							<div class="help-block with-errors"></div>
						</div>
						<div class="form-group has-feedback">
							<label>Emotional Development Details:</label>
							<input type="text" id="spiceform_input_emotionaldev" name="emotional" class="form-control">
							<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
							<div class="help-block with-errors"></div>
						</div>
				</br>
			  </div>
			  <div class="modal-footer">
				<button id="btn-spiceform-submit" class="btn btn-lg btn-warning btn-block " type="submit">SAVE</button>
			  </div>
			</form>

		</div>
		
	  </div>
	</div>

<!-- User Settings Modal -->
	<div class="modal fade" id="userSetting" tabindex="1" role="dialog" aria-labelledby="userSetting">
	
	  <div class="modal-dialog" id="widthLogModal" role="document">
		<div class="modal-content">
		
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="loginModal"><i class="glyphicon glyphicon-lock"></i> User Settings </h4>
		  </div>
		  <div class="modal-body">
				<div class="alert alert-danger" id="errorUserSetMsg" role="alert">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
					<strong><p>Your passwords do not match. Please try again.</p></strong>
				</div>
				<div class="alert alert-success" id="succUserSetMsg" role="alert">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
					<strong><p>Successfully changed password.</p></strong>
				</div>
				<form id="userSettingForm" data-toggle="validator" role="form">
					<div class="form-group has-feedback" >
						<input type="text" id="usernameSetting" name="username" class="form-control" value="<?php echo $this->session->userdata('userName'); ?>" disabled>
						<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
						<div class="help-block with-errors"></div>
					</div>
					<div class="form-group has-feedback">
						<input type="password" id="settingNewPassword" class="form-control" placeholder="New Password" required="">
						<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
						<div class="help-block with-errors"></div>
					</div>
					<div class="form-group has-feedback">
						<input type="password" id="settingReNewPassword" class="form-control" placeholder="Re-Enter New Password" required="">
						<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
						<div class="help-block with-errors"></div>
					</div>
			</br>
		  </div>
		  <div class="modal-footer">
			<button id="userSettingBtn" class="btn btn-lg btn-warning btn-block " type="submit">SAVE</button>
			</form>
		  </div>
		</div>
		
	  </div>
	</div>
	
	<div class="modal fade" id="childGameModal" tabindex="1" role="dialog" aria-labelledby="childGameModal">
	  <div class="modal-dialog"  role="document">
		<div class="modal-content">
			<form action="<?php echo base_url().'dashboard'; ?>" method="post" >
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="loginModal">Choose a child</h4>
			  </div>
			  <div class="modal-body text-center">
				<select name="childid" id="childGameSelect" class="form-control" required="">
				</select>
			  </div>
			  <div class="modal-footer">
				<button class="btn btn-lg btn-warning btn-block " type="submit">Play as this child</button>
			  </div>
			</form>
		</div>
		
	  </div>
	</div>	

<body>
		
    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header navbar-left">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<h1 style="font-family:'Capriola', sans-serif;margin:0; "><a href="<?php echo base_url('home');?>" style="padding:0; font-size:1em; color:#fff" class="navbar-brand" href="<?php echo base_url('home');?>"><span>T</span>eaching</a></h1>
					</div>

            <ul class="nav navbar-top-links navbar-right">
                <li><a href="#" data-toggle="modal" data-target="#userSetting" class="u_settings"><i class="fa fa-gear fa-fw"></i></a>
            </ul>

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
						<li>
                            <a href="<?php echo base_url('home');?>"><i class="fa fa-home fa-fw"></i> Home</a>
                        </li>
                        <li data-targetid="child-dashboard-container">
                            <a href="javascript:void(0)"><i class="fa fa-dashboard fa-fw"></i>Child Dashboard</a>
                        </li>
                        <li class="active" data-targetid="parent-caretaker-dashboard-container">
                            <a href="javascript:void(0)"><i class="fa fa-dashboard fa-fw"></i>Caretaker Dashboard</a>
                        </li>
						<li data-targetid="gamelistings-container">
                            <a href="javascript:void(0);"><i class="fa fa-th-list fa-fw"></i> Game Listings</a>
                        </li>
						<li data-targetid="guidance-container">
                            <a href="javascript:void(0);"><i class="fa fa-forward fa-fw"></i> Guidance </a>
                        </li>
						<li data-gameplay="">
                            <a href="javascript:void(0);"><i class="fa fa-gamepad fa-fw"></i> Gameplay </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>curriculum"><i class="fa fa-book fa-fw"></i> Curriculum</a>
                        </li>
                        <li data-targetid="messages-container">
                            <a href="javascript:void(0);"><i class="fa fa-book fa-fw"></i> Messages</a>
                        </li>
						
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
			<div id="child-dashboard-container">
				<div class="row">
					<div class="col-lg-12">
						<h1 class="page-header">Child Monitoring Dashboard</h1>
					</div>
					<!-- /.col-lg-12 -->
				</div>
				
				<div class="row">
					<div class="col-lg-8">
						<div class="panel panel-default">
							<div class="panel-heading">
								<i class="fa fa-bar-chart-o fa-fw"></i> Child Profile
							</div>
							<!-- /.panel-heading -->
							<div class="panel-body panel-profile">
								<div id="child-profile-info" class="row">
									<div class="col-lg-3 col-md-4 profile-info">
									<!--
										<img src="<?php echo base_url(); ?>images/users/profile_123.jpg" >
										-->
									</div>
									<div class="col-lg-9 col-md-8 profile-info">
									<!--
										<label>Janine A</label>
										<label>Child of Janina Apple</label>
										<label>5 years old</label>
										<label>Contact # 123456789</label>
										<label>Joined on January 1, 2017</label>
										-->
									</div>
								</div>								
								<div>
									<br>
									<label>Details for this week:</label>
									<br>
								</div>
								<div class="row profile-spice">
									<div class="col-lg-6 col-md-6">
										<div class="panel panel-primary">
											<div class="panel-heading">
												<div class="row">
													<div class="col-xs-3">
														<i class="fa fa-comments fa-5x"></i>
													</div>
													<div class="col-xs-9 text-right">
														<div class="huge">Social</div>
														<div id="average_social">Average 10</div>
														<div id="activitiesdone-social">0 out of 5 done</div>
														<!-- <div id="average_social_done">Average 10</div> -->
													</div>
												</div>
											</div>
											<a href="javascript:void(0);" data-targetid="social_activities" class="view-activities-modal">
												<div class="panel-footer">
													<span class="pull-left">View Details</span>
													<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
													<div class="clearfix"></div>
												</div>
											</a>
										</div>
									</div>
									<div class="col-lg-6 col-md-6">
										<div class="panel panel-green">
											<div class="panel-heading">
												<div class="row">
													<div class="col-xs-3">
														<i class="fa fa-soccer-ball-o fa-5x"></i>
													</div>
													<div class="col-xs-9 text-right">
														<div class="huge">Physical</div>
														<div id="average_physical">Average 8.8</div>
														<div id="activitiesdone-physical">0 out of 5 done</div>
													</div>
												</div>
											</div>
											<a href="javascript:void(0);" data-targetid="physical_activities" class="view-activities-modal">
												<div class="panel-footer">
													<span class="pull-left">View Details</span>
													<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
													<div class="clearfix"></div>
												</div>
											</a>
										</div>
									</div>
								</div>
								<div class="row profile-spice">
									<div class="col-lg-6 col-md-6">
										<div class="panel panel-yellow">
											<div class="panel-heading">
												<div class="row">
													<div class="col-xs-3">
														<i class="fa fa-book fa-5x"></i>
													</div>
													<div class="col-xs-9 text-right">
														<div class="huge">Intellectual</div>
														<div id="average_intellectual">Average 5.5</div>
														<div id="activitiesdone-intellectual">0 out of 5 done</div>
													</div>
												</div>
											</div>
											<a href="javascript:void(0);" data-targetid="intellectual_activities" class="view-activities-modal">
												<div class="panel-footer">
													<span class="pull-left">View Details</span>
													<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
													<div class="clearfix"></div>
												</div>
											</a>
										</div>
									</div>
									<div class="col-lg-6 col-md-6">
										<div class="panel panel-red">
											<div class="panel-heading">
												<div class="row">
													<div class="col-xs-3">
														<i class="fa fa-paint-brush fa-5x"></i>
													</div>
													<div class="col-xs-9 text-right">
														<div class="huge">Creative</div>
														<div id="average_creative">Average 6.7</div>
														<div id="activitiesdone-creative">0 out of 5 done</div>
													</div>
												</div>
											</div>
											<a href="javascript:void(0);" data-targetid="creative_activities" class="view-activities-modal">
												<div class="panel-footer">
													<span class="pull-left">View Details</span>
													<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
													<div class="clearfix"></div>
												</div>
											</a>
										</div>
									</div>										
								</div>									
								<div class="row profile-spice">
									<div class="col-lg-6 col-md-6">
										<div class="panel panel-purple">
											<div class="panel-heading">
												<div class="row">
													<div class="col-xs-3">
														<i class="fa fa-smile-o fa-5x"></i>
													</div>
													<div class="col-xs-9 text-right">
														<div class="huge">Emotional</div>
														<div id="average_emotional">Average 4.8</div>
														<div id="activitiesdone-emotional">0 out of 5 done</div>
													</div>
												</div>
											</div>
											<a href="javascript:void(0);" data-targetid="emotional_activities" class="view-activities-modal">
												<div class="panel-footer">
													<span class="pull-left">View Details</span>
													<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
													<div class="clearfix"></div>
												</div>
											</a>
										</div>
									</div>
									<div class="col-lg-6 col-md-6">
										<div class="panel panel-aqua">
											<div class="panel-heading">
												<div class="row">
													<div class="col-xs-3">
														<i class="fa fa-plus-circle fa-5x"></i>
													</div>
													<div class="col-xs-9 text-right">
														<div class="huge">&nbsp </div>
														<br>
														<div>Missing details?</div>
													</div>
												</div>
											</div>
											<a id="btn_modal_spiceform" data-target="#modal_spiceForm">
												<div class="panel-footer">
													<span class="pull-left">Add spice details</span>
													<span class="pull-right"><i class="fa fa-plus-circle"></i></span>
													<div class="clearfix"></div>
												</div>
											</a>
										</div>
									</div>									
								</div>		
							</div>
							<!-- /.panel-body -->
						</div>
					</div>
					<!-- /.col-lg-8 -->
					<div class="col-lg-4">
						<div class="panel panel-default">
							<div class="panel-heading">
								<i class="fa fa-bell fa-fw"></i> Notifications
							</div>
							<!-- /.panel-heading -->
							<div class="panel-body">
								<div id="panel-notifications" class="list-group">
								</div>
							</div>
							<!-- /.panel-body -->
						</div>					
						<div class="panel panel-default panel-assigned">
							<div class="panel-heading">
								<i class="fa fa-bell fa-fw"></i> Children
							</div>
							<!-- /.panel-heading -->
							<div class="panel-body assigned">
								<form id="form-search-assigned-users" >
									<div class="input-group">
										<input name="name" type="text" class="form-control input-sm" placeholder="Enter name here..." />
										<span class="input-group-btn">
											<button class="btn btn-warning btn-sm" type="submit">
												Search
											</button>
										</span>
									</div>
								</form>
								<div id="assigned-list" class="list-group assigned-list">
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-lg-8">
						<div class="panel panel-default">
							<div class="panel-heading">
								<i class="fa fa-bar-chart-o fa-fw"></i> SPICE Chart
								<div class="pull-right">
									<div class="btn-group">
										<button id="btn-chart" type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
											Line Chart
											<span class="caret"></span>
										</button>
										<ul id="chart-dropdown" class="dropdown-menu pull-right" role="menu">
											<li data-action="linechart"><a href="javascript:void(0)">Line Chart</a></li>
											<li data-action="barchart"><a href="javascript:void(0)">Bar Chart</a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="panel-body">
								<div id="morris-line-chart"></div>
								<div style="display:none;" id="morris-bar-chart"></div>
							</div>
						</div>
						
						<!-- /.panel -->
						<div class="panel panel-default">
							<div class="panel-heading">
								<i class="fa fa-clock-o fa-fw"></i> Latest Activities
							</div>
							<!-- /.panel-heading -->
							
							<div class="panel-body">
								<ul id="activity-timeline" class="timeline">
									<li>
										<div class="timeline-badge"><i class="fa fa-check"></i>
										</div>
										<div class="timeline-panel">
											<div class="timeline-heading">
												<h4 class="timeline-title">Just played tetris!</h4>
												<p><small class="text-muted"><i class="fa fa-clock-o"></i> 11 hours ago</small>
												</p>
											</div>
											<div class="timeline-body">
												<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Libero laboriosam dolor perspiciatis omnis exercitationem. Beatae, officia pariatur? Est cum veniam excepturi. Maiores praesentium, porro voluptas suscipit facere rem dicta, debitis.</p>
											</div>
										</div>
									</li>
									<li class="timeline-inverted">
										<div class="timeline-badge warning"><i class="fa fa-credit-card"></i>
										</div>
										<div class="timeline-panel">
											<div class="timeline-heading">
												<h4 class="timeline-title">Scored a 9.8 from a social activity!</h4>
											</div>
											<div class="timeline-body">
												<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem dolorem quibusdam, tenetur commodi provident cumque magni voluptatem libero, quis rerum. Fugiat esse debitis optio, tempore. Animi officiis alias, officia repellendus.</p>
												<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium maiores odit qui est tempora eos, nostrum provident explicabo dignissimos debitis vel! Adipisci eius voluptates, ad aut recusandae minus eaque facere.</p>
											</div>
										</div>
									</li>
									<li>
										<div class="timeline-badge danger"><i class="fa fa-bomb"></i>
										</div>
										<div class="timeline-panel">
											<div class="timeline-heading">
												<h4 class="timeline-title">Lorem ipsum dolor</h4>
											</div>
											<div class="timeline-body">
												<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellendus numquam facilis enim eaque, tenetur nam id qui vel velit similique nihil iure molestias aliquam, voluptatem totam quaerat, magni commodi quisquam.</p>
											</div>
										</div>
									</li>
									<li class="timeline-inverted">
										<div class="timeline-panel">
											<div class="timeline-heading">
												<h4 class="timeline-title">Lorem ipsum dolor</h4>
											</div>
											<div class="timeline-body">
												<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptates est quaerat asperiores sapiente, eligendi, nihil. Itaque quos, alias sapiente rerum quas odit! Aperiam officiis quidem delectus libero, omnis ut debitis!</p>
											</div>
										</div>
									</li>
									<li>
										<div class="timeline-badge info"><i class="fa fa-save"></i>
										</div>
										<div class="timeline-panel">
											<div class="timeline-heading">
												<h4 class="timeline-title">Lorem ipsum dolor</h4>
											</div>
											<div class="timeline-body">
												<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis minus modi quam ipsum alias at est molestiae excepturi delectus nesciunt, quibusdam debitis amet, beatae consequuntur impedit nulla qui! Laborum, atque.</p>
												<hr>
												<div class="btn-group">
													<button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">
														<i class="fa fa-gear"></i> <span class="caret"></span>
													</button>
													<ul class="dropdown-menu" role="menu">
														<li><a href="#">Action</a>
														</li>
														<li><a href="#">Another action</a>
														</li>
														<li><a href="#">Something else here</a>
														</li>
														<li class="divider"></li>
														<li><a href="#">Separated link</a>
														</li>
													</ul>
												</div>
											</div>
										</div>
									</li>
									<li>
										<div class="timeline-panel">
											<div class="timeline-heading">
												<h4 class="timeline-title">Lorem ipsum dolor</h4>
											</div>
											<div class="timeline-body">
												<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi fuga odio quibusdam. Iure expedita, incidunt unde quis nam! Quod, quisquam. Officia quam qui adipisci quas consequuntur nostrum sequi. Consequuntur, commodi.</p>
											</div>
										</div>
									</li>
									<li class="timeline-inverted">
										<div class="timeline-badge success"><i class="fa fa-graduation-cap"></i>
										</div>
										<div class="timeline-panel">
											<div class="timeline-heading">
												<h4 class="timeline-title">Lorem ipsum dolor</h4>
											</div>
											<div class="timeline-body">
												<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deserunt obcaecati, quaerat tempore officia voluptas debitis consectetur culpa amet, accusamus dolorum fugiat, animi dicta aperiam, enim incidunt quisquam maxime neque eaque.</p>
											</div>
										</div>
									</li>
								</ul>
							</div>
							<!-- /.panel-body -->
						</div>
						<!-- /.panel -->
					</div>
					<!-- /.col-lg-8 -->
					<div class="col-lg-4">

						<!-- /.panel -->
						<!-- /.panel -->
						<!-- /.panel .chat-panel -->
					</div>
					<!-- /.col-lg-4 -->
				</div>
				<!-- /.row -->
			</div>

			<div id="parent-caretaker-dashboard-container" style="display:none;">
				<div class="row">
					<div class="col-lg-12">
						<h1 class="page-header">Caretaker Dashboard</h1>
					</div>
					<!-- /.col-lg-12 -->
				</div>
				
				<div class="row">
					<div class="col-lg-8">
						<div class="panel panel-default">
							<div class="panel-heading">
								<i class="fa fa-bar-chart-o	 fa-fw"></i> Caretaker Profile
							</div>
							<!-- /.panel-heading -->
							<div class="panel-body panel-profile">
								<div id="parent-caretaker-profile-info" class="row">
									<div class="col-lg-3 col-md-4 profile-info">
									</div>
									<div class="col-lg-9 col-md-8 profile-info">
									</div>
								</div>								
								<div>
									<br>
									<label>Assigned Children:</label>
									<br>
								</div>
								<div class="row profile-spice">
									<div class="col-lg-12">
											<div id="parent-caretaker-table-children" class="table-responsive">
											</div>
									</div>
								</div>
							</div>
							<!-- /.panel-body -->
						</div>
					</div>
					<!-- /.col-lg-8 -->
					<div class="col-lg-4">
						<div class="panel panel-default">
							<div class="panel-heading">
								<i class="fa fa-bell fa-fw"></i> Notifications
							</div>
							<!-- /.panel-heading -->
							<div id="parent-caretaker-notification-panel" class="panel-body">
								<div class="list-group">
									<a href="#" class="list-group-item">
										<i class="fa fa-pencil fa-fw"></i> 3 needs spice input
										<span class="pull-right text-muted small"><em>4 minutes ago</em>
										</span>
									</a>
									<a href="javascript:void(0)" class="btn-newusers list-group-item">
										<i class="fa fa-user-plus fa-fw"></i> 5 new users
										<span class="pull-right text-muted small"><em>12 minutes ago</em>
										</span>
									</a>
								</div>
							</div>
							<!-- /.panel-body -->
						</div>								
					</div>
					<!-- /.col-lg-4 -->
				</div>

			</div>
			
			<div id="gamelistings-container" style="display:none;">
				<div class="row">
					<div class="col-lg-12 col-md-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="row">
									<div class="col-lg-6 col-md-6">
										<h2>Available Games for you</h2>
									</div>
									<div class="col-lg-6 col-md-6">
										<a id="gamelisting-modal-add" href="" class="pull-right btn btn-success" data-toggle="modal" data-target="#modal_add-game">Add a game</a>
									</div>
								</div>
							</div>
							<!-- /.panel-heading -->
							<div class="panel-body panel-guidance">
								<div id="game-list" class="row list-group">
									<!--
									<div class="item  col-xs-4 col-lg-4">
										<div class="thumbnail">
											<img class="group list-group-image" src="http://placehold.it/400x250/000/fff" alt="" />
											<div class="caption">
												<h4 class="group inner list-group-item-heading">
													Product title</h4>
												<p class="group inner list-group-item-text">
													Product description... Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
													sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
												<div class="row">
													<div class="col-xs-12 col-md-6">
														<p class="lead">
															$21.000</p>
													</div>
													<div class="col-xs-12 col-md-6">
														<a class="btn btn-success" href="http://www.jquery2dotnet.com">Add to cart</a>
													</div>
												</div>
											</div>
										</div>
									</div>
									-->
								</div>
							</div>
							<!-- /.panel-body -->
						</div>
					</div>
				</div>			
			</div>
		
			<div id="guidance-container" style="display:none; padding-top:20px;">
				<div class="row">
					<div class="col-lg-12 col-md-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="row">
									<div class="col-lg-6 col-md-6">
										<h2>Guidance</h2>
									</div>
									<div class="col-lg-6 col-md-6">
										<a href="" class="pull-right btn btn-success" data-toggle="modal" data-target="#modal_add-guidance">Add a Video</a>
									</div>
								</div>
							</div>
							<!-- /.panel-heading -->
							<div class="panel-body panel-guidance">
								<div id="guidance-videos-list" class="row list-group">
									<!--
									<div class="item  col-xs-4 col-lg-4">
										<div class="thumbnail">
											<img class="group list-group-image" src="http://placehold.it/400x250/000/fff" alt="" />
											<div class="caption">
												<h4 class="group inner list-group-item-heading">
													Product title</h4>
												<p class="group inner list-group-item-text">
													Product description... Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
													sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
												<div class="row">
													<div class="col-xs-12 col-md-6">
														<p class="lead">
															$21.000</p>
													</div>
													<div class="col-xs-12 col-md-6">
														<a class="btn btn-success" href="http://www.jquery2dotnet.com">Add to cart</a>
													</div>
												</div>
											</div>
										</div>
									</div>
									-->
								</div>
							</div>
							<!-- /.panel-body -->
						</div>
					</div>
				</div>
			</div>
			<div id="messages-container" class="messages-padding" style="display:none;">
				<div class="messages-recent-chats">
					<div id="ana">
						<div class="input-group">
						  <input type="text" class="form-control" placeholder="Search for...">
						  <span class="input-group-btn">
							<button class="btn btn-default" type="button">Go!</button>
						  </span>
						</div><!-- /input-group -->
						<div id="chat-people-container">
							<div class="profil">
								<div class="pfoto">
									<img src="http://s3-us-west-2.amazonaws.com/s.cdpn.io/510970/profile/profile-512_1.jpg">
								</div>

								<div class="mesaj">
									<b><span>Yasin Softaoğlu</span> <span class="right">19:06</span>
									<br>
									<span>İyi akşamlar napıyosun :)</span></b>
								</div>
								<div class="temizle"></div>
							</div>
							<div class="profil">
								<div class="pfoto">
									<img src="http://s3-us-west-2.amazonaws.com/s.cdpn.io/510970/profile/profile-512_1.jpg">
								</div>

								<div class="mesaj">
									<b><span>Yasin Softaoğlu</span> <span class="right">19:06</span>
									<br>
									<span>İyi akşamlar napıyosun :)</span></b>
								</div>
								<div class="temizle"></div>
							</div>
							<div class="profil">
								<div class="pfoto">
									<img src="http://s3-us-west-2.amazonaws.com/s.cdpn.io/510970/profile/profile-512_1.jpg">
								</div>

								<div class="mesaj">
									<b><span>Yasin Softaoğlu</span> <span class="right">19:06</span>
									<br>
									<span>İyi akşamlar napıyosun :)</span></b>
								</div>
								<div class="temizle"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="chatbox">
					<div class="panel-heading">
						<a href="#" class="chatMinimize" onclick="return false"><span>Özgür Gürbüz</span></a>
						<a href="#" class="chatClose" onclick="return false"><i class="glyphicon glyphicon-remove"></i></a>
						<div class="clearFix"></div>
					</div>
					<div class="panel-body">
						<div class="messageMe">
							<img src="http://www.bitrebels.com/wp-content/uploads/2011/02/Original-Facebook-Geek-Profile-Avatar-1.jpg" alt=""/>
							<div class="messagecontent">
								<span>as</span>
								<span>as</span>
								<span>as</span>
							</div>
							<div class="clearFix"></div>
						</div>
						<div class="messageHer">
							<img src="http://www.bitrebels.com/wp-content/uploads/2011/02/Original-Facebook-Geek-Profile-Avatar-1.jpg" alt=""/>
							<div class="messagecontent">
								<span>as</span>
								<span>as</span>
								<span data-time="">as</span>
							</div>
							<div class="clearFix"></div>
						</div>
					</div>
					<div class="panel-footer">
						<textarea id="text-enter-area" name="textMessage" cols="0" rows="0"></textarea>
					</div>				
				</div>
			</div>
		</div>
    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="<?php echo base_url();?>js/jquery-2.1.4.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url();?>js/bootstrap.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo base_url();?>js/metisMenu.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>js/move-top.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>js/easing.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$(".scroll").click(function(event){
				event.preventDefault();
				$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
			});
			
		});
	</script>
	<script src="<?php echo base_url();?>js/wow.min.js"></script>
	<script>
	 new WOW().init();
	</script>
	<!-- //animation-effect -->
	<!-- -charting javascripts -->

		<!-- Morris Charts JavaScript -->
    <script src="<?php echo base_url();?>js/raphael.min.js"></script>
    <script src="<?php echo base_url();?>js/morris.min.js"></script>
<!--    <script src="<?php echo base_url();?>js/morris-data.js"></script>-->

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url();?>js/caretaker.js"></script>

</body>

</html>
