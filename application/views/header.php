
<!DOCTYPE html>
<html>
<head>
<title>Teaching an Education School Category Flat Bootstrap Responsive Website Template | Home </title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Teaching Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<link href="<?php echo base_url();?>css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="<?php echo base_url();?>css/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- js -->
<script type="text/javascript" src="<?php echo base_url();?>js/jquery-2.1.4.min.js"></script>
<!-- //js -->
<link href='//fonts.googleapis.com/css?family=Capriola' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="<?php echo base_url();?>js/move-top.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/easing.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>
<!-- start-smoth-scrolling -->
<!-- animation-effect -->
<link href="<?php echo base_url();?>css/animate.min.css" rel="stylesheet"> 
<script src="<?php echo base_url();?>js/wow.min.js"></script>
<script>
 new WOW().init();
</script>
<!-- //animation-effect -->
<!-- -charting javascripts -->

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>

</head>
	
<body>
<!-- banner -->
	<?php if($menu == 1){?>
		<div class="banner">
	<?php }else{?>
		<div class="banner1">
	<?php }?>
		<div class="header">
			<div class="container">
				<nav class="navbar navbar-default">
					<div class="navbar-header navbar-left">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<h1><a class="navbar-brand" href="<?php echo base_url('home');?>"><span>T</span>eaching</a></h1>
					</div>
					
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
						<nav class="menu menu--shylock">
							<div class="agileinfo_social_icons">
								<ul class="agileinfo_social_icons1">
									<li><a href="#" class="facebook"></a></li>
									<li><a href="#" class="twitter"></a></li>
									<li><a href="#" class="google"></a></li>
									<li><a href="#" class="pinterest"></a></li>
								</ul>
							</div>
							<ul class="nav navbar-nav">
								<?php if($menu == 1){?><li class="active"><?php }else{ ?><li><?php } ?><a  class="hvr-bounce-to-bottom" href="<?php echo base_url('home');?>">Home</a></li>
								<?php if($menu == 2){?><li class="active"><?php }else{ ?><li><?php } ?><a href="<?php echo base_url('about');?>" class="hvr-bounce-to-bottom">About Us</a></li>
								<?php if($menu == 4){?><li class="active"><?php }else{ ?><li><?php } ?><a href="<?php echo base_url('contact');?>" class="hvr-bounce-to-bottom">Mail Us</a></li>
								<?php if($this->session->userdata('loggedin') == TRUE){?>
									
									<li><a href="<?php echo base_url('dashboard');?>" class="hvr-bounce-to-bottom" href="#">Dashboard</a></li>
									<li><a class="hvr-bounce-to-bottom" href="#" id= "logout" data-url ="<?php echo base_url();?>" ><span class="glyphicon glyphicon-off"></span></a></li>
								<?php }else{?>
									<li>
										<a href="" data-toggle="modal" data-target="#loginModal" class="hvr-bounce-to-bottom "><i class="glyphicon glyphicon-user"></i></a>
									</li>
								<?php }?>
							</ul>
							<div class="clearfix"> </div>
						</nav>
					</div>
				</nav>	
			</div>
		</div>
		
		<!-- Login Modal -->
		<div class="modal fade" id="loginModal" tabindex="1" role="dialog" aria-labelledby="loginModal">
		
		  <div class="modal-dialog" id="widthLogModal" role="document">
			<div class="modal-content">
			
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="loginModal"><i class="glyphicon glyphicon-lock"></i> Enter your credentials </h4>
			  </div>
			  <div class="modal-body">
					
					<div class="alert alert-danger" id="invalidLogin" role="alert">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
						<strong>Wrong Username/Password. Please try again.</strong>
					</div>
					<form id="loginForm" class="form-signin" data-toggle="validator" role="form">
						<div class="form-group has-feedback" >
							<input type="text" id="username" name="username" class="form-control" placeholder="Your Username" required>
							<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
							<input type="hidden" name="url" id="url" value="<?php echo base_url();?>">
							<div class="help-block with-errors"></div>
						</div>
						<div class="form-group has-feedback">
							<input type="password" id="inputPassword" class="form-control" placeholder="Password" required="">
							<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
							<div class="help-block with-errors"></div>
						</div>
					<div class="pull-left">
						<a href="#"><h6><i>Forgot your password?</i></h6></a>
					</div>
					<div class="pull-right">
						<a href="#" data-toggle="modal" data-target="#newAccountModal" ><p>New Account <span class="glyphicon glyphicon-arrow-right"></span></p></a>
					</div>
				</br>
			  </div>
			  <div class="modal-footer">
				<button id="loginBtn" class="btn btn-lg btn-warning btn-block " type="submit">Sign in</button>
				</form>
			  </div>
			</div>
			
		  </div>
		</div>
		<!-- check Modal 
		<div class="modal fade" id="checkModal" tabindex="1" role="dialog" aria-labelledby="loginModal">
		
		  <div class="modal-dialog" id="widthLogModal" role="document">
			<div class="modal-content">

			  <div class="modal-body">

					<form id="imageForm" class="form-signin" data-toggle="validator" role="form" method="post">
						<input type="file" name="myfile">
				</br>
			  </div>
			  <div class="modal-footer">
				<button id="loginBtn" class="btn btn-lg btn-warning btn-block " type="submit">Sign in</button>
				</form>
			  </div>
			</div>
			
		  </div>
		</div>-->
		<!-- Add Account Modal -->
		<div class="modal fade" id="newAccountModal" tabindex="1" role="dialog" aria-labelledby="newAccountModal">
		
		  <div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
			
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="loginModal"><i class="glyphicon glyphicon-plus"></i> Registration Form of New Account </h4>
			  </div>
			  <div class="modal-body">
					<div class="alert alert-danger" id="errorAddMsg" role="alert">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
						<strong><p id="errorMsg"></p></strong>
					</div>
					<div class="alert alert-success" id="succAddMsg" role="alert">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
						<strong><p id="succMsg"></p></strong>
					</div>
					<form id="addAccountForm" class="form-signin" data-toggle="validator" role="form">
						<div class="row">
							<div class="col-lg-6 col-md-6">
								<div class="form-group has-feedback" >
									<input type="text" id="inputUsername" name="username" class="form-control" placeholder="Your Username" required>
									<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
									<div class="help-block with-errors"></div>
								</div>
								<div class="form-group has-feedback">
									<input type="password" id="password" name="password" class="form-control" placeholder="Your Password" required="">
									<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
									<div class="help-block with-errors"></div>
								</div>
								<div class="form-group has-feedback">
									<input type="password" id="inputCPassword" name="cpassword" class="form-control" placeholder="Re-Enter your Password" required="">
									<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
									<div class="help-block with-errors"></div>
								</div>
								<div class="form-group has-feedback" >
									<input type="email" id="email" name="email" class="form-control" placeholder="Your Email address" required>
									<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
									<div class="help-block with-errors"></div>
								</div>
								<div class="form-group has-feedback" >
									<label>Roles&emsp;</label>
									<input type="radio" name="role" value="3" checked>&emsp;Parent&emsp;&emsp;
									<input type="radio" name="role" value="2">&emsp;Caretaker
								</div>
								<div class="form-group has-feedback" >
									<label>Birthdate&emsp;</label>
									<input type="date" name="birthdate" required>
									<div class="help-block with-errors"></div>
								</div>
							</div>
							<div class="col-lg-6 col-md-6">
								<div class="form-group has-feedback" >
									<input type="text" id="inputSurname" name="surname" class="form-control" placeholder="Your Surname" required>
									<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
									<div class="help-block with-errors"></div>
								</div>
								<div class="form-group has-feedback" >
									<input type="text" id="inputName" name="name" class="form-control" placeholder="Your Name" required>
									<input type="hidden" name="url" id="urlAdd" value="<?php echo base_url();?>">
									<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
									<div class="help-block with-errors"></div>
								</div>
								<div class="form-group has-feedback" >
									<input type="text" id="inputTelephone" name="telephone" class="form-control" placeholder="Your Telephone" required>
									<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
									<div class="help-block with-errors"></div>
								</div>
								<div class="form-group has-feedback" >
									<input type="text" id="inputAddress" name="address" class="form-control" placeholder="Your Address" required>
									<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
									<div class="help-block with-errors"></div>
								</div>
								<!--<div class="form-group has-feedback">
									<label>Profile Picture</label>
									<input type="file" name="profilePic" required>
									<p class="help-block with-errors"></p>
								</div>-->
								<div class="form-group has-feedback" >
									<label>Gender&emsp;</label>
									<input type="radio" name="gender" value="1" checked>&emsp;Male
									<input type="radio" name="gender" value="2">&emsp;Female
								</div>
								
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12 col-md-12">
								
							</div>
						</div>
				</br>
			  </div>
			  <div class="modal-footer">
				<button id="loginBtn" class="btn btn-lg btn-warning btn-block " type="submit">Add Account</button>
				</form>
			  </div>
			</div>
			
		  </div>
		</div>
	<?php if($menu == 1){?>
		
	<?php }else{?>
		</div>
	<?php }?>
	