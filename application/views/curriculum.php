<!-- portfolio -->
	<div class="portfolio">
		<div class="container">
			<h3 class="head head2">P<span>Latest <i>portfolio</i> grids</span></h3>
                <div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
					<ul id="myTab" class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active"><a href="#home" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true">All</a></li>
						<li role="presentation"><a href="#learning" role="tab" id="learning-tab" data-toggle="tab" aria-controls="learning">Learning</a></li>
						<li role="presentation"><a href="#playing" role="tab" id="playing-tab" data-toggle="tab" aria-controls="playing">Playing</a></li>
						<li role="presentation"><a href="#painting" role="tab" id="painting-tab" data-toggle="tab" aria-controls="painting">Painting</a></li>
						<li role="presentation"><a href="#school" role="tab" id="school-tab" data-toggle="tab" aria-controls="school">School</a></li>
					</ul>
					<div id="myTabContent" class="tab-content">
						<div role="tabpanel" class="tab-pane fade in active" id="home" aria-labelledby="home-tab">
							<div class="w3_tab_img">
								<div class="col-md-4 w3_tab_img_left">
									<div class="demo">
										<a class="cm-overlay" href="<?php echo base_url();?>images/6.jpg">
										  <figure class="imghvr-shutter-in-out-diag-2"><img src="<?php echo base_url();?>images/6.jpg" alt=" " class="img-responsive" />
											<figcaption>
											  <h3>Teaching</h3>
											  <p>Phasellus elementum ullamcorper urna, 
												eu rhoncus lacus rutrum non.</p>
											</figcaption>
										  </figure>
										</a>
									</div>
								</div>
								<div class="col-md-4 w3_tab_img_left">
									<div class="demo">
										<a class="cm-overlay" href="<?php echo base_url();?>images/1.jpg">
										  <figure class="imghvr-shutter-in-out-diag-2"><img src="<?php echo base_url();?>images/1.jpg" alt=" " class="img-responsive" />
											<figcaption>
											  <h3>Teaching</h3>
											  <p>Phasellus elementum ullamcorper urna, 
												eu rhoncus lacus rutrum non.</p>
											</figcaption>
										  </figure>
										</a>
									</div>
								</div>
								<div class="col-md-4 w3_tab_img_left">
									<div class="demo">
										<a class="cm-overlay" href="<?php echo base_url();?>images/7.jpg">
										  <figure class="imghvr-shutter-in-out-diag-2"><img src="<?php echo base_url();?>images/7.jpg" alt=" " class="img-responsive" />
											<figcaption>
											  <h3>Teaching</h3>
											  <p>Phasellus elementum ullamcorper urna, 
												eu rhoncus lacus rutrum non.</p>
											</figcaption>
										  </figure>
										</a>
									</div>
								</div>
								<div class="col-md-4 w3_tab_img_left">
									<div class="demo">
										<a class="cm-overlay" href="<?php echo base_url();?>images/8.jpg">
										  <figure class="imghvr-shutter-in-out-diag-2"><img src="<?php echo base_url();?>images/8.jpg" alt=" " class="img-responsive" />
											<figcaption>
											  <h3>Teaching</h3>
											  <p>Phasellus elementum ullamcorper urna, 
												eu rhoncus lacus rutrum non.</p>
											</figcaption>
										  </figure>
										</a>
									</div>
								</div>
								<div class="col-md-4 w3_tab_img_left">
									<div class="demo">
										<a class="cm-overlay" href="<?php echo base_url();?>images/9.jpg">
										  <figure class="imghvr-shutter-in-out-diag-2"><img src="<?php echo base_url();?>images/9.jpg" alt=" " class="img-responsive" />
											<figcaption>
											  <h3>Teaching</h3>
											  <p>Phasellus elementum ullamcorper urna, 
												eu rhoncus lacus rutrum non.</p>
											</figcaption>
										  </figure>
										</a>
									</div>
								</div>
								<div class="col-md-4 w3_tab_img_left">
									<div class="demo">
										<a class="cm-overlay" href="<?php echo base_url();?>images/10.jpg">
										  <figure class="imghvr-shutter-in-out-diag-2"><img src="<?php echo base_url();?>images/10.jpg" alt=" " class="img-responsive" />
											<figcaption>
											  <h3>Teaching</h3>
											  <p>Phasellus elementum ullamcorper urna, 
												eu rhoncus lacus rutrum non.</p>
											</figcaption>
										  </figure>
										</a>
									</div>
								</div>
								<div class="col-md-4 w3_tab_img_left">
									<div class="demo">
										<a class="cm-overlay" href="<?php echo base_url();?>images/11.jpg">
										  <figure class="imghvr-shutter-in-out-diag-2"><img src="<?php echo base_url();?>images/11.jpg" alt=" " class="img-responsive" />
											<figcaption>
											  <h3>Teaching</h3>
											  <p>Phasellus elementum ullamcorper urna, 
												eu rhoncus lacus rutrum non.</p>
											</figcaption>
										  </figure>
										</a>
									</div>
								</div>
								<div class="col-md-4 w3_tab_img_left">
									<div class="demo">
										<a class="cm-overlay" href="<?php echo base_url();?>images/12.jpg">
										  <figure class="imghvr-shutter-in-out-diag-2"><img src="<?php echo base_url();?>images/12.jpg" alt=" " class="img-responsive" />
											<figcaption>
											  <h3>Teaching</h3>
											  <p>Phasellus elementum ullamcorper urna, 
												eu rhoncus lacus rutrum non.</p>
											</figcaption>
										  </figure>
										</a>
									</div>
								</div>
								<div class="col-md-4 w3_tab_img_left">
									<div class="demo">
										<a class="cm-overlay" href="<?php echo base_url();?>images/13.jpg">
										  <figure class="imghvr-shutter-in-out-diag-2"><img src="<?php echo base_url();?>images/13.jpg" alt=" " class="img-responsive" />
											<figcaption>
											  <h3>Teaching</h3>
											  <p>Phasellus elementum ullamcorper urna, 
												eu rhoncus lacus rutrum non.</p>
											</figcaption>
										  </figure>
										</a>
									</div>
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>
						<div role="tabpanel" class="tab-pane fade" id="learning" aria-labelledby="learning-tab">
							<div class="w3_tab_img">
								<div class="col-md-4 w3_tab_img_left">
									<div class="demo">
										<a class="cm-overlay" href="<?php echo base_url();?>images/1.jpg">
										  <figure class="imghvr-shutter-in-out-diag-2"><img src="<?php echo base_url();?>images/1.jpg" alt=" " class="img-responsive">
											<figcaption>
											  <h3>Teaching</h3>
											  <p>Phasellus elementum ullamcorper urna, 
												eu rhoncus lacus rutrum non.</p>
											</figcaption>
										  </figure>
										</a>
									</div>
								</div>
								<div class="col-md-4 w3_tab_img_left">
									<div class="demo">
										<a class="cm-overlay" href="<?php echo base_url();?>images/10.jpg">
										  <figure class="imghvr-shutter-in-out-diag-2"><img src="<?php echo base_url();?>images/10.jpg" alt=" " class="img-responsive">
											<figcaption>
											  <h3>Teaching</h3>
											  <p>Phasellus elementum ullamcorper urna, 
												eu rhoncus lacus rutrum non.</p>
											</figcaption>
										  </figure>
										</a>
									</div>
								</div>
								<div class="col-md-4 w3_tab_img_left">
									<div class="demo">
										<a class="cm-overlay" href="<?php echo base_url();?>images/13.jpg">
										  <figure class="imghvr-shutter-in-out-diag-2"><img src="<?php echo base_url();?>images/13.jpg" alt=" " class="img-responsive">
											<figcaption>
											  <h3>Teaching</h3>
											  <p>Phasellus elementum ullamcorper urna, 
												eu rhoncus lacus rutrum non.</p>
											</figcaption>
										  </figure>
										</a>
									</div>
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>
						<div role="tabpanel" class="tab-pane fade" id="playing" aria-labelledby="playing-tab">
							<div class="w3_tab_img">
								<div class="col-md-4 w3_tab_img_left">
									<div class="demo">
										<a class="cm-overlay" href="<?php echo base_url();?>images/6.jpg">
										  <figure class="imghvr-shutter-in-out-diag-2"><img src="<?php echo base_url();?>images/6.jpg" alt=" " class="img-responsive">
											<figcaption>
											  <h3>Teaching</h3>
											  <p>Phasellus elementum ullamcorper urna, 
												eu rhoncus lacus rutrum non.</p>
											</figcaption>
										  </figure>
										</a>
									</div>
								</div>
								<div class="col-md-4 w3_tab_img_left">
									<div class="demo">
										<a class="cm-overlay" href="<?php echo base_url();?>images/7.jpg">
										  <figure class="imghvr-shutter-in-out-diag-2"><img src="<?php echo base_url();?>images/7.jpg" alt=" " class="img-responsive">
											<figcaption>
											  <h3>Teaching</h3>
											  <p>Phasellus elementum ullamcorper urna, 
												eu rhoncus lacus rutrum non.</p>
											</figcaption>
										  </figure>
										</a>
									</div>
								</div>
								<div class="col-md-4 w3_tab_img_left">
									<div class="demo">
										<a class="cm-overlay" href="<?php echo base_url();?>images/9.jpg">
										  <figure class="imghvr-shutter-in-out-diag-2"><img src="<?php echo base_url();?>images/9.jpg" alt=" " class="img-responsive">
											<figcaption>
											  <h3>Teaching</h3>
											  <p>Phasellus elementum ullamcorper urna, 
												eu rhoncus lacus rutrum non.</p>
											</figcaption>
										  </figure>
										</a>
									</div>
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>
						<div role="tabpanel" class="tab-pane fade" id="painting" aria-labelledby="painting-tab">
							<div class="w3_tab_img">
								<div class="col-md-4 w3_tab_img_left">
									<div class="demo">
										<a class="cm-overlay" href="<?php echo base_url();?>images/11.jpg">
										  <figure class="imghvr-shutter-in-out-diag-2"><img src="<?php echo base_url();?>images/11.jpg" alt=" " class="img-responsive">
											<figcaption>
											  <h3>Teaching</h3>
											  <p>Phasellus elementum ullamcorper urna, 
												eu rhoncus lacus rutrum non.</p>
											</figcaption>
										  </figure>
										</a>
									</div>
								</div>
								<div class="col-md-4 w3_tab_img_left">
									<div class="demo">
										<a class="cm-overlay" href="<?php echo base_url();?>images/12.jpg">
										  <figure class="imghvr-shutter-in-out-diag-2"><img src="<?php echo base_url();?>images/12.jpg" alt=" " class="img-responsive">
											<figcaption>
											  <h3>Teaching</h3>
											  <p>Phasellus elementum ullamcorper urna, 
												eu rhoncus lacus rutrum non.</p>
											</figcaption>
										  </figure>
										</a>
									</div>
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>
						<div role="tabpanel" class="tab-pane fade" id="school" aria-labelledby="school-tab">
							<div class="w3_tab_img">
								<div class="col-md-4 w3_tab_img_left">
									<div class="demo">
										<a class="cm-overlay" href="<?php echo base_url();?>images/8.jpg">
										  <figure class="imghvr-shutter-in-out-diag-2"><img src="<?php echo base_url();?>images/8.jpg" alt=" " class="img-responsive">
											<figcaption>
											  <h3>Teaching</h3>
											  <p>Phasellus elementum ullamcorper urna, 
												eu rhoncus lacus rutrum non.</p>
											</figcaption>
										  </figure>
										</a>
									</div>
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>
					</div>
				</div>
				<script src="<?php echo base_url();?>js/jquery.tools.min.js"></script>
				<script src="<?php echo base_url();?>js/jquery.mobile.custom.min.js"></script>
				<script src="<?php echo base_url();?>js/jquery.cm-overlay.js"></script>
				<script>
					$(document).ready(function(){
						$('.cm-overlay').cmOverlay();
					});
				</script>
		</div>
	</div>
<!-- //portfolio -->