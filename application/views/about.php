<!-- services -->
	<div class="services">
		<div class="container">
			<h3 class="head head_services">S<span>we are giving special <i>Services</i></span></h3>
			<div class="agileinfo_services_grids">
				<div class="col-md-4 agileinfo_services_grid">
					<div class="agileinfo_services_grid1">
						<span class="glyphicon glyphicon-book" aria-hidden="true"></span>
						<h4>Ut tortor augue massa</h4>
						<p>Quisque consectetur, sem id sagittis sodales, augue diam consequat mi, 
							sed suscipit turpis diam eget nisl.</p>
					</div>
				</div>
				<div class="col-md-4 agileinfo_services_grid">
					<div class="agileinfo_services_grid1">
						<span class="glyphicon glyphicon-print" aria-hidden="true"></span>
						<h4>suscipit turpis diam eget</h4>
						<p>Quisque consectetur, sem id sagittis sodales, augue diam consequat mi, 
							sed suscipit turpis diam eget nisl.</p>
					</div>
				</div>
				<div class="col-md-4 agileinfo_services_grid">
					<div class="agileinfo_services_grid1">
						<span class="glyphicon glyphicon-link" aria-hidden="true"></span>
						<h4>augue diam consequat</h4>
						<p>Quisque consectetur, sem id sagittis sodales, augue diam consequat mi, 
							sed suscipit turpis diam eget nisl.</p>
					</div>
				</div>
				<div class="col-md-4 agileinfo_services_grid">
					<div class="agileinfo_services_grid1">
						<span class="glyphicon glyphicon-duplicate" aria-hidden="true"></span>
						<h4>Fusce ligula vel sapien</h4>
						<p>Quisque consectetur, sem id sagittis sodales, augue diam consequat mi, 
							sed suscipit turpis diam eget nisl.</p>
					</div>
				</div>
				<div class="col-md-4 agileinfo_services_grid">
					<div class="agileinfo_services_grid1">
						<span class="glyphicon glyphicon-bed" aria-hidden="true"></span>
						<h4>Nullam turpis fringilla</h4>
						<p>Quisque consectetur, sem id sagittis sodales, augue diam consequat mi, 
							sed suscipit turpis diam eget nisl.</p>
					</div>
				</div>
				<div class="col-md-4 agileinfo_services_grid">
					<div class="agileinfo_services_grid1">
						<span class="glyphicon glyphicon-blackboard" aria-hidden="true"></span>
						<h4>Integer venenatis lobortis</h4>
						<p>Quisque consectetur, sem id sagittis sodales, augue diam consequat mi, 
							sed suscipit turpis diam eget nisl.</p>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
<!-- //services -->
<!-- services-bottom -->
	<div class="services-bottom">
		<div class="container">
			<div class="col-md-6 w3agile_services_bottom_grid">
			</div>
			<div class="col-md-6 w3agile_services_bottom_grid">
				<h3>Integer venenatis massa lobortis porta ultricies nulla nec facilisis turpis</h3>
				<p>Etiam sit amet porta lectus, in convallis sapien. Nam quis erat lorem. 
					Nullam bibendum nisi eu fringilla vulputate. Fusce non ligula vel sapien 
					blandit cursus.</p>
				<div class="more">
					<a href="mail.html" class="hvr-bounce-to-bottom">Mail Us</a>
				</div>
			</div>
		</div>
	</div>
<!-- //services-bottom -->