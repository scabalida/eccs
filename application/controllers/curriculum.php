<?php defined('BASEPATH') OR exit('No direct script access allowed');
	
	class curriculum extends Admin_Controller {
		public function __construct() {
      parent::__construct();
	  $this->load->model('m_user');
    }
    
		public function index() {
			
			$data['menu'] =3;
			$this->load->view('header',$data);
			$this->load->view('curriculum');
			$this->load->view('footer');
		}
		
		// End Dashboard Class
	}