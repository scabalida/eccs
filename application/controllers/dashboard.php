	<?php defined('BASEPATH') OR exit('No direct script access allowed');
	
	class dashboard extends Admin_Controller {
		public function __construct() {
		  parent::__construct();
		 $this->load->model('m_user');
		}
    
		public function index() {
			
			$data['menu'] = 5;
			
			$data['childid'] = $this->session->userdata('childid') ? $this->session->userdata('childid') : $this->input->post('childid');
			
			if($data['childid'] != null) {
				if(!$this->session->userdata('childid')) {
					$this->session->set_userdata(array('childid' => $data['childid']));
				}
				$this->load->view('dashboard/child_dashboard',$data);
			} else {
				switch($this->session->userdata['roleID']) {
					case '1':
						$this->load->view('dashboard/moderator_dashboard',$data);
						break;
					case '2':
						$this->load->view('dashboard/caretaker_dashboard',$data);
						break;
					case '3':
						$this->load->view('dashboard/parent_dashboard',$data);
						break;
				}
			} 
		}
		
		
		public function addConversation() {
			$senderID = $this->session->userdata('userID');
			$receiverID = $this->input->post('receiverid');
			$results = $this->m_user->addConversation($senderID,$receiverID);
			echo json_encode($results);
		}		
		
		public function getConversations() {
			$userID = $this->session->userdata('userID');
			$data['result'] = $this->m_user->getConversations($userID);
			echo json_encode($data);
		}
		
		public function play($gameID = null , $name = null) {
			$childID = $this->session->userdata('childid') ? $this->session->userdata('childid') : null;
			
			$data['result'] = $this->m_user->getGameData($gameID, $childID);
			$data['childid'] = $childID;
			$data['name'] = $name;
			$this->load->view('game',$data);
		}
		
		public function updateGameHistory() {
			$data['message'] = $this->input->post('message');
			$data['gameID'] = $this->input->post('gameid');
			$data['childID'] = $this->session->userdata('childid') ? $this->session->userdata('childid') : null;
			$data['userID'] = $this->session->userdata('userID');
			
			$results['result'] = $this->m_user->addGameHistoryData($data);
			$results['data'] = $data;
			// $results = true;
			
			echo json_encode($data);
		}
		
		// searches the assigned children of the user. but if admin, can search all the users even parents, caretakers
		public function getAssignedChildData() {
			
			$childID = $this->input->post('userid');
			
			$results['result'] = $this->m_user->getChildData($childID);
			echo json_encode($results);
		}
		
		// searches the assigned children of the user. but if admin, can search all the users even parents, caretakers
		public function searchAssignedUsers() {
			
			$searchString = $this->input->post('name');
			$searcherRoleID = $this->session->userdata('roleID');
			$searcherUserID = $this->session->userdata('userID');
			
			$results['result'] = $this->m_user->getAssignedChildren($searchString, $searcherUserID, $searcherRoleID);
			
			echo json_encode($results);
		}
		
		public function searchParentsCaretakers() {
			
			$searchString = $this->input->post('name');
			$searcherRoleID = $this->session->userdata('roleID');
			$searcherUserID = $this->session->userdata('userID');
			
			$results['result'] = $this->m_user->getParentsCaretakers($searchString, $searcherUserID, $searcherRoleID);
			
			echo json_encode($results);
		}
		
		public function submitSpiceForm() {
			$data = array (
				'social' => $this->input->post('social'),
				'physical' => $this->input->post('physical'),
				'intellectual' => $this->input->post('intellectual'),
				'creative' => $this->input->post('creative'),
				'emotional' => $this->input->post('emotional'),
				'childid' => $this->input->post('childid'),
				'caretakerID' => $this->session->userdata('userID'),
				'currentUserRoleID' => $this->session->userdata('roleID')
			);
			
			$results= $this->m_user->insertOrUpdateSpiceForm($data);
			
			echo json_encode($results);
			
		}
		
		public function saveActivityDetail() {
			$data = array (
				'spiceActivityID' => $this->input->post('spiceActivityID'),
				'spiceDevelopmentID' => $this->input->post('spiceDevelopmentID'),
				'spiceChildID' => $this->input->post('spiceChildID'),
				'rating' => $this->input->post('rating'),
				'comment' => $this->input->post('comment'),
				'caretakerID' => $this->session->userdata('userID')
			);
			$results= $this->m_user->saveActivityDetail($data);
			
			echo json_encode($results);
		}
		
		public function getNotifications() {
			$data = array (
				'caretakerID' => $this->session->userdata('userID'),
				'currentUserRoleID' => $this->session->userdata('roleID')
			);
			
			$results = $this->m_user->getNotifications($data);
			echo json_encode($results);
		}
		
		public function getActivities() {
			$results = $this->m_user->getActivities();
			echo json_encode($results);
			
		}
		
		public function addActivity() {
			$data = array (
				'name' => $this->input->post('activityname'),
				'spiceDevelopmentID' => $this->input->post('devid')
			);
			$results = $this->m_user->addActivity($data);
			echo json_encode($results);
			
		}
		
		public function deleteActivity() {
			$data = array (
				'activityID' => $this->input->post('activityID')
			);
			$results = $this->m_user->deleteActivity($data);
			echo json_encode($results);
			
			// echo json_encode($data);
		}
		
		public function getGuidanceVideos() {
			$data = array(
				'roleID' => $this->session->userdata('roleID')
			);
			
			$results = $this->m_user->getGuidanceVideos($data);
			echo json_encode($results);
		}
		
		public function add_guidance_video() {
			$data = array (
				'filename' => $this->input->post('add-guidance-url'),
				'roleID' => $this->input->post('roleid'),
				'levelID' => $this->input->post('levelid'),
				'description' => $this->input->post('add-guidance-description'),
				'uploaderID' => $this->session->userdata('userID'),
				'uploadDate' => date('Y-m-d ', time())
			);
			$results = $this->m_user->addVideo($data);
			echo json_encode($results);
		}
		
		public function watch() {
			// $data['results']= $this->m_user->getVideoById($this->input->get('v'));
			$results= $this->m_user->getVideoById($this->input->get('v'));
			$parts = parse_url($results[0]['filename']);
			
			
			// $data['query'] = ;
			parse_str($parts['query'], $query);
			$data['query'] = $query;
			// echo $query['email'];
			$this->load->view('video',$data);
		}
		
		public function deleteVideo() {
			$data = array (
				'videoID' => $this->input->post('videoid')
			);
			
			$results = $this->m_user->deleteGuidanceVideo($data);
			echo json_encode($results);
		}
		
		public function getGamesToAdd() {
			$results = $dirs = array_filter(glob('games/games to add/*'), 'is_dir');
			echo json_encode($results);
		}
		
		public function addGame() {
			$data = array (
				'name' => $this->input->post('game-title'),
				'description' => $this->input->post('game-description'),
				'levelID' => $this->input->post('game-level'),
				'uploaderID' => $this->session->userdata('userID')
				);
			$game = $this->input->post('game');
				
			$results = false;
			if($this->xcopy('games/games to add/'.$game,'games/play/'.$data['name'])) {
				$results = $this->m_user->addGame($data);
			}
			
			
			echo json_encode($data);				
		}
		
		function xcopy($source, $dest, $permissions = 0755) {
			// Check for symlinks
			if (is_link($source)) {
				return symlink(readlink($source), $dest);
			}

			// Simple copy for a file
			if (is_file($source)) {
				return copy($source, $dest);
			}

			// Make destination directory
			if (!is_dir($dest)) {
				mkdir($dest, $permissions);
			}

			// Loop through the folder
			$dir = dir($source);
			while (false !== $entry = $dir->read()) {
				// Skip pointers
				if ($entry == '.' || $entry == '..') {
					continue;
				}

				// Deep copy directories
				$this->xcopy("$source/$entry", "$dest/$entry", $permissions);
			}

			// Clean up
			$dir->close();
			return true;
		}
		
		public function getGameListings() {
			$data = array(
				'roleID' => $this->session->userdata('roleID'),
				'userID' => $this->session->userdata('userID')
			);
			
			$results = $this->m_user->getGameListings($data);
			echo json_encode($results);
		}
		
		public function deleteGame() {
			$data = array(
				'gameID' => $this->input->post('gameid')
			);
			
			$gameName = $this->input->post('gamename');
			$gameLocation = './games/play/'.$gameName;
			$files = array_diff(scandir($gameLocation), array('.', '..'));

			$this->deleteFiles($gameLocation);
			$results = $this->m_user->deleteGame($data);
			echo json_encode($results);
		}
		
		function deleteFiles($path)
		{
			if (is_dir($path) === true)
			{
				$files = array_diff(scandir($path), array('.', '..'));

				foreach ($files as $file)
				{
					$this->deleteFiles(realpath($path) . '/' . $file);
				}

				return rmdir($path);
			}

			else if (is_file($path) === true)
			{
				return unlink($path);
			}

			return false;
		}
		
		// searches the assigned children of the user. but if admin, can search all the users even parents, caretakers
		public function getParentCaretakerData() {
			$userID = $this->input->post('userid');
			$roleID = $this->input->post('roleid');
			
			$results['result'] = $this->m_user->getParentCaretakerData($userID,$roleID);
			echo json_encode($results);
		}

		// searches the assigned children of the user. but if admin, can search all the users even parents, caretakers
		public function getMyParentCaretakerData() {
			
			$userID = $this->session->userdata('userID');
			$roleID = $this->session->userdata('roleID');
		
			$results['result'] = $this->m_user->getParentCaretakerData($userID,$roleID);
			echo json_encode($results);
		}

			
		// End Dashboard Class
	}