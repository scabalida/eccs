<?php defined('BASEPATH') OR exit('No direct script access allowed');
	
	class user extends Admin_Controller {
		public function __construct() {
		
		parent::__construct();
		
	  $this->load->model('m_user');
    }
    
		public function index() {
			
			$data['menu'] = 1;
			$this->load->view('header',$data);
			$this->load->view('index');
			$this->load->view('footer');
		}
		
		public function services() {
			
			$data['menu'] = 2;
			$this->load->view('header',$data);
			$this->load->view('services');
			$this->load->view('footer');
		}
		
		public function portfolio() {
			
			$data['menu'] = 3;
			$this->load->view('header',$data);
			$this->load->view('portfolio');
			$this->load->view('footer');
		}
		
		public function shortcodes() {
			
			$data['menu'] = 4;
			$this->load->view('header',$data);
			$this->load->view('shortcodes');
			$this->load->view('footer');
		}
		
		public function mail() {
			
			$data['menu'] = 5;
			$this->load->view('header',$data);
			$this->load->view('mail');
			$this->load->view('footer');
		}
		
		public function loginNow(){
			$username =  $this->input->post('user');
			$password =  md5($this->input->post('pass'));
			
			if($this->m_user->login($username, $password))
			{	
				echo json_encode('true');			
			}
			else
			{
				echo json_encode('false');
			}
			
		}
		
		public function checkUser($user) {
			
			
			if($this->m_user->checkUsername($user))
			{	
				echo json_encode('true');			
			}
			else
			{
				echo json_encode('false');
			}
		}
		
		public function changeUserPassword() {
			$id = $this->session->userdata('userID');
			$data = array (
						'pass' => md5($this->input->post('pass'))
			);
			if($this->m_user->changePassUser($data,$id))
			{	
				echo json_encode('true');			
			}
			else
			{
				echo json_encode('false');
			}
		}
		
		/***** Add and Get Data ****/
		public function addUser(){
			$data = array (
				'surname' =>  $this->input->post('surname'),
				'name' =>  $this->input->post('name'),
				'telephone' =>  $this->input->post('telephone'),
				'address' =>  $this->input->post('address'),
				'gender' =>  $this->input->post('gender'),
				'bday' =>  $this->input->post('bday'),
				'email' =>  $this->input->post('email'),
				'username' =>  $this->input->post('username'),
				'role' =>  $this->input->post('role'),
				 'hash' => md5(rand(0, 1000)),
				'pass' =>  md5($this->input->post('pass'))
			);
			$pass_data = $this->m_user->addUser($data);
			if($pass_data > 0)
			{	
				$this->load->library('email');
				$this->email->from('office@eccs.net', 'Eccs');
				$this->email->to($this->input->post('email'));

				$this->email->subject('Email Verification');
				$this->email->message('Thanks for signing up! Your account has been created, you can login with the following credentials after you have activated your account by pressing the url below.Please click this link to activate your account:<a href='.base_url('user/verifyEmail/'.$pass_data.'/'.$data['hash']).'> </a>'); 
			
				
				if($this->email->send()){
				 	echo json_encode('true');
				 }
				else{
					echo json_encode('false');
						
				 }			
			}
			else
			{
				echo json_encode('false');
			}
			
		}
		public function verifyEmail($userID,$hash){
			
			$data = array(
			'emailVerified' => 1
			
			);
			
				if($this->m_user->verifyEmail($data,$userID ) ){
						redirect('http://enmachi.com/eccs/');
						
					}
					else{
						redirect('http://enmachi.com/eccs/');
					}
		}
		public function getAdminVerify(){
			$data = $this->m_user->getAdminVerify();
			
			echo json_encode($data);
		}
		public function getCaretakers(){
			$data = $this->m_user->getCaretakers();
			
			echo json_encode($data);
		}
		public function getAssignedChildToCaretaker($id){
			 $data = $this->m_user->getAssignedChildToCaretaker($id);
			echo json_encode($data);
		 }
		public function getParents(){
			$data = $this->m_user->getParents();
			
			echo json_encode($data);
		}
		public function getUserChild(){
			$data = $this->m_user->getUserChild();
			
			echo json_encode($data);
		}
		public function getMyChildren($id){
			$data = $this->m_user->getMyChildren($id);
			
			echo json_encode($data);
		}
		public function getClassroomWithCaretaker(){
			$data = $this->m_user->getClassroomWithCaretaker();
			
			echo json_encode($data);
		}
		
		public function getUserInfo($userID){
			$data = $this->m_user->getUserInfo($userID);
			
			echo json_encode($data);
		}
		
		public function getClassroomInfo($classroomID){
			$data = $this->m_user->getClassroomInfo($classroomID);
			
			echo json_encode($data);
		}
		public function getClassroomLevel($classroomID){
			
			$data = $this->m_user->getClassroomLevel($classroomID);
			
			echo json_encode($data);
		}
		public function adminVerified($id){
			$data = $this->m_user->adminVerified($id);
			
			echo json_encode($data);
		}
		
		public function getParentID(){  
			// process posted form data  
			$keyword = $this->input->post('term');  
			$data['response'] = 'false'; //Set default response  
			$query = $this->m_user->getParentID($keyword); //Search DB  
			if( ! empty($query) )  
			{  
				$data['response'] = 'true'; //Set response  
				$data['message'] = array(); //Create array  
				foreach( $query as $row )  
				{  
					$data['message'][] = array(   
											'id' => $row->userID,  
											'value' => $row->firstName." ".$row->lastName
											
										 );  //Add a row to array  
				}  
			}  
		
			if('IS_AJAX')  
			{  
				echo json_encode($data); //echo json string if ajax request  
				   
			}  
			else  
			{  
				
				$data['menu'] = 5;
				switch($this->session->userdata['roleID']) {
					case '1':
						$this->load->view('dashboard/moderator_dashboard',$data);
						break;
					case '2':
						$this->load->view('dashboard/caretaker_dashboard',$data);
						break;
					case '3':
						$this->load->view('dashboard/parent_dashboard',$data);
						break;
				}
			}  
				   
			
		}
		
		public function getCaretakerID(){  
			// process posted form data  
			$keyword = $this->input->post('term');  
			$data['response'] = 'false'; //Set default response  
			$query = $this->m_user->getLikeCaretaker($keyword); //Search DB  
			if( ! empty($query) )  
			{  
				$data['response'] = 'true'; //Set response  
				$data['message'] = array(); //Create array  
				foreach( $query as $row )  
				{  
					$data['message'][] = array(   
											'id' => $row->userID,  
											'value' => $row->firstName." ".$row->lastName
											
										 );  //Add a row to array  
				}  
			}  
		
			if('IS_AJAX')  
			{  
				echo json_encode($data); //echo json string if ajax request  
				   
			}  
			else  
			{  
				
				$data['menu'] = 5;
				switch($this->session->userdata['roleID']) {
					case '1':
						$this->load->view('dashboard/moderator_dashboard',$data);
						break;
					case '2':
						$this->load->view('dashboard/caretaker_dashboard',$data);
						break;
					case '3':
						$this->load->view('dashboard/parent_dashboard',$data);
						break;
				}
			}  
				   
			
		}
		public function autoGetClassroom(){  
			
			// process posted form data  
			$keyword = $this->input->post('term');  
			$data['response'] = 'false'; //Set default response  
			$query = $this->m_user->getAllClassroom($keyword); //Search DB  
			if( ! empty($query) )  
			{  
				$data['response'] = 'true'; //Set response  
				$data['message'] = array(); //Create array  
				foreach( $query as $row )  
				{  
					if($row->levelID == 1){
						$level = "Nursery";
					}else if($row->levelID == 2){
						$level = "Preschool";
					}else if($row->levelID == 3){
						$level = "Kindergarten";
					}
					
					$data['message'][] = array(   
											'id' => $row->classroomID,  
											'value' => $row->name." - ".$level
											
										 );  //Add a row to array  
				}  
			}  
		
			if('IS_AJAX')  
			{  
				echo json_encode($data); //echo json string if ajax request  
				   
			}  
			else  
			{  
				
				$data['menu'] = 5;
				switch($this->session->userdata['roleID']) {
					case '1':
						$this->load->view('dashboard/moderator_dashboard',$data);
						break;
					case '2':
						$this->load->view('dashboard/caretaker_dashboard',$data);
						break;
					case '3':
						$this->load->view('dashboard/parent_dashboard',$data);
						break;
				}
			}  
				   
			
		}
		public function addChild(){
			$data = array (
				'surname' =>  $this->input->post('surname'),
				'name' =>  $this->input->post('name'),
				'telephone' =>  $this->input->post('telephone'),
				'address' =>  $this->input->post('address'),
				'gender' =>  $this->input->post('gender'),
				'bday' =>  $this->input->post('bday'),
				'parentID' =>  $this->input->post('parentID'),
				'classroomID' =>  $this->input->post('classroomID'),
				'roleID' =>  4,
				'levelID' =>  $this->input->post('level')
			);
			$pass_data = $this->m_user->addChild($data);
			if($pass_data > 0)
			{	
				echo json_encode('true');
				 		
			}
			else
			{
				echo json_encode('false');
			}
			
		}
		
		public function addClassroom(){
			$data = array (
				'caretakerID' =>  $this->input->post('caretakerID'),
				'name' =>  $this->input->post('name'),
				'levelID' =>  $this->input->post('level')
			);
			$pass_data = $this->m_user->addClassroom($data);
			if($pass_data > 0)
			{	
				echo json_encode('true');
				 		
			}
			else
			{
				echo json_encode('false');
			}
			
		}
		
		public function allChildrenInClassroom($classroomID){
			$data = $this->m_user->allChildrenInClassroom($classroomID);
			
			echo json_encode($data);
		}
		public function getNames($userID){
			$data = $this->m_user->getNames($userID);
			
			echo json_encode($data);
		}
		/***** Delete Function*****/
		public function removeUser($userID){
			$data = $this->m_user->removeUser($userID);
			
			if($data)
			{	
				echo json_encode('true');
			}
			else
			{
				echo json_encode('false');
			}
		}
		
		public function removeChild($userID){
			$data = $this->m_user->removeChild($userID);
			
			if($data)
			{	
				echo json_encode('true');
			}
			else
			{
				echo json_encode('false');
			}
		}
		
		public function removeClassroom($classID){
			$data = $this->m_user->removeClassroom($classID);
			
			if($data)
			{	
				echo json_encode('true');
			}
			else
			{
				echo json_encode('false');
			}
		}
		
		public function removeMyChild(){
			$userID = $this->input->post('id');
			
			$data = $this->m_user->removeMyChild($userID);
			
			if($data)
			{	
				echo json_encode('true');
			}
			else
			{
				echo json_encode('false');
			}
		}
		
		public function removeChildInClass(){
			$userID = $this->input->post('id');
			$data = $this->m_user->removeChildInClass($userID);
			
			if($data)
			{	
				echo json_encode('true');
			}
			else
			{
				echo json_encode('false');
			}
		}
		/******* Update Function *******/
		
		public function updateProfileAccount(){
			
			$userID = $this->input->post('userID');
			
			$profileData = array (
				'lastName' =>  $this->input->post('surname'),
				'firstName' =>  $this->input->post('name'),
				'telephone' =>  $this->input->post('telephone'),
				'address' =>  $this->input->post('address'),
				'gender' =>  $this->input->post('gender'),
				'dateOfBirth' =>  $this->input->post('bday'),
				'emailAddress' =>  $this->input->post('email')
			);
			
			$data = $this->m_user->updateProfileAccount($profileData,$userID);
			
			if($data)
			{	
				echo json_encode('true');
			}
			else
			{
				echo json_encode('false');
			}
		}
		public function updateChildAccount(){
			
			$userID = $this->input->post('userID');
			
			$classID = array (
				'classroomID' =>  $this->input->post('classID')
			);
			$profileData = array (
				'lastName' =>  $this->input->post('surname'),
				'firstName' =>  $this->input->post('name'),
				'telephone' =>  $this->input->post('telephone'),
				'address' =>  $this->input->post('address'),
				'gender' =>  $this->input->post('gender'),
				'gender' =>  $this->input->post('gender'),
				'gender' =>  $this->input->post('gender'),
				'parentID' =>  $this->input->post('parentID'),
				'dateOfBirth' =>  $this->input->post('bday')
			);
			
			$data = $this->m_user->updateChildProfileAccount($profileData,$userID,$classID);
			
			if($data)
			{	
				echo json_encode('true');
			}
			else
			{
				echo json_encode('false');
			}
		}
		
		public function levelUpChild(){
			
			$userID = $this->input->post('userID');
			$level = $this->input->post('level');
			
			
			$data = $this->m_user->levelUpChild($userID,$level);
			
			
				echo json_encode($data);
			
		}
		
		public function uploadImage()
		{
			$this->load->library('upload');

			if (isset($_FILES['myfile']) && !empty($_FILES['myfile']))
			{
				if ($_FILES['myfile']['error'] != 4)
				{
					 // Image file configurations
					$config['upload_path'] = 'upload/users';
					$config['allowed_types'] = 'jpg|jpeg|png';
					$this->upload->initialize($config);
					$this->upload->do_upload('myfile');
				}
			}
		}

		public function updateClassroom(){
			
			$classID = $this->input->post('classID');
			
			$classData = array (
				'caretakerID' =>  $this->input->post('caretakerID'),
				'levelID' =>  $this->input->post('level'),
				'name' =>  $this->input->post('name')
			);
			
			$data = $this->m_user->updateClassroom($classData,$classID);
			
			if($data)
			{	
				echo json_encode('true');
			}
			else
			{
				echo json_encode('false');
			}
		}
		public function logout(){
			
			if($this->session->sess_destroy())
			{	
				echo json_encode('true');			
			}
			else
			{
				echo json_encode('false');
			}
			
		}
		// End Dashboard Class
	}