<?php defined('BASEPATH') OR exit('No direct script access allowed');
	
	class home extends Admin_Controller {
		public function __construct() {
      parent::__construct();
	  $this->load->model('m_user');
    }
    
		public function index() {
			
			$data['menu'] = 1;
			$this->load->view('header',$data);
			$this->load->view('index');
			$this->load->view('footer');
		}
		
		public function template() {
			
			$data['menu'] = 1;
			$this->load->view('header',$data);
			$this->load->view('dashboard_template');
			// $this->load->view('footer');
		}
		
		
		
		// End Dashboard Class
	}