/*!
 * Start Bootstrap - SB Admin 2 v3.3.7+1 (http://startbootstrap.com/template-overviews/sb-admin-2)
 * Copyright 2013-2016 Start Bootstrap
 * Licensed under MIT (https://github.com/BlackrockDigital/startbootstrap/blob/gh-pages/LICENSE)
 */
$(function() {
    $('#side-menu').metisMenu();
});

//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size
$(function() {
    $(window).bind("load resize", function() {
        var topOffset = 50;
        var width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $('div.navbar-collapse').addClass('collapse');
            topOffset = 100; // 2-row-menu
        } else {
            $('div.navbar-collapse').removeClass('collapse');
        }

        var height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            $("#page-wrapper").css("min-height", (height) + "px");
        }
    });

    var url = window.location;
    // var element = $('ul.nav a').filter(function() {
    //     return this.href == url;
    // }).addClass('active').parent().parent().addClass('in').parent();
    var element = $('ul.nav a').filter(function() {
        return this.href == url;
    }).addClass('active').parent();

    while (true) {
        if (element.is('li')) {
            element = element.parent().addClass('in').parent();
        } else {
            break;
        }
    }
	
	// adds functionality to sidebar
	$('#side-menu > li').click(function () {
		var targetID = $(this).data('targetid');
		if(targetID !== undefined) {
			$(this).parent().children().children().removeClass('active');
			$(this).children().addClass('active').blur();
			
			var parent = $('#page-wrapper');
			parent.children().hide();
			parent.children('#'+ targetID).show();
			
			switch(targetID) {
				case 'child-dashboard-container':
					refresh.notificationPanel();
					break;
				case 'gamelistings-container':
					refresh.gamelistings();
					break;
			}
		}
	});
	
	var refresh = {
		gamelistings: function() {
			$.ajax({
				type: "POST",
				url: "dashboard/getGameListings",
				dataType: "json",
				success:
				function(data) {
					console.log(data);
					var gameContainer = $('#game-list');
					gameContainer.empty();
					for(var x = 0 ;x < data.length; x++) {
						// console.log(data[x]);
						var name = data[x].name;
						
						var item = $('<div>',{class:'item col-xs-4 col-lg-4'}),
							thumbnail = $('<div>',{class:'thumbnail'}),
							img = $('<img>', {class:'group list-group-image', src:"./games/play/"+data[x].name+"/screenshot.png",alt:"Image here"}),
							divCaption = $('<div>',{class:'caption'}),
							title = $('<h4>', {class:'group inner list-group-item-heading'}).append(data[x].name),
							// description = $('<p>', {class:'group inner list-group-item-text'}).append(data[x].description),
							btnContainer = $('<div>', {class:"btn-container",style:"text-align:center;"});
							btnWatch = $('<a>', {class:'btn btn-success',target:'_blank',href:"./games/play/"+data[x].name+"/index.html"}).append("Play this game");
							btnDelete = $('<a>', {class:'btn btn-danger','data-name':data[x].name,'data-id':data[x].gameID}).append('Delete');
							btnDelete.click(function() {
								
								if(confirm("Are you sure you want to delete this?")) {
									var el = $(this),
										gameID = el.data('id'),
										gameName = el.data('name');
									console.log('I was clicked');
									$.ajax({
										type: "POST",
										url: "dashboard/deleteGame",
										dataType: "json",
										data: {
											gameid: gameID,
											gamename: gameName
										},
										success:
										  function(data) {
											  console.log(data);
											  if(data == true) {
												  console.log(el);
												  el.parent().parent().parent().parent().remove();
											  }
										},
										error: function(data){
											console.log(data);
										  }
									});
									
								}
							});
							btnContainer.append(btnWatch).append(btnDelete);
							
							divCaption.append(title)
							// .append(description)
							.append(btnContainer);
							thumbnail.append(img).append(divCaption);
							item.append(thumbnail);
							gameContainer.append(item);
					}
				}
			});
		}
	};
	
	$('#chart-dropdown li').click(function() {
		if($(this).data('action') == 'linechart') {
			$('#btn-chart').empty()
								.append('Line Chart ')
								.append($('<span>',{class:'caret'}));
			$('#morris-bar-chart').hide();
			$('#morris-line-chart').show();
		} else {
			$('#btn-chart').empty()
								.append('Bar Chart ')
								.append($('<span>',{class:'caret'}));
			$('#morris-bar-chart').show();
			$('#morris-line-chart').hide();
		}
		console.log();
	});
	
	// shows a modal when clicks on a panel for details for this week
	$('.view-activities-modal').click(function() {
		var divmodal = $('#modal_spiceActivities'),
			   targetid = $(this).data('targetid'),
			   container = $('#spice-activities-container');
		
		container.children().hide();
		container.children('#'+targetid).show();
		divmodal.modal('show');
		
	});
	
	$('#btn_modal_spiceform').click(function() {
		var target_modal = $('#modal_spiceForm');
		target_modal.modal('show');
	});
	
	
	
	function init() {
		
		var socialActivities = $('#manageactivities_social'),
			physicalActivities = $('#manageactivities_physical'),
			intellectualActivities = $('#manageactivities_intellectual'),
			creativeActivities = $('#manageactivities_creative'),
			emotionalActivities = $('#manageactivities_emotional');
		
		console.log('init');
		$.get('/eccs/dashboard/getActivities', {},function(data) {
			console.log('data is' );
			console.log(data);
			for(var x = 0, len = data.length; x < len; x++) {
				var tr = $('<tr>');
					tr.append($('<td>').append(data[x].spiceActivityName))
					  .append($('<td>')
					  .append(
							$('<div>',{class:'text-center'}).append(
								$('<a>',{'data-spiceactivityid':data[x].spiceactivityID,href:'javascript:void(0)'}).append('X').click(function() {
									var el = $(this);
									$.ajax({
										beforeSend: function() {
										},
										type: "POST",
										url: "dashboard/deleteActivity",
										data: 'activityID=' + $(this).data('spiceactivityid'),
										dataType: "json",
										success:
										function(data) {
											if(data == true) {
												el.parent().parent().parent().remove();
											}
											console.log(data);
										}
									});
								})
							)
						 )
					  );
						
				switch(data[x].spiceDevelopmentName) {
					case 'Social':
						socialActivities.append(tr);
						break;
					case 'Physical':
						physicalActivities.append(tr);
						break;
					case 'Intellectual':
						intellectualActivities.append(tr);
						break;
					case 'Creative':
						console.log('cativeee');
						creativeActivities.append(tr);
						break;
					case 'Emotional':
						emotionalActivities.append(tr);
						break;
				}
			}
		}, "json");
	}
	
	init();
	
});
