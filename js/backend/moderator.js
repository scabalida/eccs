
$(document).ready(function() {
    //User Settings//
		$("#errorUserSetMsg").hide();
		$("#succUserSetMsg").hide();
		
		$(".errorAddChildMsg").hide();
		$("#succAddChildMsg").hide();
		
		$("#errorAddMsg").hide();
		$("#succAddMsg").hide();
		
		$("#errorEditMsg").hide();
		$("#succEditMsg").hide();
		
		$(".errorEditChildMsg").hide();
		$("#succEditChildMsg").hide();
		$("#ajaxManage").hide();
		
		$("#userSettingForm").submit(function(e) {
			e.preventDefault();
			var pass = $("#settingNewPassword").val();
			var repass = $("#settingReNewPassword").val();
			
			if(pass != repass ){
				$("#errorUserSetMsg").show();
			}
			else if(pass == "" && repass == ""){
				$("#succUserSetMsg").hide(); 
			}
			else{
				$("#errorUserSetMsg").hide();
				$.ajax({
					type: "POST",
					url: "user/changeUserPassword",
					dataType: "json",
					data:  { pass:pass},
					success:
					  function(data) {
							if (data == "true")
							{
								$("#succUserSetMsg").show(); 
								$("#settingNewPassword").val("") ;
								$("#settingReNewPassword").val("");
							}
							else
							{
								$("#errorUserSetMsg").show();
								
							}
								
					  },
								
				error: function(data){
						console.log(data);
					  }
				});
				return false;
			}
		});

		//ADD CHILD //
		$("#addChildForm").submit(function(e) {
			e.preventDefault();
			var surname = $("#addChildinputSurname").val();
			var name = $("#addChildinputName").val();
			var telephone = $("#addChildinputTelephone").val();
			var address = $("#addChildinputAddress").val();
			var parentID = $("#ParentID").val();
			var classroomID = $("#ClassroomID").val();
			var gender = $("input[name=addChildgender]").val();
			var bday = $("input[name=addChildbirthdate]").val();
			var level = $("#addChildinputLevel").val();


			if(surname != "" && name != "" && telephone != "" && address != "" && classroomID != "" && parentID != "" && gender != "" && bday != "" && level != ""){
			
				$.get('/eccs/user/getClassroomLevel/'+classroomID, {},function(data) {
					
					if(data[0].levelID != level){
						$(".errorChildMsg").html("Classroom level is not equal to child level.");
								$(".errorAddChildMsg").show();
					}
					else{
						$.ajax({
						type: "POST",
						url: "user/addChild",
						dataType: "json",
						data:  {surname:surname, name:name, telephone:telephone, address:address, gender:gender, bday:bday, parentID:parentID, classroomID:classroomID, level:level},
						success:
						  function(data) {
							if (data == "true")
							{
								$(".errorAddChildMsg").hide();
								$("#succChildMsg").html("New Child Successfully Added.");
								$("#succAddChildMsg").show(); 

								setTimeout(function(){  
									getUserChild();
									 $("#addChildModal").modal("hide");
									 $("#succAddChildMsg").hide();
								}, 2000);
							}
							else
							{
								$(".errorChildMsg").html("Error in adding the Child.");
								$(".errorAddChildMsg").show();
								
							}
								
					  },
								
						error: function(data){
								console.log(data);
							  }
						});
					}
				}, "json");

			}else{
				console.log("sss");
			}
			
			
		});
		
		//ADD Classroom //
		$("#addClassroomForm").submit(function(e) {
			e.preventDefault();
			var name = $("#inputClassname").val();
			var caretakerID = $("#caretakerID").val();
			var level = $("#inputLevel").val();
			
			if(name != "" && caretakerID != "" && level != ""){
				$.ajax({
					type: "POST",
					url: "user/addClassroom",
					dataType: "json",
					data:  {caretakerID:caretakerID, name:name, level:level},
					success:
					  function(data) {
							if (data == "true")
							{
								$("#succMsg").html("New Classroom Successfully Added.");
								$("#succAddMsg").show(); 

								setTimeout(function(){  
									getClassrooms();
									 $("#addClassroomModal").modal("hide");
									 $("#succAddMsg").hide();
								}, 2000);
							}
							else
							{
								$("#errorMsg").html("Error in adding the Classroom.");
								$("#errorAddMsg").show();
								
							}
								
					  },
								
					error: function(data){
							console.log(data);
						  }
					}); 
			}
			
		});
		
		$("#editCaretakerAccountForm").submit(function(e) {
			e.preventDefault();
			var surname = $("#inputSurname").val();
			var name = $("#inputName").val();
			var email = $("#email").val();
			var telephone = $("#inputTelephone").val();
			var address = $("#inputAddress").val();
			var userID = $("#userID").val();
			var gender = $("input[name=gender]").val();
			var bday = $("input[name=birthdate]").val();

			if(surname != "" && name != "" && telephone != "" && address != "" && gender != "" && bday != "" && email != ""){
				 $.ajax({
					type: "POST",
					url: "user/updateProfileAccount",
					dataType: "json",
					data:  {surname:surname, name:name, telephone:telephone, address:address, gender:gender, bday:bday, userID:userID, email:email},
					success:
					  function(data) {
							if (data == "true")
							{
								$("#succMsg").html("Caretaker's account successfully updated.");
								$("#succEditMsg").show(); 

								setTimeout(function(){  
									getCaretakers();
									 $("#editCaretakerAccountModal").modal("hide");
									 $("#succEditMsg").hide();
								}, 1700);
							}
							else
							{
								$("#errorMsg").html("Error in updating the caretaker.");
								$("#errorEditMsg").show();
								
							}
								
					  },
								
					error: function(data){
							console.log(data);
						  }
					}); 
			}
			
			
		});
		
		
		$("#editParentAccountForm").submit(function(e) {
			e.preventDefault();
			var surname = $("#parentSurname").val();
			var name = $("#parentName").val();
			var email = $("#parentEmail").val();
			var telephone = $("#parentTelephone").val();
			var address = $("#parentAddress").val();
			var userID = $("#parentID").val();
			var gender = $("input[name=parentGender]").val();
			var bday = $("input[name=parentBday]").val();

			if(surname != "" && name != "" && telephone != "" && address != "" && gender != "" && bday != "" && email != ""){
				 $.ajax({
					type: "POST",
					url: "user/updateProfileAccount",
					dataType: "json",
					data:  {surname:surname, name:name, telephone:telephone, address:address, gender:gender, bday:bday, userID:userID, email:email},
					success:
					  function(data) {
							if (data == "true")
							{
								$("#succEditParentMsg").html("Parent's account successfully updated.");
								$("#succEditParentMsg").show(); 

								setTimeout(function(){  
									getParents();
									 $("#editParentAccountModal").modal("hide");
									 $("#succEditParentMsg").hide();
								}, 1700);
							}
							else
							{
								$("#errorEditParentMsg").html("Error in updating the parent.");
								$("#errorEditParentMsg").show();
								
							}
								
					  },
								
					error: function(data){
							console.log(data);
						  }
					}); 
			}
			
			
		});
		
		
		$("#editChildAccountForm").submit(function(e) {
			e.preventDefault();
			var surname = $("#childSurname").val();
			var name = $("#childName").val();
			var telephone = $("#childTelephone").val();
			var address = $("#childAddress").val();
			var userID = $("#childID").val();
			var gender = $("input[name=childGender]").val();
			var bday = $("input[name=childBday]").val();
			var parentID = $("#ParentinChildID").val();
			var classID = $("#classroomInChildID").val();
			var level = $("#editChildLevelId").val();

			if(parentID != "" && classID != "" && surname != "" && name != "" && telephone != "" && address != "" && gender != "" && bday != ""){
				 $.get('/eccs/user/getClassroomLevel/'+classID, {},function(data) {
					
					if(data[0].levelID != level){
						console.log(data[0].levelID);
						console.log(level);
						$(".errorEditChildMsg").html("Classroom level is not equal to child level.");
								$(".errorEditChildMsg").show();
								console.log(0);
					}
					else{
						console.log(1);
						 $.ajax({
							type: "POST",
							url: "user/updateChildAccount",
							dataType: "json",
							data:  {surname:surname, name:name, telephone:telephone, address:address, gender:gender, bday:bday, userID:userID, parentID:parentID, classID:classID},
							success:
							  function(data) {
									if (data == "true")
									{
										$("#succEditChildMsg").html("Child's account successfully updated.");
										$("#succEditChildMsg").show(); 
										$(".errorEditChildMsg").hide();

										setTimeout(function(){  
											getUserChild();
											 $("#editChildAccountModal").modal("hide");
											 $("#succEditChildMsg").hide();
										}, 1700);
									}
									else
									{
										$(".errorEditChildMsg").html("Error in updating the child.");
										$(".errorEditChildMsg").show();
										
									}
										
							  },
										
							error: function(data){
									console.log(data);
								  }
							}); 
					}
				}, "json");
			}
			
			
			
		});
		
		//EDIT Classroom //
		$("#editClassroomForm").submit(function(e) {
			e.preventDefault();
			var name = $("#inputClassname").val();
			var caretakerID = $("#caretakerID").val();
			var classID = $("#classID").val();
			var level = $("#classroomInputLevel").val();

			
			if(name != "" && caretakerID != "" && level != ""){
				$.ajax({
					type: "POST",
					url: "user/updateClassroom",
					dataType: "json",
					data:  {caretakerID:caretakerID, name:name, level:level, classID:classID,},
					success:
					  function(data) {
							if (data == "true")
							{
								$("#succEditClassMsg").html("Classroom Successfully Updated.");
								$("#succEditClassMsg").show(); 

								setTimeout(function(){  
									getClassrooms();
									 $("#editClassroomModal").modal("hide");
									 $("#succEditClassMsg").hide();
								}, 1700);
							}
							else
							{
								$("#errorEditClassMsg").html("Error in updating the Classroom.");
								$("#errorEditClassMsg").show();
								
							}
								
					  },
								
					error: function(data){
							console.log(data);
						  }
					}); 
			}
			
		});
		autoParentNames();
		 autoClassroomNames();
		 autoCaretakerNames();
		 getNames(6);
		
});

document.getElementById('manageusers').addEventListener("click", verifyAdmin);
document.getElementById('clickverifyUserPills').addEventListener("click", verifyAdmin);
document.getElementById('clickCaretakerPills').addEventListener("click", getCaretakers);
document.getElementById('clickParentsPills').addEventListener("click", getParents);
document.getElementById('clickChildrenPills').addEventListener("click", getUserChild);
document.getElementById('clickClassroomPills').addEventListener("click", getClassrooms);
	function ucwords (str) {
    return (str + '').replace(/^([a-z])|\s+([a-z])/g, function ($1) {
        return $1.toUpperCase();
    });
	}
function role (str) {
	if(str == 1){
		return "Moderator";
	}
	else if(str == 2){
		return "Caretaker";
	}
	else if(str == 3){
		return "Parent";
	}
	else if(str == 4){
		return "Child";
	}
}

function gender (str) {
	if(str == 1){
		return "Male";
	}
	else if(str == 2){
		return "Female";
	}
}

function verifyMe(id){
	$('#verifyUserModal').modal("show");
	
	$("#goVerify").click(function(e) {
			$.get('/eccs/user/adminVerified/'+id, {},function(data) {
			$('#verifyUserModal').modal("hide");
			verifyAdmin();
	}, "json");
	});

}

	function verifyAdmin() {
			$("#ajaxManage").show();
			var verifyTable = $('#VerifydataTables').DataTable();
			verifyTable.clear().draw();
			
			$.ajax({
					type: "get",
					url: "user/getAdminVerify",
					dataType: "json",
					success:
					  function(data) {
							for(var a=0, len=data.length; a<len; a++) {
								if(data[a].roleID == 2 || data[a].roleID == 3){ 
									verifyTable.row.add( [
										ucwords(data[a].firstName)+" "+ucwords(data[a].lastName),
										role(data[a].roleID),
										data[a].telephone,
										data[a].address,
										gender(data[a].gender),
										data[a].dateOfBirth,
										"<div class='text-center'><a onclick='verifyMe("+data[a].userID+")' class='btn btn-default'>Verify</a></div>",
										"<div class='text-center'><a onclick='deleteVerify("+data[a].userID+")' class='btn btn-danger'><span class='fa fa-trash-o'></span></a></div>"
									] ).draw( false );
								}
							}
							$("#ajaxManage").hide();
					  },
								
					error: function(data){
							console.log(data);
						  }
					});
		}
	function deleteVerify(id) {
		console.log('i was clicked');
		$('#deleteUnverifiedUserModal').modal("show");
	
		$("#dltUnverifyUser").click(function(e) {
				$.get('/eccs/user/removeUser/'+id, {},function(data) {
				if(data == "true"){
					$('#deleteUnverifiedUserModal').modal("hide");
					verifyAdmin();
				}
				else{
					$('#deleteUnverifiedUserModal').modal("hide");
				}
				
		}, "json");
		});
	}
	function getCaretakers() {
			$("#ajaxManage").show();
			var caretakerTable = $('#caretakerDataTables').DataTable();
			caretakerTable.clear().draw();
			
				$.ajax({
					type: "get",
					url: "user/getCaretakers",
					dataType: "json",
					success:
					  function(data) {
							for(var a=0, len=data.length; a<len; a++) {
								caretakerTable.row.add( [
									ucwords(data[a].firstName)+" "+ucwords(data[a].lastName),
									data[a].emailAddress,
									data[a].telephone,
									data[a].address,
									gender(data[a].gender),
									data[a].dateOfBirth,
									"<div class='text-center'><a onclick='assignedCaretaker("+data[a].userID+")' class='btn btn-default'>View All</a></div>",
									"<div class='text-center'><a onclick='editCaretaker("+data[a].userID+")' class='btn btn-warning'><span class='fa fa-pencil-square-o'></span></a></div>",
									"<div class='text-center'><a onclick='removeCaretaker("+data[a].userID+")' class='btn btn-danger'><span class='fa fa-trash-o'></span></a></div>"
								] ).draw( false );
							}
							$("#ajaxManage").hide();
					  },
								
					error: function(data){
							console.log(data);
						  }
					});
	}
	function editCaretaker(id) {
		$('#editCaretakerAccountModal').modal("show");
		$.get('/eccs/user/getUserInfo/'+id, {},function(data) {
			console.log(data);
			$('#userID').val(data[0].userID);
			$('#inputSurname').val(data[0].lastName);
			$('#inputName').val(data[0].firstName);
			$('#email').val(data[0].emailAddress);
			$('#inputAddress').val(data[0].address);
			$('#inputTelephone').val(data[0].telephone);
			$("input[name=birthdate]").val(data[0].dateOfBirth);
			$("input[name=gender][value="+data[0].gender+"]").prop("checked",true);
		  }, "json");
	}
	function removeCaretaker(id) {
		$('#deleteCaretakerModal').modal("show");
	
		$("#dltCaretaker").click(function(e) {
				$.get('/eccs/user/removeUser/'+id, {},function(data) {
				if(data == "true"){
					$('#deleteCaretakerModal').modal("hide");
					getCaretakers();
				}
				else{
					$('#deleteCaretakerModal').modal("hide");
				}
				
		}, "json");
		});
		
	}
	function getParents() {
			$("#ajaxManage").show();
			var parentTable = $('#parentsDataTables').DataTable();
			parentTable.clear().draw();
			
			$.ajax({
					type: "get",
					url: "user/getParents",
					dataType: "json",
					success:
					  function(data) {
							for(var a=0, len=data.length; a<len; a++) {
								parentTable.row.add( [
									ucwords(data[a].firstName)+" "+ucwords(data[a].lastName),
									data[a].emailAddress,
									data[a].telephone,
									data[a].address,
									gender(data[a].gender),
									data[a].dateOfBirth,
									"<div class='text-center'><a onclick='myChildren("+data[a].userID+")' class='btn btn-default'>View All</a></div>",
									"<div class='text-center'><a onclick='editParent("+data[a].userID+")' class='btn btn-warning'><span class='fa fa-pencil-square-o'></span></a></div>",
									"<div class='text-center'><a onclick='removeParent("+data[a].userID+")' class='btn btn-danger'><span class='fa fa-trash-o'></span></a></div>"
								] ).draw( false );
							}
							$("#ajaxManage").hide();
					  },
								
					error: function(data){
							console.log(data);
						  }
					});
	}
	function editParent(id) {
		$("#errorEditParentMsg").hide();
		$("#succEditParentMsg").hide();
		$('#editParentAccountModal').modal("show");
		$.get('/eccs/user/getUserInfo/'+id, {},function(data) {
			console.log(data);
			$('#parentID').val(data[0].userID);
			$('#parentSurname').val(data[0].lastName);
			$('#parentName').val(data[0].firstName);
			$('#parentEmail').val(data[0].emailAddress);
			$('#parentAddress').val(data[0].address);
			$('#parentTelephone').val(data[0].telephone);
			$("input[name=parentBday]").val(data[0].dateOfBirth);
			$("input[name=parentGender][value="+data[0].gender+"]").prop("checked",true);
		  }, "json");
	}
	function removeParent(id) {
		$('#deleteParentModal').modal("show");
	
		$("#dltParent").click(function(e) {
				$.get('/eccs/user/removeUser/'+id, {},function(data) {
				if(data == "true"){
					$('#deleteParentModal').modal("hide");
					getParents();
				}
				else{
					$('#deleteParentModal').modal("hide");
				}
				
		}, "json");
		});
	}
	function getUserChild() {
			$("#ajaxManage").show();
			setTimeout(function(){
				var childrenTable = $('#childrenDataTables').DataTable();
				childrenTable.clear().draw();
				
				$.ajax({
					type: "get",
					url: "user/getUserChild/",
					dataType: "json",
					async: false,
					success:
					  function(data) {
							console.log(data);
							for(var a=0, len=data.length; a<len; a++) {
								childrenTable.row.add( [
									ucwords(data[a].firstName)+" "+ucwords(data[a].lastName),
									getNames(data[a].parentID),
									getNames(data[a].caretakerID),
									data[a].telephone,
									// data[a].address,
									data[a].levelName,
									gender(data[a].gender),
									data[a].dateOfBirth,
									"<div class='text-center'><a onclick='levelUp("+data[a].userID+","+data[a].uLevelID+")' class='btn btn-success'><span class='fa fa-level-up'></span></a></div>",
									"<div class='text-center'><a onclick='editChild("+data[a].userID+")' class='btn btn-warning'><span class='fa fa-pencil-square-o'></span></a></div>",
									"<div class='text-center'><a onclick='deleteChild("+data[a].userID+")' class='btn btn-danger'><span class='fa fa-trash-o'></span></a></div>"
									] ).draw( false );
							}
							$("#ajaxManage").hide();
					  },
								
					error: function(data){
							console.log(data);
						  }
					});
			}, 500);
	}
	
	function levelUp(userID,level) {
		$('#nextLevelModal').modal("show");
		
		$("#nxtLvl").click(function(e) {
			$.ajax({
					type: "post",
					url: "user/levelUpChild/",
					dataType: "json",
					data:  {userID:userID, level:level},
					async: false,
					success:
					  function(data) {
							getUserChild();
							$('#nextLevelModal').modal("hide");
					  },
								
					error: function(data){
							$('#nextLevelModal').modal("hide");
							console.log(data);
						  }
					});
		});
	}
	function editChild(id) {
	
		$('#editChildAccountModal').modal("show");
		
		$.get('/eccs/user/getUserInfo/'+id, {},function(data) {
			console.log(data);
			console.log(data[0].childLevel);
			$('#childID').val(data[0].userID);
			$('#editChildLevelId').val(data[0].childLevel);
			$('#childSurname').val(data[0].lastName);
			$('#childName').val(data[0].firstName);
			$('#childEmail').val(data[0].emailAddress);
			$('#childAddress').val(data[0].address);
			$('#childTelephone').val(data[0].telephone);
			$('#ParentinChildID').val(data[0].parentID);
			$('#classroomInChildID').val(data[0].classroomID);
			$('#inputParentinChildID').val(getNames(data[0].parentID));
			$('#inputClassroominChildID').val(data[0].name);
			$("input[name=childBday]").val(data[0].dateOfBirth);
			$("input[name=childGender][value="+data[0].gender+"]").prop("checked",true);
		  }, "json");
		
	}
	function deleteChild(id) {
		
		$('#deleteChildModal').modal("show");
	
		$("#dltChild").click(function(e) {
				$.get('/eccs/user/removeChild/'+id, {},function(data) {
				if(data == "true"){
					$('#deleteChildModal').modal("hide");
					getUserChild();
				
				}
				else{
					$('#deleteChildModal').modal("hide");
				}
				
		}, "json");
		});
	}
	function assignedCaretaker(id) {	
		var assignedChildToCaretakerDataTables = $('#assignedChildToCaretakerDataTables').DataTable();
		assignedChildToCaretakerDataTables.clear().draw();
			
			$.get('/eccs/user/getAssignedChildToCaretaker/'+id, {},function(data) {
				console.log(data);
				$('#assignedChildToCaretaker').modal("show");
				for(var a=0, len=data.length; a<len; a++) {
						assignedChildToCaretakerDataTables.row.add( [
						ucwords(data[a].firstName)+" "+ucwords(data[a].lastName),
						data[a].parentName,
						data[a].telephone,
						data[a].address,
						gender(data[a].gender),
						data[a].dateOfBirth
						] ).draw( false );
				}
            }, "json");
	}
	
	function myChildren(id) {
		
		var myChildrenDataTablesDataTables = $('#myChildrenDataTables').DataTable();
		myChildrenDataTablesDataTables.clear().draw();
			
			if(id == ""){
				$('#myChildren').modal("show");
			}
			else{
				$.get('/eccs/user/getMyChildren/'+id, {},function(data) {
					console.log(data);
					$('#myChildren').modal("show");
					for(var a=0, len=data.length; a<len; a++) {
							myChildrenDataTablesDataTables.row.add( [
							ucwords(data[a].firstName)+" "+ucwords(data[a].lastName),
							data[a].caretakerName,
							data[a].telephone,
							data[a].address,
							gender(data[a].gender),
							data[a].dateOfBirth,
							"<div class='text-center'><a onclick='deleteMyChild("+data[a].userID+","+id+")' class='btn btn-danger'><span class='fa fa-trash-o'></span></a></div>"
							] ).draw( false );
					}
				}, "json");
			}
	}
	function deleteMyChild(id,parentID) {
			$('#deleteChildInParentModal').modal("show");
			$('#myChildren').modal("hide");
			
			$("#dltChildInParent").click(function(e) {
					$.ajax({
						type: "POST",
						url: "user/removeMyChild",
						dataType: "json",
						data:  { id:id},
						success:
						  function(data) {
							if(data == "true"){
								myChildren(parentID);	
								$('#deleteChildInParentModal').modal("hide");
								$('#myChildren').modal("show");
							}else{
								$('#deleteChildInParentModal').modal("hide");
								$('#myChildren').modal("show");
							}
											
						  },
									
						error: function(data){
								console.log(data);
							  }
					});
			});
	}
	
	function autoParentNames() {
					 $.ajax({  
                        url: "user/getParentID",  
                        dataType: 'json',  
                        type: 'POST', 
                        success:      
                        function(data){  
						parentNames = data.message;
						$("#inputParentID").autocomplete({  
							minLength: 1,  
							source:  data.message, 
							 select: function( event, ui ) {
									$( "#inputParentID" ).val( ui.item.value );
									$("#ParentID").val(ui.item.id);    
									return false;
							}
							      
						}),
						$("#inputParentinChildID").autocomplete({  
							minLength: 1,  
							source:  data.message, 
							 select: function( event, ui ) {
									$( "#inputParentinChildID" ).val( ui.item.value );
									$("#ParentinChildID").val(ui.item.id);    
									return false;
							}
							      
						})
                        },  
                    })
		}
	
		function autoClassroomNames() {
					 $.ajax({  
                        url: "user/autoGetClassroom",  
                        dataType: 'json',  
                        type: 'POST', 
                        success:      
                        function(data){  
						parentNames = data.message;
						$("#inputClassroomID").autocomplete({  
							minLength: 1,  
							source:  data.message, 
							 select: function( event, ui ) {
									$( "#inputClassroomID" ).val(ui.item.value);
									$("#ClassroomID").val(ui.item.id);    
									return false;
							}
							      
						}),
						$("#inputClassroominChildID").autocomplete({  
							minLength: 1,  
							source:  data.message, 
							 select: function( event, ui ) {
									$( "#inputClassroominChildID" ).val( ui.item.value );
									$("#classroomInChildID").val(ui.item.id);    
									return false;
							}
							      
						})
                        },  
                    })
		}
		
		function getClassrooms() {
			$("#ajaxManage").show();
			var classroomDataTables = $('#classroomDataTables').DataTable();
			classroomDataTables.clear().draw();

			$.ajax({
					type: "get",
					url: "user/getClassroomWithCaretaker",
					dataType: "json",
					success:
					  function(data) {
						  console.log(data);
							for(var a=0, len=data.length; a<len; a++) {
								classroomDataTables.row.add( [
									ucwords(data[a].cname),
									data[a].firstName != null ? ucwords(data[a].firstName)+" "+ucwords(data[a].lastName) : '',
									"<div class='text-center'>"+data[a].levelName+"</div>",
									"<div class='text-center'><a onclick='allChildrenInClassroom("+data[a].classroomID+")' class='btn btn-default'>View All</a></div>",
									"<div class='text-center'><a onclick='editClassroom("+data[a].classroomID+")' class='btn btn-warning'><span class='fa fa-pencil-square-o'></span></a></div>",
									"<div class='text-center'><a onclick='removeClassroom("+data[a].classroomID+")' class='btn btn-danger'><span class='fa fa-trash-o'></span></a></div>"
									] ).draw( false );
							}
							$("#ajaxManage").hide();
					  },
								
					error: function(data){
							console.log(data);
						  }
					});
		}
		function allChildrenInClassroom(id) {
			
			var childrenInClassroomDataTables = $('#childrenInClassroomDataTables').DataTable();
		childrenInClassroomDataTables.clear().draw();
			
			if(id == ""){
				$('#childrenInClassroom').modal("show");
			}
			else{
				$.get('/eccs/user/allChildrenInClassroom/'+id, {},function(data) {
					console.log(data);
					$('#childrenInClassroom').modal("show");
					for(var a=0, len=data.length; a<len; a++) {
							childrenInClassroomDataTables.row.add( [
							ucwords(data[a].firstName)+" "+ucwords(data[a].lastName),
							data[a].telephone,
							data[a].address,
							gender(data[a].gender),
							data[a].dateOfBirth,
							"<div class='text-center'><a onclick='removeChildInClass("+data[a].userID+","+id+")' class='btn btn-danger'><span class='fa fa-trash-o'></span></a></div>"
							] ).draw( false );
					}
				}, "json");
			}
		}
		function removeChildInClass(id,classID){
			
			$('#deleteChildInClassModal').modal("show");
			$('#childrenInClassroom').modal("hide");
			
			$("#dltChildInClass").click(function(e) {
					$.ajax({
						type: "POST",
						url: "user/removeChildInClass",
						dataType: "json",
						data:  { id:id},
						success:
						  function(data) {
							if(data == "true"){
								allChildrenInClassroom(classID);	
								$('#deleteChildInClassModal').modal("hide");
								$('#childrenInClassroom').modal("show");
							}else{
								$('#deleteChildInClassModal').modal("hide");
								$('#childrenInClassroom').modal("show");
							}
											
						  },
									
						error: function(data){
								console.log(data);
							  }
					});
			});
		}
		function editClassroom(id) {
			$("#errorEditClassMsg").hide();
			$("#succEditClassMsg").hide();
			$('#editClassroomModal').modal("show");
			$.get('/eccs/user/getClassroomInfo/'+id, {},function(data) {
				console.log(data);
				console.log(data[0].firstName);
				$('#classID').val(data[0].classroomID);
				$('#caretakerID').val(data[0].caretakerID);
				$('#inputClassname').val(data[0].name);
				if(data[0].caretakerID == null){
					$('input[name=EditCaretaker]').val();
					console.log("1");
				}else{
					$('input[name=EditCaretaker]').val(ucwords(data[0].firstName)+" "+ucwords(data[0].lastName));
					console.log("2");
				}
				
				$('#classroomInputLevel option[value='+data[0].clevel+']').attr("selected", "selected");
				
			  }, "json");
		}
		function removeClassroom(id) {
			
			$('#deleteClassModal').modal("show");
	
			$("#dltClass").click(function(e) {
					$.get('/eccs/user/removeClassroom/'+id, {},function(data) {
						if(data == "true"){
							$('#deleteClassModal').modal("hide");
							getClassrooms();
						}
						else{
							$('#deleteClassModal').modal("hide");
						}
					
				}, "json");
			});
		}
		
		function autoCaretakerNames() {
					 $.ajax({  
                        url: "user/getCaretakerID",  
                        dataType: 'json',  
                        type: 'POST', 
                        success:      
                        function(data){  
						parentNames = data.message;
						$("#inputCaretaker").autocomplete({  
							minLength: 1,  
							source:  data.message, 
							 select: function( event, ui ) {
									$( "#inputCaretaker" ).val( ui.item.value );
									$("#caretakerID").val(ui.item.id);    
									return false;
							}
							      
						}),
						$("#inputCaretakerA").autocomplete({  
							minLength: 1,  
							source:  data.message, 
							 select: function( event, ui ) {
									$( "#inputCaretakerA" ).val( ui.item.value );
									$("#caretakerIDA").val(ui.item.id);    
									return false;
							}
							      
						})
                        },  
                    })
		}
		
		function getNames(id) {
				$("#ajaxManage").show();
				var result = null;

				$.ajax({
					type: "get",
					url: "user/getNames/"+id,
					dataType: "json",
					async: false,
					success:
					  function(data) {
							if(data == "" || data == "0"){
								result = "";
							}
							else{
								result = ucwords(data[0].firstName)+" "+ucwords(data[0].lastName);
							}
							
							$("#ajaxManage").hide();
					  },
								
					error: function(data){
							console.log(data);
						  }
					});
				return result;
				
		}