$(document).ready(function() {
    //User Settings//
		$("#errorUserSetMsg").hide();
		$("#succUserSetMsg").hide();
		$("#userSettingForm").submit(function(e) {
			e.preventDefault();
			var pass = $("#settingNewPassword").val();
			var repass = $("#settingReNewPassword").val();
			
			if(pass != repass){
				$("#errorUserSetMsg").show();
			}
			else{
				$("#errorUserSetMsg").hide();
				$.ajax({
					type: "POST",
					url: "user/changeUserPassword",
					dataType: "json",
					data:  { pass:pass},
					success:
					  function(data) {
							if (data == "true")
							{
								$("#succUserSetMsg").show(); 
								$("#settingNewPassword").val("") ;
								$("#settingReNewPassword").val("");
							}
							else
							{
								$("#errorUserSetMsg").show();
								
							}
								
					  },
								
				error: function(data){
						console.log(data);
					  }
				});
				return false;
			}
		});
});