/*!
 * Start Bootstrap - SB Admin 2 v3.3.7+1 (http://startbootstrap.com/template-overviews/sb-admin-2)
 * Copyright 2013-2016 Start Bootstrap
 * Licensed under MIT (https://github.com/BlackrockDigital/startbootstrap/blob/gh-pages/LICENSE)
 */
$(function() {
    $('#side-menu').metisMenu();
});

//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size
$(function() {
    $(window).bind("load resize", function() {
        var topOffset = 50;
        var width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $('div.navbar-collapse').addClass('collapse');
            topOffset = 100; // 2-row-menu
        } else {
            $('div.navbar-collapse').removeClass('collapse');
        }

        var height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            $("#page-wrapper").css("min-height", (height) + "px");
        }
    });

    var url = window.location;
    // var element = $('ul.nav a').filter(function() {
    //     return this.href == url;
    // }).addClass('active').parent().parent().addClass('in').parent();
    var element = $('ul.nav a').filter(function() {
        return this.href == url;
    }).addClass('active').parent();

    while (true) {
        if (element.is('li')) {
            element = element.parent().addClass('in').parent();
        } else {
            break;
        }
    }
	
	// adds functionality to sidebar
	$('#side-menu > li').click(function () {
		var targetID = $(this).data('targetid');
		if(targetID !== undefined) {
			$(this).parent().children().children().removeClass('active');
			$(this).children().addClass('active').blur();
			
			var parent = $('#page-wrapper');
			parent.children().hide();
			parent.children('#'+ targetID).show();
			
			switch(targetID) {
				case 'child-dashboard-container':
					refresh.notificationPanel();
					break;
				case 'parent-caretaker-dashboard-container':
					
					// updates notification panel
					refresh.notificationPanel();
					$('#form-search-parents-caretakers').trigger('submit');

					break;
				case 'guidance-container':
					refresh.guidanceVideos();
					break;
				case 'gamelistings-container':
					refresh.gamelistings();
					break;
			}
		} else {
			if($(this).data('gameplay') !== undefined) {
				$.ajax({
					beforeSend: function() {
						// listAssigned.empty();
						// listAssigned.append('<div class="ajax-loader"></div>');
					},
					type: "POST",
					url: "dashboard/searchAssignedUsers",
					dataType: "json",
					data:  'name=',
					success:
					function(data) {
						console.log(data);
						var select = $('#childGameSelect'),
							data = data.result;
						select.empty();
						for(var x = 0, length = data.length ; x< length; x++) {
							select.append($('<option>', {value:data[x].userID }).append(data[x].firstName + ' ' + data[x].lastName));
						}
						$("#childGameModal").modal('show');
					}
				});

			}
		}
	});
	
	var refresh = {
		gamelistings: function() {
			$.ajax({
				type: "POST",
				url: "dashboard/getGameListings",
				dataType: "json",
				success:
				function(data) {
					console.log(data);
					var gameContainer = $('#game-list');
					gameContainer.empty();
					for(var x = 0 ;x < data.length; x++) {
						// console.log(data[x]);
						var name = data[x].name;
						
						var item = $('<div>',{class:'item col-xs-4 col-lg-4'}),
							thumbnail = $('<div>',{class:'thumbnail'}),
							img = $('<img>', {class:'group list-group-image', src:"./games/play/"+data[x].name+"/screenshot.png",alt:"Image here"}),
							divCaption = $('<div>',{class:'caption'}),
							title = $('<h4>', {class:'group inner list-group-item-heading'}).append(data[x].name),
							// description = $('<p>', {class:'group inner list-group-item-text'}).append(data[x].description),
							btnContainer = $('<div>', {class:"btn-container",style:"text-align:center;"});
							// btnWatch = $('<a>', {class:'btn btn-success',target:'_blank',href:"./games/play/"+data[x].name+"/index.html"}).append("Play this game");
							btnWatch = $('<a>', {class:'btn btn-success',target:'_blank',href:"./dashboard/play/"+data[x].gameID+"/"+data[x].name}).append("Play this game");
							btnDelete = $('<a>', {class:'btn btn-danger','data-name':data[x].name,'data-id':data[x].gameID}).append('Delete');
							btnDelete.click(function() {
								
								if(confirm("Are you sure you want to delete this?")) {
									var el = $(this),
										gameID = el.data('id'),
										gameName = el.data('name');
									console.log('I was clicked');
									$.ajax({
										type: "POST",
										url: "dashboard/deleteGame",
										dataType: "json",
										data: {
											gameid: gameID,
											gamename: gameName
										},
										success:
										  function(data) {
											  console.log(data);
											  if(data == true) {
												  console.log(el);
												  el.parent().parent().parent().parent().remove();
											  }
										},
										error: function(data){
											console.log(data);
										  }
									});
									
								}
							});
							btnContainer.append(btnWatch).append(btnDelete);
							
							divCaption.append(title)
							// .append(description)
							.append(btnContainer);
							thumbnail.append(img).append(divCaption);
							item.append(thumbnail);
							gameContainer.append(item);
					}
				}
			});
		},
		guidanceVideos: function() {
			$.ajax({
				type: "POST",
				url: "dashboard/getGuidanceVideos",
				dataType: "json",
				success:
				function(data) {
					console.log(data);
					var guidanceContainer = $('#guidance-videos-list');
					guidanceContainer.empty();
					for(var x = 0 ;x < data.length; x++) {
						// console.log(data[x]);
						var hashes = data[x].filename.slice(data[x].filename.indexOf('?') + 1).split('&'),
							hash = [],
							imageUrl = '';
							
						for(var i = 0; i < hashes.length; i++)
						{
							hash = hashes[i].split('=');
							if(hash[0] == 'v') {
								imageUrl = 'https://img.youtube.com/vi/'+hash[1]+'/0.jpg';
								break;
							}
						}
						
						var item = $('<div>',{class:'item col-xs-4 col-lg-4'}),
							thumbnail = $('<div>',{class:'thumbnail'}),
							img = $('<img>', {class:'group list-group-image', src:imageUrl}),
							divCaption = $('<div>',{class:'caption'}),
							title = $('<h4>', {class:'group inner list-group-item-heading'}).append(data[x].title),
							description = $('<p>', {class:'group inner list-group-item-text'}).append('description'),
							btnContainer = $('<div>', {class:"btn-container",style:"text-align:center;"});
							btnWatch = $('<a>', {class:'btn btn-success',target:'_blank',href:"./dashboard/watch?v="+data[x].videoID}).append('Watch this video');
							btnDelete = $('<a>', {class:'btn btn-danger','data-id':data[x].videoID}).append('Delete');
							btnDelete.click(function() {
								
								if(confirm("Are you sure you want to delete this?")) {
									var videoID = $(this).data('id');
									var el = $(this);
									console.log('I was clicked');
									$.ajax({
										type: "POST",
										url: "dashboard/deleteVideo",
										dataType: "json",
										data: 'videoid='+ videoID ,
										success:
										  function(data) {
											  console.log(data);
											  if(data == true) {
												  console.log(el);
												  el.parent().parent().parent().parent().remove();
											  }
										},
										error: function(data){
											console.log(data);
										  }
									});
									
								}
							});
							btnContainer.append(btnWatch).append(btnDelete);
							
							divCaption.append(title)
							// .append(description)
							.append(btnContainer);
							thumbnail.append(img).append(divCaption);
							item.append(thumbnail);
							guidanceContainer.append(item);
					}
				}
			});
			
		},
		notificationPanel: function() {
			var notifications = $('#panel-notifications'),
				notifications2 = $('#parent-caretaker-notification-panel');
								 
			$.ajax({
				beforeSend: function() {
					notifications.empty()
								 .append('<div class="ajax-loader"></div>');
					notifications2.empty()
								 .append('<div class="ajax-loader"></div>');
				},
				type: "POST",
				url: "dashboard/getNotifications",
				dataType: "json",
				success:
				function(data) {
					notifications.empty();
					notifications.append($('<a>',{href:"#", class:'list-group-item'})
														.append($('<i>',{class:'fa fa-pencil fa-fw'}))
														.append('  '+ (parseInt(data.totalAssignedChildren) - parseInt(data.numCompletedThisWeek)) + ' children need spice input')
														.click(function() {
															// too add a fnction here
															
														})
												);
					notifications.append($('<a>',{href:"javascript:void(0)", class:'btn-newusers list-group-item'})
														.append($('<i>',{class:'fa fa-user-plus fa-fw'}))
														.append('  '+ (data.newUsers) + ' new users')
														.click(function() {
															$('#manageusers').trigger('click');
														})
												);
												
					notifications2.empty();
					notifications2.append($('<a>',{href:"#", class:'list-group-item'})
														.append($('<i>',{class:'fa fa-pencil fa-fw'}))
														.append('  '+ (parseInt(data.totalAssignedChildren) - parseInt(data.numCompletedThisWeek)) + ' children need spice input')
														.click(function() {
															// too add a fnction here
															
														})
												);
					notifications2.append($('<a>',{href:"javascript:void(0)", class:'btn-newusers list-group-item'})
														.append($('<i>',{class:'fa fa-user-plus fa-fw'}))
														.append('  '+ (data.newUsers) + ' new users')
														.click(function() {
															$('#manageusers').trigger('click');
														})
												);
				}
			});
			
		}
	};
					

	var init_once = false;
	
	// search users in dashboard
	$('#form-search-assigned-users').submit(function() {
			console.log('i was clicked');
			// <div class="ajax-loader"></div>
			var listAssigned = $('#assigned-list');
			console.log($(this).serialize());
		  	$.ajax({
				beforeSend: function() {
					listAssigned.empty();
					listAssigned.append('<div class="ajax-loader"></div>');
				},
				type: "POST",
				url: "dashboard/searchAssignedUsers",
				dataType: "json",
				data:  $(this).serialize(),
				success:
				  function(data) {
					  
					  listAssigned.empty();
					  var results = data.result,
							length = results.length;
					 // var length = 5; // sample length
					  
					  for(var x = 0 ; x < length; x++) {
						  var today = new Date(),
								 lastModified = new Date(results[x].lastModified),
								 imgName = results[x].imageName;
								 
						  var name = results[x].firstName + ' ' + results[x].lastName,
								 timeDiff = Math.abs(today.getTime() - lastModified.getTime()),
								 diffSeconds = Math.ceil(timeDiff / 1000 ),
								 diffMinutes = Math.ceil(timeDiff / 1000 * 60),
								 diffHours = Math.ceil(timeDiff / (1000 * 3600)),
								 diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)),
								 lastUpdate = '';
							
							if(diffSeconds < 60) {
								lastUpdate = diffSeconds + ' second' +(diffSeconds == 1 ? '' :'s')+ ' ago';
							} else if(diffMinutes < 60) {
								lastUpdate = diffMinutes + ' minute' +(diffMinutes == 1 ? '' :'s')+ ' ago';
							} else if(diffHours <= 24) {
								lastUpdate = diffHours + ' hour' +(diffHours == 1 ? '' :'s')+ ' ago';
							} else  {
								lastUpdate = diffDays + ' day' +(diffDays == 1 ? '' :'s')+ ' ago';
							}
						  var listingContainer = $("<a>", {href: "javascript:void(0)", 'data-userid': results[x].userID, "class": "list-group-item"}),
								 listingImage = $("<span>").append(
																				$("<img>", {src: 'images/users/'+ imgName})
																			),
								 listingName = document.createTextNode(name),
								 listingLastUpdate = $("<span>", {class: 'pull-right text-muted small'}).append(
																				$("<em>").append(lastUpdate)
																			);
						  listingContainer.append(listingImage);
						  listingContainer.append(listingName);
						  listingContainer.append(listingLastUpdate);
						  listingContainer.click(function() { userClickedOnList($(this).data('userid')) });
						  listAssigned.append(listingContainer);
					  }
					  
					  if(init_once == false) {
						  if(listAssigned.children().length > 0) {
							listAssigned.children().eq(0).click();
						  }
						  init_once = true;
						  
					  }
					  // console.log(data);
				  },
							
			error: function(data){
					console.log(data);
				  }
			});
			return false;
	});
	
	var init_once_parent = false;
	$('#form-search-parents-caretakers').submit(function() {
			// <div class="ajax-loader"></div>
			var listParents = $('#parents-caretakers-list');
			console.log($(this).serialize());
		  	$.ajax({
				beforeSend: function() {
					listParents.empty();
					listParents.append('<div class="ajax-loader"></div>');
				},
				type: "POST",
				url: "dashboard/searchParentsCaretakers",
				dataType: "json",
				data:  $(this).serialize(),
				success:
				  function(data) {
					  
					  listParents.empty();
					  var results = data.result,
							length = results.length;
					 // var length = 5; // sample length
					  
					  for(var x = 0 ; x < length; x++) {
						  var today = new Date(),
								 lastModified = new Date(results[x].lastModified),
								 imgName = results[x].imageName;
								 
						  var name = results[x].firstName + ' ' + results[x].lastName,
								 timeDiff = Math.abs(today.getTime() - lastModified.getTime()),
								 diffSeconds = Math.ceil(timeDiff / 1000 ),
								 diffMinutes = Math.ceil(timeDiff / 1000 * 60),
								 diffHours = Math.ceil(timeDiff / (1000 * 3600)),
								 diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)),
								 lastUpdate = '';
							
							if(diffSeconds < 60) {
								lastUpdate = diffSeconds + ' second' +(diffSeconds == 1 ? '' :'s')+ ' ago';
							} else if(diffMinutes < 60) {
								lastUpdate = diffMinutes + ' minute' +(diffMinutes == 1 ? '' :'s')+ ' ago';
							} else if(diffHours <= 24) {
								lastUpdate = diffHours + ' hour' +(diffHours == 1 ? '' :'s')+ ' ago';
							} else  {
								lastUpdate = diffDays + ' day' +(diffDays == 1 ? '' :'s')+ ' ago';
							}
						  var listingContainer = $("<a>", {href: "javascript:void(0)", 'data-userid': results[x].userID, 'data-roleid':results[x].roleID,"class": "list-group-item"}),
								 listingImage = $("<span>").append(
																				$("<img>", {src: 'images/users/'+ imgName})
																			),
								 listingName = document.createTextNode(name),
								 listingLastUpdate = $("<span>", {class: 'pull-right text-muted small'}).append(
																				$("<em>").append(lastUpdate)
																			);
						  listingContainer.append(listingImage);
						  listingContainer.append(listingName);
						  listingContainer.append(listingLastUpdate);
						  listingContainer.click(function() { userClickedOnParentCaretakerList($(this).data('userid'),$(this).data('roleid')) });
						  listParents.append(listingContainer);
					  }
					  
					  if(init_once_parent == false) {
						  if(listParents.children().length > 0) {
							listParents.children().eq(0).click();
						  }
						  init_once_parent = true;
					  }
					  // console.log(data);
				  },
							
			error: function(data){
					console.log(data);
				  }
			});
			return false;
	});
	
	function userClickedOnList(userID) {
		 var childProfileContainer = $('#child-profile-info'),
				profPicDiv = childProfileContainer.children().eq(0),
				profInfoDiv = childProfileContainer.children().eq(1);
		
		// console.log(el);
		
		$.ajax({
				beforeSend: function() {
					// profPicDiv.empty();
					profInfoDiv.empty();
					// profPicDiv.append('<div class="ajax-loader"></div>');
					profInfoDiv.append('<div class="ajax-loader"></div>');
				},
				type: "POST",
				url: "dashboard/getAssignedChildData",
				dataType: "json",
				data:  { userid : userID},
				success:
				  function(data) {
					  profPicDiv.empty();
					  profInfoDiv.empty();
					  
					  var child = data.result.details[0],
						 age = Math.ceil(Math.abs((new Date()).getTime() - (new Date(child.dateOfBirth)).getTime()) / (1000 * 3600 * 24 * 365)),
						 parent = data.result.parent[0];
					
					console.log(data.result);
					  profPicDiv.append($("<img>", {src: 'images/users/'+child.filename}));
					  profInfoDiv.append($("<label>").append(child.firstName + ' ' + child.lastName))
									  .append($("<label>").append(parent == undefined ? 'No Parent':'Child of ' + parent.firstName + ' ' + parent.lastName))
									  .append($("<label>").append('Contact # '+ child.telephone))
									  .append($("<label>").append(age + ' years old'));
						
					  // adds spice form data
					  // console.log('child user id is ' + child.userID);
					  $('#spiceform_input_idchild').val(child.firstName + ' ' + child.lastName)
																.attr('data-userid', child.userID);
					  
					  if(data.result.spiceFormDetails != null) {
						  spiceFormDetails = data.result.spiceFormDetails[0];
						  $('#spiceform_input_socialdev').val(spiceFormDetails.social_detail);
						  $('#spiceform_input_physicaldev').val(spiceFormDetails.physical_detail);
						  $('#spiceform_input_intellectualdev').val(spiceFormDetails.intellectual_detail);
						  $('#spiceform_input_creativedev').val(spiceFormDetails.creative_detail);
						  $('#spiceform_input_emotionaldev').val(spiceFormDetails.emotional_detail);
					  } else {
						  $('#spiceform_input_socialdev').val('');
						  $('#spiceform_input_physicaldev').val('');
						  $('#spiceform_input_intellectualdev').val('');
						  $('#spiceform_input_creativedev').val('');
						  $('#spiceform_input_emotionaldev').val('');
					  }
					  
					  // adds activities to table and edits averages
					  
					  var activities = data.result.spiceActivities,
						  activityRatings = data.result.spiceActivityRatings,
						  socialDiv = $('#table-activities-social'),
						  physicalDiv = $('#table-activities-physical'),
						  intellectualDiv = $('#table-activities-intellectual'),
						  creativeDiv = $('#table-activities-creative'),
						  emotionalDiv = $('#table-activities-emotional'),
						  ratings = {
										numSocialActivities: 0, socialTotal:0, socialActivitiesDone: 0,
										numPhysicalActivities: 0, physicalTotal:0, physicalActivitiesDone: 0,
										numIntellectualActivities: 0, intellectualTotal:0, intellectualActivitiesDone: 0,
										numCreativeActivities: 0, creativeTotal:0, creativeActivitiesDone: 0,
										numEmotionalActivities: 0, emotionalTotal:0, emotionalActivitiesDone: 0
									};
					  socialDiv.empty();
					  physicalDiv.empty();
					  intellectualDiv.empty();
					  creativeDiv.empty();
					  emotionalDiv.empty();
					  
					  
					  function getSunday(d) {
						  d = new Date(d);
						  var day = d.getDay(),
							  diff = d.getDate() - day; // adjust when day is sunday
						  var sunday = new Date(d.setDate(diff));
						  sunday.setHours(0);
						  sunday.setMinutes(0);
						  sunday.setSeconds(0);
						  return sunday;
						}

					 var lastSunday = getSunday(new Date()),
							activitiesDoneThisWeek = []; 
							
						for(var y = 0 ; y < activityRatings.length; y++) {
							
							if(new Date(activityRatings[y].addedOn) > lastSunday) {
								activitiesDoneThisWeek.push(activityRatings[y]);
							}
							
						}
						
					  for( var x = 0, length = activities.length ; x < length ; x++) {
						for(var y = 0 ; y < activityRatings.length; y++) {
							if(activityRatings[y].spiceActivityID == activities[x].spiceActivityID) {
								activityRatings[y].spiceDevelopmentName = activities[x].spiceDevelopmentName;
							}
						}
					  }
					 
					 
					  function editActivity(el) {
							var dataSrc = el.parent().parent(),
								   spiceActivityID = dataSrc.data('spice-activity-id'),
								   spiceDevelopmentID = dataSrc.data('spice-development-id'),
								   spiceChildID = dataSrc.data('spice-child-id');
								   
							console.log(dataSrc);
							var ratingCell = dataSrc.children().eq(1),
								   commentCell = dataSrc.children().eq(2),
								   rating = ratingCell.text(),
								   comment = commentCell.text();
							
							
							
							ratingCell.empty().append($('<input>',{style:'height: 34px;text-align: center',type:'number',min:-1,max:10,value:rating}));
							commentCell.empty().append($('<input>',{class:'form-control',type:'text',value:comment}));
							el.text('Save');
							el.unbind('click');
							el.click(function() {
								saveActivity($(this));
							});
					  }
					  
					  function saveActivity(el) {
							var dataSrc = el.parent().parent(),
								   spiceActivityID = dataSrc.data('spice-activity-id'),
								   spiceDevelopmentID = dataSrc.data('spice-development-id'),
								   spiceChildID = dataSrc.data('child-id'),
								   ratingCell = dataSrc.children().eq(1),
								   commentCell = dataSrc.children().eq(2),
								   rating = ratingCell.find('input').val(),
								   comment = commentCell.find('input').val();
							
							$.ajax({
								beforeSend: function() {
									el.prop('disabled', true);
								},
								type: "POST",
								url: "dashboard/saveActivityDetail",
								dataType: "json",
								data:  {
									spiceActivityID:spiceActivityID,
									spiceDevelopmentID:spiceDevelopmentID,
									spiceChildID:spiceChildID,
									rating:rating,
									comment:comment
								},	
								success:
								function(data) {
									console.log(data);
									
									if(parseInt(rating) > 10) rating = 10;
									else if (parseInt(rating) < -1) rating = 0;
									
									ratingCell.empty().append(rating);
									commentCell.empty().append(comment);
									el.text('Edit');
									el.prop('disabled', false);
									if(data.mode == 'delete') {
										ratingCell.empty().append('No rating');
										commentCell.empty().append('No comment');
									}
								}
							});
							
							el.unbind('click');
							el.click(function() {
								editActivity($(this));
							});
					  }
					 
					  for( var x = 0, length = activities.length ; x < length ; x++) {
						  
						for(var y = 0 ; y < activitiesDoneThisWeek.length; y++) {
							if(activitiesDoneThisWeek[y].spiceActivityID == activities[x].spiceActivityID) {
								activities[x].rating = activitiesDoneThisWeek[y].rating;
								activities[x].comments = activitiesDoneThisWeek[y].comments;
								break;
							}
						}
						
						var btnEdit = $('<button>',{class:'btn btn-default'}).append('Edit');
						btnEdit.click(function() {
							editActivity($(this));
						});
						 var tr = $('<tr>',{ class:"tr-center",
													 'data-spice-activity-id':activities[x].spiceActivityID,
													 'data-spice-development-id':activities[x].spiceDevelopmentID,
													 'data-child-id':child.userID}).append($('<td>').append(activities[x].spiceActivityName))
								  .append($('<td>').append(activities[x].rating == null ? 'No rating' : activities[x].rating))
								  .append($('<td>').append(activities[x].comments == null ? 'No comment' : activities[x].comments))
								  .append($('<td>').append(btnEdit));
								  
						 switch(activities[x].spiceDevelopmentName) {
							 case 'Social': 
								 socialDiv.append(tr); 
								 ratings.numSocialActivities++;
								 
								 if(activities[x].rating != null) {
									 ratings.socialActivitiesDone++;
									 ratings.socialTotal += parseInt(activities[x].rating);
								 }
								 break;
							 case 'Physical': 
								 physicalDiv.append(tr);
								 ratings.numPhysicalActivities++;
								 if(activities[x].rating != null) {
									 ratings.physicalActivitiesDone++;
									ratings.physicalTotal += parseInt(activities[x].rating);
								 }
								 break;
							 case 'Intellectual': 
								 intellectualDiv.append(tr); 
								 ratings.numIntellectualActivities++;
								 if(activities[x].rating != null) {
									 ratings.intellectualActivitiesDone++;
									ratings.intellectualTotal += parseInt(activities[x].rating);
								 }
								 break;
							 case 'Creative': 
								 creativeDiv.append(tr); 
								 ratings.numCreativeActivities++;
								 if(activities[x].rating != null) {
									 ratings.creativeActivitiesDone++;
									ratings.creativeTotal += parseInt(activities[x].rating);
								 }
								 break;
							 case 'Emotional': 
								 emotionalDiv.append(tr); 
								 ratings.numEmotionalActivities++;
								 if(activities[x].rating != null) {
									ratings.emotionalActivitiesDone++;
									ratings.emotionalTotal += parseInt(activities[x].rating);
								 }
								 break;
						 }
					  }
					  
					$('#average_social').empty().append('Average ' + (ratings.socialActivitiesDone == 0 ? 0 : (ratings.socialTotal / ratings.socialActivitiesDone).toFixed(2)));
					$('#average_physical').empty().append('Average ' + (ratings.physicalActivitiesDone == 0 ? 0 : (ratings.physicalTotal / ratings.physicalActivitiesDone).toFixed(2)));
					$('#average_intellectual').empty().append('Average ' + (ratings.intellectualActivitiesDone == 0 ? 0 : (ratings.intellectualTotal / ratings.intellectualActivitiesDone).toFixed(2)));
					$('#average_creative').empty().append('Average ' + (ratings.creativeActivitiesDone == 0 ? 0 : (ratings.creativeTotal / ratings.creativeActivitiesDone).toFixed(2)));
					$('#average_emotional').empty().append('Average ' + (ratings.emotionalActivitiesDone == 0 ? 0 : (ratings.emotionalTotal / ratings.emotionalActivitiesDone).toFixed(2)));
					
					$('#activitiesdone-social').empty().append(ratings.socialActivitiesDone + ' out of ' + ratings.numSocialActivities + ' activities done');
					$('#activitiesdone-physical').empty().append(ratings.physicalActivitiesDone + ' out of ' + ratings.numPhysicalActivities + ' activities done');
					$('#activitiesdone-intellectual').empty().append(ratings.intellectualActivitiesDone + ' out of ' + ratings.numIntellectualActivities + ' activities done');
					$('#activitiesdone-creative').empty().append(ratings.creativeActivitiesDone + ' out of ' + ratings.numCreativeActivities + ' activities done');
					$('#activitiesdone-emotional').empty().append(ratings.emotionalActivitiesDone + ' out of ' + ratings.numEmotionalActivities + ' activities done');
					var chartData = [],
							chartDate = getSunday(new Date()),
							weeksAgo = 8;
							
					 // console.log(activityRatings);

					for(var x = 0; x < weeksAgo; x++) {
						
						var tempData = {
											period: chartDate.getFullYear()+'-'+(chartDate.getMonth() + 1)+'-'+chartDate.getDate(),
											social:0,
											physical:0,
											intellectual:0,
											creative:0,
											emotional:0
										};
						var limitDate = new Date(chartDate),
							  chart_socialActivitiesDone = 0,
							  chart_physicalActivitiesDone = 0,
							  chart_intellectualActivitiesDone = 0,
							  chart_creativeActivitiesDone = 0,
							  chart_emotionalActivitiesDone = 0;
							  
						limitDate.setDate(limitDate.getDate() + 7);
						
					 // console.log(activityRatings);
					 
						for(var y = 0, length = activityRatings.length ; y < length; y++) {
							var addedOnDate = new Date(activityRatings[y].addedOn);
							
							// console.log(activityRatings[y].spiceDevelopmentName);
							
							// console.log(addedOnDate.getTime() + ' >' + chartDate.getTime() + ' && '  + addedOnDate.getTime() + '<' + limitDate.getTime() + ' = ' + ((addedOnDate.getTime() >= chartDate.getTime() ) && (addedOnDate.getTime()  < limitDate.getTime() )) );
							if((addedOnDate.getTime() >= chartDate.getTime() ) && (addedOnDate.getTime()  < limitDate.getTime() )) {
								// console.log('limit date is');
								// console.log(limitDate);
								switch(activityRatings[y].spiceDevelopmentName) {
									case 'Social': 
										 // console.log('social');
										 tempData.social += parseInt(activityRatings[y].rating);
										 chart_socialActivitiesDone++;
										 break;
									 case 'Physical': 
										 // console.log('physical');
										 tempData.physical += parseInt(activityRatings[y].rating);
										 chart_physicalActivitiesDone++;
										 break;
									 case 'Intellectual': 
										 // console.log('intellectual');
										 tempData.intellectual += parseInt(activityRatings[y].rating);
										 chart_intellectualActivitiesDone++;
										 break;
									 case 'Creative': 
										 // console.log('creative');
										 tempData.creative += parseInt(activityRatings[y].rating);
										 chart_creativeActivitiesDone++;
										 break;
									 case 'Emotional': 
										 // console.log('emotional');
										 tempData.emotional += parseInt(activityRatings[y].rating);
										 chart_emotionalActivitiesDone++;
										 break;
								}
							}
						}

						tempData.social = (chart_socialActivitiesDone == 0 ? 0 :  tempData.social / chart_socialActivitiesDone);
						tempData.physical = (chart_physicalActivitiesDone == 0 ? 0 :  tempData.physical / chart_physicalActivitiesDone);
						tempData.intellectual = (chart_intellectualActivitiesDone == 0 ? 0 :  tempData.intellectual / chart_intellectualActivitiesDone);
						tempData.creative = (chart_creativeActivitiesDone == 0 ? 0 :  tempData.creative / chart_creativeActivitiesDone);
						tempData.emotional = (chart_emotionalActivitiesDone == 0 ? 0 :  tempData.emotional / chart_emotionalActivitiesDone);
						// console.log(tempData);
						chartData.push(tempData);
						chartDate.setDate(chartDate.getDate() -  7)	;
					}
					
					$('#morris-line-chart').empty();
					$('#morris-bar-chart').empty();
					
					Morris.Line({
							element: 'morris-line-chart',
							data: chartData,
							xkey: 'period',
							ykeys: ['social', 'physical', 'intellectual','creative','emotional'],
							labels: ['social', 'physical', 'intellectual','creative','emotional'],
							pointSize: 1,
							hideHover: 'auto',
							resize: true
						});
						
					Morris.Bar({
							element: 'morris-bar-chart',
							data: chartData,
							xkey: 'period',
							ykeys: ['social', 'physical', 'intellectual','creative','emotional'],
							labels: ['social', 'physical', 'intellectual','creative','emotional'],
							pointSize: 1,
							hideHover: 'auto',
							resize: true
						});
						
					
					// Latest Activities
					var timeline = $('#activity-timeline');
					timeline.empty();
					var gamesDone = data.result.timeline;
					var activitiesDone = data.result.timelineSpice;
					
					console.log(data);
					
					function addToTimeLine(type, data) {
						var li = $('<li>');
						var divPanel = $('<div>',{class:'timeline-panel'});
						var divHeading = $('<div>',{class:'timeline-heading'});
						var divBody = $('<div>', {class:'timeline-body'});
						
						
						if(type == 'game') {
							console.log('im in here');
							li.addClass('timeline-inverted');
							divHeading.append($('<h4>',{class:'timeline-title'}).append("Just played " + data.name + "!"));
							divHeading.append($('<p>').append('<small>', {class:'text-muted'})
												.append($('<i>',{class:"fa fa-clock-o"}))
												.append(' '+ data.playedOn)
											);
							divBody.append($('<p>').append(data.message));
						} else if(type == "activity") {
							console.log('im in here2');
							divHeading.append($('<h4>',{class:'timeline-title'}).append("Got a rating of " + data.rating +" on " + data.spiceActivityName));
							divHeading.append($('<p>').append('<small>', {class:'text-muted'})
												.append($('<i>',{class:"fa fa-clock-o"}))
												.append(' '+ data.addedOn)
											);
							
							
							divBody.append($('<p>').append(data.comments));
						}
					
						divPanel.append(divHeading);
						divPanel.append(divBody);
						li.append(divPanel);
						timeline.append(li);
					}
					
					
					
					var y = activitiesDone == null ? -1 : activitiesDone.length - 1;
					if(gamesDone != null) {
						for(var x = gamesDone.length - 1; x >=0; x--) {
							for( ; y >= 0; y--) {
								var date1 = new Date(gamesDone[x].playedOn);
								var date2 = new Date(activitiesDone[y].addedOn);
								if(date2 > date1) {
									addToTimeLine('activity',activitiesDone[y]);
								} else {
									break;
								}
							}
							
							console.log(new Date(gamesDone[x].playedOn));
							addToTimeLine('game', gamesDone[x]);
						}
						
					}
					
					for( ; y >= 0; y--) {
						addToTimeLine('activity',activitiesDone[y]);
					}
					
					
				  },
			error: function(data){
					console.log(data);
				  }
			});
	}
	
	function userClickedOnParentCaretakerList(userID,roleID) {
		 var childProfileContainer = $('#parent-caretaker-profile-info'),
				profPicDiv = childProfileContainer.children().eq(0),
				profInfoDiv = childProfileContainer.children().eq(1);
		
		// console.log(el);
		
		$.ajax({
				beforeSend: function() {
					// profPicDiv.empty();
					profInfoDiv.empty();
					// profPicDiv.append('<div class="ajax-loader"></div>');
					profInfoDiv.append('<div class="ajax-loader"></div>');
				},
				type: "POST",
				url: "dashboard/getParentCaretakerData",
				dataType: "json",
				data:  { userid : userID, roleid: roleID},
				success:
				  function(data) {
					  console.log(data);
					  profPicDiv.empty();
					  profInfoDiv.empty();
					  
					  var user = data.result.user[0],
						 age = Math.ceil(Math.abs((new Date()).getTime() - (new Date(user.dateOfBirth)).getTime()) / (1000 * 3600 * 24 * 365));
						 // parent = data.result.parent[0];
							 
					  profPicDiv.append($("<img>", {src: 'images/users/'+user.filename}));
					  profInfoDiv.append($("<label>").append(user.firstName + ' ' + user.lastName))
							  .append($("<label>").append(roleID == "3" ? 'Parent' : 'Caretaker'))
							  .append($("<label>").append('Contact # '+ user.telephone))
							  .append($("<label>").append(age + ' years old'));
							  
					  var tableContainer = $('#parent-caretaker-table-children');
					  tableContainer.empty();
					  
					  var table = $('<table>',{class:'table table-bordered table-hover table-striped'}),
							thead = $('<thead>'),
							tbody = $('<tbody>'),
							thead_tr = $('<tr>');
							
						  thead_tr.append($('<th>').append('Name'));
						  thead_tr.append($('<th>').append('Gender'));
						  thead_tr.append($('<th>').append('Email'));
						  thead_tr.append($('<th>').append('Contact #'));
						  
						  thead.append(thead_tr);
						  table.append(thead);
					  
					  if(data.result.children.length == 0) {
						var tbody_tr = $('<tr>');

						tbody_tr.append($('<td>').append('None'));
						tbody_tr.append($('<td>').append('None'));
						tbody_tr.append($('<td>').append('None'));
						tbody_tr.append($('<td>').append('None'));
						tbody.append(tbody_tr);
												  
						table.append(tbody);
						tableContainer.append(table);
					  } else {
						  var children = data.result.children;
						  console.log(children);
						  for(var x = 0, length = children.length; x < length; x++) {
							 var child = children[x],
								 tbody_tr = $('<tr>');
								
								tbody_tr.append($('<td>').append(child.firstName + ' ' + child.lastName));
								tbody_tr.append($('<td>').append(child.gender == 2 ? 'Female' : 'Male' ));
								tbody_tr.append($('<td>').append(child.emailAddress ));
								tbody_tr.append($('<td>').append(child.telephone ));
								tbody.append(tbody_tr);
						  }
						  
						  table.append(tbody);
						  tableContainer.append(table);
					  }
				  },
			error: function(data){
					console.log(data);
				  }
			});
	}

	$('#chart-dropdown li').click(function() {
		if($(this).data('action') == 'linechart') {
			$('#btn-chart').empty()
								.append('Line Chart ')
								.append($('<span>',{class:'caret'}));
			$('#morris-bar-chart').hide();
			$('#morris-line-chart').show();
		} else {
			$('#btn-chart').empty()
								.append('Bar Chart ')
								.append($('<span>',{class:'caret'}));
			$('#morris-bar-chart').show();
			$('#morris-line-chart').hide();
		}
		console.log();
	});
	
	$('#spiceForm').submit(function() {
		$.ajax({
			beforeSend: function() {
				$('#btn-spiceform-submit').prop('disabled', true);
			},
			type: "POST",
			url: "dashboard/submitSpiceForm",
			dataType: "json",
			data:  $(this).serialize() + '&childid='+$('#spiceform_input_idchild').data('userid'),
			success:
			function(data) {
				console.log(data);
				$('#btn-spiceform-submit').prop('disabled', false);

				if(data.mode == 'insert') {
					
				}
			}
		});
		
		return false;
		
	});
	
	// shows a modal when clicks on a panel for details for this week
	$('.view-activities-modal').click(function() {
		var divmodal = $('#modal_spiceActivities'),
			   targetid = $(this).data('targetid'),
			   container = $('#spice-activities-container');
		
		container.children().hide();
		container.children('#'+targetid).show();
		divmodal.modal('show');
		
	});
	
	$('.btn-newusers').click(function() {
		$('#manageusers').trigger('click');
	});
	
	$('#btn_modal_spiceform').click(function() {
		var target_modal = $('#modal_spiceForm');
		target_modal.modal('show');
	});
	
	$('.add-activity').click(function() {
		var name = $(this).data('type');
		var devid = $(this).data('devid');
			
		$('#add-activity-type').val(name)
							   .attr('data-id',devid)
							   .data('id',devid);
							   
		$('#modal_add-activity').modal('show');
	});
	
	$('#add-activity-form').submit(function() {
		// console.log('submitted');
		var type = $('#add-activity-type'),
			act_name = $(this).serialize();
		console.log(act_name);
		$.ajax({
			beforeSend: function() {
				// notifications.empty()
							 // .append('<div class="ajax-loader"></div>');
			},
			type: "POST",
			url: "dashboard/addActivity",
			data: $(this).serialize() + '&devid=' + type.data('id'),
			dataType: "json",
			success:
			function(data) {
				console.log(data);
				console.log($('#manageactivities_'+ type.val()));
				if(data.flag == true) {
					console.log('asdasd');
					var tr = $('<tr>');
						tr.append($('<td>').append(act_name.substring(13)))
						  .append($('<td>')
						  .append(
								$('<div>',{class:'text-center'}).append(
									$('<a>',{'data-spiceactivityid':data.id,href:'javascript:void(0)'}).append('X').click(function() {
										var el = $(this);
										$.ajax({
											beforeSend: function() {
											},
											type: "POST",
											url: "dashboard/deleteActivity",
											data: 'activityID=' + $(this).data('spiceactivityid'),
											dataType: "json",
											success:
											function(data) {
												if(data == true) {
													el.parent().parent().parent().remove();
												}
											}
										});
									})
								)
							 )
						  );
					console.log('manageactivities_'+ type.val().toLowerCase());
					$('#manageactivities_'+ type.val().toLowerCase()).append(tr);
					
				}
	  
			}
		});
		
		
		return false;
	});
	
	$('#gamelisting-modal-add').click(function() {
		$.ajax({
			type: "POST",
			url: "dashboard/getGamesToAdd",
			dataType: "json",
			success:
			function(data) {
				var select = $('#form-game');
				select.empty();
				for(var x = 0 ;x < data.length; x++ ) {
					var name = data[x].split('/');
					select.append($('<option>',{value:name[2]}).append(name[2]));
				}
				$('#modal_add-game').modal('show');
			}
		});
		return false;
	});
	
	$('#add-game-form').submit(function() {
		
		var data = $(this).serialize();
		console.log(data);
		$.ajax({
			type: "POST",
			url: "dashboard/addGame",
			data: $(this).serialize(),
			dataType: "json",
			success:
			function(data) {
				console.log(data);
				alert('Game has been added');
				$('#modal_add-game').modal('hide');
				refresh.gamelistings();
			},
			error: function(data) {
				console.log(data);
				alert('Title already exists');
			}
		});
		return false;
	});
	
	
	$('#add-guidance-form').submit(function() {
		$.ajax({
			beforeSend: function() {
				// notifications.empty()
							 // .append('<div class="ajax-loader"></div>');
			},
			type: "POST",
			url: "dashboard/add_guidance_video",
			data: $(this).serialize(),
			dataType: "json",
			success:
			function(data) {
				console.log(data);
			}
		});
		return false;
	});
	
	function init() {
		$('#form-search-assigned-users').trigger('submit');
		refresh.notificationPanel();
		
		var socialActivities = $('#manageactivities_social'),
			physicalActivities = $('#manageactivities_physical'),
			intellectualActivities = $('#manageactivities_intellectual'),
			creativeActivities = $('#manageactivities_creative'),
			emotionalActivities = $('#manageactivities_emotional');
		
		$.get('/eccs/dashboard/getActivities', {},function(data) {
			
			for(var x = 0, len = data.length; x < len; x++) {
				var tr = $('<tr>');
					tr.append($('<td>').append(data[x].spiceActivityName))
					  .append($('<td>')
					  .append(
							$('<div>',{class:'text-center'}).append(
								$('<a>',{'data-spiceactivityid':data[x].spiceactivityID,href:'javascript:void(0)'}).append('X').click(function() {
									var el = $(this);
									$.ajax({
										beforeSend: function() {
										},
										type: "POST",
										url: "dashboard/deleteActivity",
										data: 'activityID=' + $(this).data('spiceactivityid'),
										dataType: "json",
										success:
										function(data) {
											if(data == true) {
												el.parent().parent().parent().remove();
											}
											console.log(data);
										}
									});
								})
							)
						 )
					  );
						
				switch(data[x].spiceDevelopmentName) {
					case 'Social':
						socialActivities.append(tr);
						break;
					case 'Physical':
						physicalActivities.append(tr);
						break;
					case 'Intellectual':
						intellectualActivities.append(tr);
						break;
					case 'Creative':
						console.log('cativeee');
						creativeActivities.append(tr);
						break;
					case 'Emotional':
						emotionalActivities.append(tr);
						break;
				}
			}
		}, "json");
	}
	
	init();
	
});
