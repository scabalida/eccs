$(document).ready(function() {
	
	$("#invalidLogin").hide();
	$("#errorAddMsg").hide();
	$("#succAddMsg").hide();
	
	$("#imageForm").submit(function(evt) {
		evt.preventDefault();

		var formData = new FormData($(this)[0]);
		$.ajax({
			url: "user/uploadImage",
			type: 'POST',
			data: formData,
			processData: false,
			contentType: false,
			success: function (res) {
				console.log(res);
			},
			error: function (error) {
				console.log(error);
			}
		});          

	});
	//LOGIN//
		$("#loginForm").submit(function() {
			var user = $("#username").val();
			user = user.replace(/\s/g,'');
			var pass = $("#inputPassword").val();
			
				
		  	$.ajax({
				type: "POST",
				url: "user/loginNow",
				dataType: "json",
				data:  {user:user, pass:pass},
				success:
				  function(data) {
						if (data == "true")
						{
							$("#invalidLogin").hide(); 
							setTimeout(function(){ window.location = $("#url").val(); }, 1000);
						}
						else
						{
							$("#invalidLogin").show();
							
						}
							
				  },
							
			error: function(data){
					console.log(data);
				  }
			});
			return false;
		});
		//LOG OUT //
		$("#logout").click(function() {
			var url = $("#logout").attr("data-url");
			$.get('/eccs/user/logout', {},function(data) {
				
				if(data == "true"){
					setTimeout(function(){ window.location = url ; }, 1000);
				}
				else{
					setTimeout(function(){ window.location = url ; }, 1000);
				}
				
            }, "json");
		});
		//ADD USER //
		$("#addAccountForm").submit(function(e) {
			e.preventDefault();
			var surname = $("#inputSurname").val();
			var name = $("#inputName").val();
			var telephone = $("#inputTelephone").val();
			var address = $("#inputAddress").val();
			var gender = $("input[name=gender]").val();
			var bday = $("input[name=birthdate]").val();
			var email = $("#email").val();
			email = email.replace(/\s/g,'');
			var username = $("#inputUsername").val();
			username = username.replace(/\s/g,'');
			var pass = $("#password").val();
			var cpass = $("#inputCPassword").val();
			var role = $("input[name=role]").val();
			
			if(pass != cpass){
				$("#errorMsg").html("Your passwords do not match. Please try again.");
				$("#errorAddMsg").show();
			}
			else{
				console.log(username);
			  $.get('/eccs/user/checkUser/'+username, {},function(data) {
				
				if(data == "true"){
					$("#errorMsg").html("Username is already exist.");
				$("#errorAddMsg").show();
				}
				else{
					$("#errorAddMsg").hide();
					$.ajax({
					type: "POST",
					url: "user/addUser",
					dataType: "json",
					data:  {surname:surname, name:name, telephone:telephone, address:address, gender:gender, bday:bday, email:email, username:username, pass:pass, role:role},
					success:
					  function(data) {
							if (data == "true")
							{
								$("#succMsg").html("New Account Successfully Added.");
								$("#succAddMsg").show(); 
								setTimeout(function(){ window.location = $("#urlAdd").val(); }, 3000);
							}
							else
							{
								$("#errorMsg").html("Error in adding the User Infomation.");
								$("#errorAddMsg").show();
								
							}
								
					  },
								
					error: function(data){
							console.log(data);
						  }
					}); 
					return false;
				}
            }, "json");
				
				
			
			}
		});
    });

	