$(function() {

    Morris.Line({
        element: 'morris-area-chart',
        data: [{
            period: '2017-02-12',
			social:1,
			physical:2,
			intellectual:3,
			creative:4,
			emotional:5
        }, {
            period: '2017-02-19',
			social:2,
			physical:3,
			intellectual:4,
			creative:5,
			emotional:7
        }, {
            period: '2017-02-26',
			social:4,
			physical:7,
			intellectual:5,
			creative:8,
			emotional:5
        }, {
            period: '2017-03-3',
			social:3,
			physical:4,
			intellectual:8,
			creative:1,
			emotional:5
        }, {
            period: '2017-03-10',
			social:2,
			physical:3,
			intellectual:4,
			creative:5,
			emotional:5
        }, {
            period: '2017-03-17',
			social:5,
			physical:8,
			intellectual:2,
			creative:3,
			emotional:5
        }, {
            period: '2017-03-24',
			social:6,
			physical:7,
			intellectual:3,
			creative:2,
			emotional:2
        }, {
            period: '2017-03-31',
			social:5,
			physical:2,
			intellectual:3,
			creative:2,
			emotional:5
        }],
        xkey: 'period',
        ykeys: ['social', 'physical', 'intellectual','creative','emotional'],
        labels: ['social', 'physical', 'intellectual','creative','emotional'],
        pointSize: 1,
        hideHover: 'auto',
        resize: true
    });



});
